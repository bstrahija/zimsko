<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'zimsko' );

if ($_SERVER['SERVER_NAME'] === 'zimsko.test') {
    define('WP_HOME',    'https://zimsko.test');
    define('WP_SITEURL', 'https://zimsko.test');
} else {
    define('WP_HOME',    'http://zimsko.com');
    define('WP_SITEURL', 'http://zimsko.com');
}

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('WP_DEBUG', true);
define('WP_DEBUG_DISPLAY', true);
define('WP_DEBUG_LOG', true);

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2oe3D5Xkg/IjTfN/jtt4nZTzm+5n3Eh1LQqvEIWoMH9xlitglHKYLLtXB5qS/dy+EbfvRtrnYncn6yvuoNy34w==');
define('SECURE_AUTH_KEY',  'xrH4jIcrftZr3Iq+w17O5PIvXfbOu1mehoOYGOM190P7j94n/DPEBSsCLsmdW25wYeyydU5rDvIzHTCSHaOXAg==');
define('LOGGED_IN_KEY',    'KgzLxlAVKnHWFnGjZOOnTEWVMrgWStW7fAEDVsS96h7enstoOa3iT8s7FG/yqCrEGJC9BVYfkNV+vIqZcnMRNQ==');
define('NONCE_KEY',        '/MJLMK/LguxDCHSR4RY1QDEUGaI5nKg5QJkTXvr00kJ2ObO0JEjGh8SIguZBVPEcaM+PIUkgKVaUcg2Ki7AduA==');
define('AUTH_SALT',        'VEwS0JUjItXDJ5OSVrLNDMGqIrQm1/myXYpaXGN3UD+mtNYOIA73bfzbvmlv8sHgP1MkRUEWN3/Qr8P/+viJTA==');
define('SECURE_AUTH_SALT', 'Zys4uxWdOrptWSAS21GA6grTyyZrcLNbwujitVUWsTA3LTb3F1A/Nh/rEE7xCgTOJYc6zPfiKcoJdwKSvvXwLA==');
define('LOGGED_IN_SALT',   '68LMWjdygyYj9HAM9YMr9Gx5bAJh5Lb+Hdaeh3767tJOJ9I2BULVNoW6Fm4QiG5V+1KoBtKa1uTKGKWOLnRO/Q==');
define('NONCE_SALT',       'opCu+sB7wD2iCEBEoNR3jiSPF3MbMG817jNhE9U9puv2UVoPIxK2EFUKuidRWQK1e+pyd54vkw0A8SiPas5mQg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
