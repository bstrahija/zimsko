<?php

namespace Creolab\Basket\Plugin\Data;

use function  Basket\log;
use Creolab\Basket\Plugin\Db\Tables;
use WP_Post;

class SyncCoach extends BaseSync
{
    protected static $table = 'coaches';

    /**
     * Sync coach data to custom table
     *
     * @param  WP_Post|int $post
     * @return object
     */
    public function sync($post)
    {
        global $wpdb;

        if (is_int($post)) {
            $post = get_post($post);
        }

        // Prepare the data
        $data = [
            'wp_id'      => $post->ID,
            'name'       => $post->post_title,
            'slug'       => $post->post_name,
            'body'       => $post->post_content,
            'birthday'   => get_field('birthday', $post->ID),
            'updated_at' => $post->post_modified,
        ];

        // First check if we already have this coach in the database
        $exists = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('coaches')." WHERE wp_id = %d", $post->ID));

        if (! $exists) {
            log('[DATA] Saving coach to database: ' . $post->post_title);
            $wpdb->insert(Tables::get('coaches'), array_merge($data, ['created_at' => $post->post_date]));
        } else {
            log('[DATA] Updating coach in database: ' . $post->post_title);
            $wpdb->update(Tables::get('coaches'), $data, ['wp_id' => $post->ID]);
        }

        // @TODO: Find a way to connect images, maybe through a custom JSON field in the DB

        // Get the record and return it
        return $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('coaches')." WHERE wp_id = %d", $post->ID));
    }
}
