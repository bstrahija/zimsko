<?php

namespace Creolab\Basket\Plugin\Data;

use function  Basket\log;
use Creolab\Basket\Plugin\Db\Tables;
use WP_Post;

abstract class BaseSync
{
    protected static $table;

    public function find($id)
    {
        global $wpdb;

        return $wpdb->get_row($wpdb->prepare("SELECT * FROM " . self::table() . " WHERE wp_id = %d", $id));
    }

    public function findByWpId($wpId)
    {
        global $wpdb;

        return $wpdb->get_row($wpdb->prepare("SELECT * FROM " . self::table() . " WHERE wp_id = %d", $wpId));
    }

    public static function table()
    {
        return Tables::get(static::$table);
    }
}
