<?php

namespace Creolab\Basket\Plugin\Data;

use function  Basket\log;
use Creolab\Basket\Plugin\Db\Tables;
use WP_Term;

class SyncEvent extends BaseSync
{
    protected static $table = 'events';

    /**
     * Sync player data to custom table
     *
     * @param  WP_Post $post
     * @return object
     */
    public function sync(WP_Term $term)
    {
        global $wpdb;

        try {
            // Prepare the data
            $data = [
                'wp_id'      => $term->term_id,
                'title'      => $term->name,
                'slug'       => $term->slug,
                'updated_at' => current_time('mysql'),
            ];

            // First check if we already have this tournament in the database
            $eventExists = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('events')." WHERE wp_id = %d", $term->term_id));

            if (! $eventExists) {
                log('[DATA] Saving event to database: ' . $term->name);
                $wpdb->insert(Tables::get('events'), array_merge($data, ['created_at' => current_time('mysql')]));
            } else {
                log('[DATA] Updating event in database: ' . $term->name);
                $wpdb->update(Tables::get('events'), $data, ['wp_id' => $term->term_id]);
            }
        } catch(\Exception $e) {
            log('[DATA] Failed to update event: ' . $term->name . ' / ' . $e->getMessage(), 'error');
        }

        return $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('events')." WHERE wp_id = %d", $term->term_id));
    }
}
