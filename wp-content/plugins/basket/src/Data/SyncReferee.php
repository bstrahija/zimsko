<?php

namespace Creolab\Basket\Plugin\Data;

use function  Basket\log;
use Creolab\Basket\Plugin\Db\Tables;
use WP_Post;

class SyncReferee extends BaseSync
{
    protected static $table = 'referees';

    /**
     * Sync referee data to custom table
     *
     * @param  WP_Post $post
     * @return object
     */
    public function sync(WP_Post $post)
    {
        global $wpdb;

        if (is_int($post)) {
            $post = get_post($post);
        }

        // Prepare the data
        $data = [
            'wp_id'      => $post->ID,
            'name'       => $post->post_title,
            'slug'       => $post->post_name,
            'body'       => $post->post_content,
            'updated_at' => $post->post_modified,
        ];

        // First check if we already have this referee in the database
        $exists = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('referees')." WHERE wp_id = %d", $post->ID));

        if (! $exists) {
            log('[DATA] Saving referee to database: ' . $post->post_title);
            $wpdb->insert(Tables::get('referees'), array_merge($data, ['created_at' => $post->post_date]));
        } else {
            log('[DATA] Updating referee in database: ' . $post->post_title);
            $wpdb->update(Tables::get('referees'), $data, ['wp_id' => $post->ID]);
        }

        // @TODO: Find a way to connect images, maybe through a custom JSON field in the DB

        // @TODO: Connect to the event (possible multiple events)

        // Get the record and return it
        return $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('referees')." WHERE wp_id = %d", $post->ID));
    }
}
