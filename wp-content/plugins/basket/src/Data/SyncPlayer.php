<?php

namespace Creolab\Basket\Plugin\Data;

use function  Basket\log;
use Creolab\Basket\Plugin\Db\Tables;
use WP_Post;

class SyncPlayer extends BaseSync
{
    protected static $table = 'players';

    /**
     * Sync player data to custom table
     *
     * @param  WP_Post|int $post
     * @return void
     */
    public function sync($post)
    {
        global $wpdb;

        if (is_int($post)) {
            $post = get_post($post);
        }

        // Get position
        $position = get_field('position', $post->ID);
        if ($position) $position = $position['value'];

        // Prepare the data
        $data = [
            'wp_id'      => $post->ID,
            'name'       => $post->post_title,
            'slug'       => $post->post_name,
            'body'       => $post->post_content,
            'position'   => $position,
            'data'       => '{}',
            'number'     => get_field('jersey_number',   $post->ID),
            'birthday'   => get_field('birthday', $post->ID),
            'updated_at' => $post->post_modified,
        ];

        // First check if we already have this player in the database
        $exists = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('players')." WHERE wp_id = %d", $post->ID));

        if (! $exists) {
            log('[DATA] Saving player to database: ' . $post->post_title);
            $wpdb->insert(Tables::get('players'), array_merge($data, ['created_at' => $post->post_date]));
        } else {
            log('[DATA] Updating player in database: ' . $post->post_title);
            $wpdb->update(Tables::get('players'), $data, ['wp_id' => $post->ID]);
        }

        // @TODO: Find a way to connect images, maybe through a custom JSON field in the DB

        // Connect to the team (possible multiple teams)
        $teams = get_field('teams', $post->ID);
        if ($teams) {
            foreach ($teams as $team) {
                // Check if connection exists
                $exists = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('player_team')." WHERE player_id = %d AND team_id = %d", $post->ID, $team->ID));

                if (! $exists) {
                    log('[DATA] Saving player to team: ' . $post->post_title . ' -> ' . $team->post_title);
                    $wpdb->insert(Tables::get('player_team'), [
                        'player_id'  => $post->ID,
                        'team_id'    => $team->ID,
                        'created_at' => current_time('mysql'),
                        'updated_at' => current_time('mysql'),
                    ]);
                } else {
                    log('[DATA] Player already in team: ' . $post->post_title . ' -> ' . $team->post_title);
                }
            }
        }

        // @TODO: Add awards and stats

        // Get the record and return it
        return $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('players')." WHERE wp_id = %d", $post->ID));
    }
}
