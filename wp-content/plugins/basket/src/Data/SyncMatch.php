<?php

namespace Creolab\Basket\Plugin\Data;

use function  Basket\log;
use Creolab\Basket\Plugin\Db\Tables;
use WP_Post;

class SyncMatch extends BaseSync
{
    protected static $table = 'matches';

    /**
     * Sync match data to custom table
     *
     * @param  WP_Post|int $post
     * @return object
     */
    public function sync($post)
    {
        global $wpdb;

        if (is_int($post)) {
            $post = get_post($post);
        }

        // Prepare the data
        $data = [
            'wp_id'        => $post->ID,
            'title'        => $post->post_title,
            'slug'         => $post->post_name,
            'body'         => $post->post_content,
            'scheduled_at' => get_field('match_date', $post->ID),
            'updated_at'   => $post->post_modified,
        ];

        // First check if we already have this match in the database
        $exists = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".self::table()." WHERE wp_id = %d", $post->ID));

        if (! $exists) {
            log('[DATA] Saving match to database: ' . $post->post_title);
            $wpdb->insert(self::table(), array_merge($data, ['created_at' => $post->post_date]));
        } else {
            log('[DATA] Updating match in database: ' . $post->post_title);
            $wpdb->update(self::table(), $data, ['wp_id' => $post->ID]);
        }

        // Get the record
        $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".self::table()." WHERE wp_id = %d", $post->ID));

        // Add to event
        $events = get_the_terms($post, 'tournament');
        if ($events) {
            foreach ($events as $event) {
                $eventRecord = (new SyncEvent())->sync($event);
                $wpdb->update(self::table(), ['event_id' => $eventRecord->id], ['id' => $item->id]);
            }
        }

        // Add the teams (connected by custom ID, not the WP ID)
        $teams    = get_field('match_teams', $post->ID);
        $home     = isset($teams['home_team'], $teams['home_team'][0]) ? $teams['home_team'][0] : null;
        $away     = isset($teams['away_team'], $teams['away_team'][0]) ? $teams['away_team'][0] : null;
        $homeTeam = (new SyncTeam())->sync($home);
        $awayTeam = (new SyncTeam())->sync($away);

        // Connect to the match
        $wpdb->update(self::table(), ['home_team_id' => $homeTeam->id, 'away_team_id' => $awayTeam->id], ['id' => $item->id]);

        // Get the scores
        $results    = get_field('match_results',    $post->ID);
        $resultsQ1  = get_field('match_results_q1', $post->ID);
        $resultsQ2  = get_field('match_results_q2', $post->ID);
        $resultsQ3  = get_field('match_results_q3', $post->ID);
        $resultsQ4  = get_field('match_results_q4', $post->ID);
        $resultsOT1 = get_field('match_results_p1', $post->ID);

        // And write them to the database
        $wpdb->update(self::table(), [
            'winner_id'       => $results['home_score'] > $results['away_score'] ? $homeTeam->id : $awayTeam->id,
            'home_team_score' => $results['home_score'],
            'away_team_score' => $results['away_score'],
            'home_team_score_q1' => $resultsQ1['home_score_q1'],
            'away_team_score_q1' => $resultsQ1['away_score_q1'],
            'home_team_score_q2' => $resultsQ2['home_score_q2'],
            'away_team_score_q2' => $resultsQ2['away_score_q2'],
            'home_team_score_q3' => $resultsQ3['home_score_q3'],
            'away_team_score_q3' => $resultsQ3['away_score_q3'],
            'home_team_score_q4' => $resultsQ4['home_score_q4'],
            'away_team_score_q4' => $resultsQ4['away_score_q4'],
            'home_team_score_ot1' => $resultsOT1['home_score_p1'],
            'away_team_score_ot1' => $resultsOT1['away_score_p1'],
        ], ['id' => $item->id]);

        // Status
        $finished = get_field('match_finished', $post->ID);
        if ($finished) $wpdb->update(self::table(), ['status' => 'finished'], ['id' => $item->id]);

        // Referees
        $referees = get_field('match_referees', $post->ID);
        if ($referees) {
            foreach ($referees as $referee) {
                $refereeRecord = (new SyncReferee)->sync($referee);

                // Connect to the match
                $exists = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('match_referee')." WHERE match_id = %d AND referee_id = %d", $item->id, $refereeRecord->id));
                if (! $exists) {
                    $wpdb->insert(Tables::get('match_referee'), ['match_id' => $item->id, 'referee_id' => $refereeRecord->id]);
                }
            }
        }

        // Also sync the player and team stats
        $this->syncPlayerStats($post);
        $this->syncTeamStats($post, ['home_team_id' => $homeTeam->id, 'away_team_id' => $awayTeam->id]);

        return $item;
    }

    /**
     * Sync player data to custom table
     *
     * @param  WP_Post|int $post
     * @return void
     */
    public function syncPlayerStats($post)
    {
        global $wpdb;

        if (is_int($post)) {
            $post = get_post($post);
        }

        // Get player scores
        $playerScores = get_field('player_scores', $post->ID);
        $players      = [];

        // Get match and event
        $item  = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".self::table()." WHERE wp_id = %d", $post->ID));
        $event = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('events')." WHERE id = %d", $item->event_id));

        if ($playerScores['home_players']) {
            $players = array_merge($players, $playerScores['home_players']);
        }
        if ($playerScores['away_players']) {
            $players = array_merge($players, $playerScores['away_players']);
        }

        // Loop through players and add the scores
        foreach ($players as $player) {
            if ($player['player'] && $player['player']->ID) {
                $data         = ['points' => $player['score'], 'points_avg' => $player['score'], 'event_id' => $event->id];
                $exists       = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('player_stats')." WHERE player_id = %d AND match_id = %d", [$player['player']->ID, $item->id]));
                if (! $exists) {
                    $wpdb->insert(Tables::get('player_stats'), array_merge($data, ['player_id' => $player['player']->ID, 'match_id' => $item->id]));
                } else {
                    $wpdb->update(Tables::get('player_stats'), $data, ['player_id' => $player['player']->ID, 'match_id' => $item->id]);
                }
            }
        }

        // We also need to calculate the average points for the player
        foreach ($players as $player) {
            if ($player['player'] && $player['player']->ID) {
                $eventMatches  = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".Tables::get('player_stats')." WHERE player_id = %d AND event_id = %d AND match_id IS NOT NULL", [$player['player']->ID, $event->id]));
                $eventScore    = $wpdb->get_var($wpdb->prepare("SELECT SUM(points) FROM ".Tables::get('player_stats')." WHERE player_id = %d AND event_id = %d AND match_id IS NOT NULL", [$player['player']->ID, $event->id]));
                $eventScoreAvg = $eventMatches ? str_replace(",", ".", (string) round($eventScore / count($eventMatches), 2)) : 0;
                $matches       = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".Tables::get('player_stats')." WHERE player_id = %d AND event_id IS NULL AND match_id IS NOT NULL", $player['player']->ID));
                $score         = $wpdb->get_var($wpdb->prepare("SELECT SUM(points) FROM ".Tables::get('player_stats')." WHERE player_id = %d AND event_id IS NULL AND match_id IS NOT NULL", $player['player']->ID));
                $scoreAvg      = $matches ? str_replace(",", ".", (string) round($score / count($matches), 2)) : 0;

                $data         = ['points' => $eventScore, 'points_avg' => $eventScoreAvg, 'type' => 'event'];
                $existsEvent  = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('player_stats')." WHERE player_id = %d AND event_id = %d AND match_id IS NULL", [$player['player']->ID, $event->id]));
                if (! $existsEvent) {
                    $wpdb->insert(Tables::get('player_stats'), array_merge($data, ['player_id' => $player['player']->ID, 'event_id' => $event->id]));
                } else {
                    $wpdb->update(Tables::get('player_stats'), $data, ['player_id' => $player['player']->ID, 'match_id' => null, 'event_id' => $event->id]);
                }

                $data    = ['points' => $score, 'points_avg' => $scoreAvg, 'type' => 'all'];
                $exists  = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('player_stats')." WHERE player_id = %d AND event_id IS NULL AND match_id IS NULL", $player['player']->ID));
                if (! $exists) {
                    $wpdb->insert(Tables::get('player_stats'), array_merge($data, ['player_id' => $player['player']->ID]));
                } else {
                    $wpdb->update(Tables::get('player_stats'), $data, ['player_id' => $player['player']->ID, 'match_id' => null, 'event_id' => null]);
                }
            }
        }
    }

    /**
     * Sync team data to custom table
     *
     * @param  WP_Post|int $post
     * @return void
     */
    public function syncTeamStats($post, $options = [])
    {
        global $wpdb;

        if (is_int($post)) {
            $post = get_post($post);
        }

        // Get match and event record
        $item  = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".self::table()." WHERE wp_id = %d", $post->ID));
        $event = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('events')." WHERE id = %d", $item->event_id));

        // Check stats
        $existsHome = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('team_stats')." WHERE team_id = %d AND match_id = %d", [$options['home_team_id'], $item->id]));
        $existsAway = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('team_stats')." WHERE team_id = %d AND match_id = %d", [$options['home_team_id'], $item->id]));

        // Prepare data
        $data = [
            'team_id'        => (int) $options['home_team_id'],
            'match_id'       => (int) $item->id,
            'points_avg'     => (float) $item->home_team_score,
            'points'         => (int) $item->home_team_score,
            'points_against' => (int) $item->away_team_score,
            'points_diff'    => (int) $item->home_team_score - $item->away_team_score,
            'event_id'       => (int) $event->id
        ];

        // Home team
        if (! $existsHome) {
            $wpdb->insert(Tables::get('team_stats'), $data);
        } else {
            $wpdb->update(Tables::get('team_stats'), $data, ['team_id' => $options['home_team_id'], 'match_id' => $item->id]);
        }

        // Prepare data
        $data = [
            'team_id'        => (int) $options['away_team_id'],
            'match_id'       => (int) $item->id,
            'points_avg'     => (float) $item->away_team_score,
            'points'         => (int) $item->away_team_score,
            'points_against' => (int) $item->home_team_score,
            'points_diff'    => (int) $item->away_team_score - $item->home_team_score,
            'event_id'       => (int) $event->id
        ];

        // Away team
        if (! $existsAway) {
            $wpdb->insert(Tables::get('team_stats'), $data);
        } else {
            $wpdb->update(Tables::get('team_stats'), $data, ['team_id' => $options['away_team_id'], 'match_id' => $item->id]);
        }

        // Get all matches for the event and all matches for the team
        $homeEventMatches       = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".Tables::get('team_stats')." WHERE team_id = %d AND event_id = %d AND match_id IS NOT NULL", [$options['home_team_id'], $event->id]));
        $homeEventPoints        = $this->getTeamPoints($options['home_team_id'], 'event', $event->id);
        $homeEventPointsAgainst = $this->getTeamPointsAgainst($options['home_team_id'], 'event', $event->id);
        $homeEventPointsDiff    = $homeEventPoints - $homeEventPointsAgainst;
        $homeEventPointsAvg     = str_replace(",", ".", (string) round($homeEventPoints / count($homeEventMatches), 2));
        $homeMatches            = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".Tables::get('team_stats')." WHERE team_id = %d AND match_id IS NOT NULL", $options['home_team_id']));
        $homePoints             = $this->getTeamPoints($options['home_team_id'], 'all');
        $homePointsAgainst      = $this->getTeamPointsAgainst($options['home_team_id'], 'all');
        $homePointsDiff         = $homePoints - $homePointsAgainst;
        $homePointsAvg          = str_replace(",", ".", (string) round($homePoints / count($homeMatches), 2));

        // Check stats
        $existsHomeEvent = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('team_stats')." WHERE team_id = %d AND match_id IS NULL AND event_id = %d", [$options['home_team_id'], $event->id]));
        $existsHome      = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('team_stats')." WHERE team_id = %d AND match_id IS NULL AND event_id IS NULL", $options['home_team_id']));
        if (! $existsHomeEvent) {
            $wpdb->insert(Tables::get('team_stats'), ['team_id' => $options['home_team_id'], 'event_id' => $event->id, 'points' => $homeEventPoints, 'points_avg' => $homeEventPointsAvg, 'points_against' => $homeEventPointsAgainst, 'points_diff' => $homeEventPointsDiff, 'type' => 'event']);
        } else {
            $wpdb->update(Tables::get('team_stats'), ['points' => $homeEventPoints, 'points_avg' => (string) $homeEventPointsAvg, 'points_diff' => $homeEventPointsDiff, 'points_against' => $homeEventPointsAgainst, 'type' => 'event'], ['team_id' => $options['home_team_id'], 'event_id' => $event->id, 'match_id' => null]);
        }
        if (! $existsHome) {
            $wpdb->insert(Tables::get('team_stats'), ['points' => $homePoints, 'team_id' => $options['home_team_id'], 'points_avg' => $homePointsAvg, 'points_against' => $homePointsAgainst, 'points_diff' => $homePointsDiff, 'type' => 'all']);
        } else {
            $wpdb->update(Tables::get('team_stats'), ['points' => $homePoints, 'points_avg' => (string) $homePointsAvg, 'points_diff' => $homePointsDiff, 'points_against' => $homePointsAgainst, 'type' => 'all'], ['team_id' => $options['home_team_id'], 'event_id' => null, 'match_id' => null]);
        }

        // Now handle away team
        $awayEventMatches       = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".Tables::get('team_stats')." WHERE team_id = %d AND event_id = %d AND match_id IS NOT NULL", [$options['away_team_id'], $event->id]));
        $awayEventPoints        = $this->getTeamPoints($options['away_team_id'], 'event', $event->id);
        $awayEventPointsAgainst = $this->getTeamPointsAgainst($options['away_team_id'], 'event', $event->id);
        $awayEventPointsDiff    = $awayEventPoints - $awayEventPointsAgainst;
        $awayEventPointsAvg     = str_replace(",", ".", (string) round($awayEventPoints / count($awayEventMatches), 2));
        $awayMatches            = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".Tables::get('team_stats')." WHERE team_id = %d AND match_id IS NOT NULL", $options['away_team_id']));
        $awayPoints             = $this->getTeamPoints($options['away_team_id'], 'all');
        $awayPointsAgainst      = $this->getTeamPointsAgainst($options['away_team_id'], 'all');
        $awayPointsDiff         = $awayPoints - $awayPointsAgainst;
        $awayPointsAvg          = str_replace(",", ".", (string) round($awayPoints / count($awayMatches), 2));

        // Check stats
        $existsAwayEvent = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('team_stats')." WHERE team_id = %d AND match_id IS NULL AND event_id = %d", [$options['away_team_id'], $event->id]));
        $existsAway      = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('team_stats')." WHERE team_id = %d AND match_id IS NULL AND event_id IS NULL", $options['away_team_id']));
        if (! $existsAwayEvent) {
            $wpdb->insert(Tables::get('team_stats'), ['points' => $awayEventPoints, 'team_id' => $options['away_team_id'], 'event_id' => $event->id, 'points_avg' => $awayEventPointsAvg, 'points_against' => $awayEventPointsAgainst, 'points_diff' => $awayEventPointsDiff, 'type' => 'event']);
        } else {
            $wpdb->update(Tables::get('team_stats'), ['points' => $awayEventPoints, 'points_avg' => $awayEventPointsAvg, 'points_diff' => $awayPointsDiff, 'points_against' => $awayEventPointsAgainst, 'type' => 'event'], ['team_id' => $options['away_team_id'], 'event_id' => $event->id, 'match_id' => null]);
        }
        if (! $existsAway) {
            $wpdb->insert(Tables::get('team_stats'), ['points' => $awayPoints, 'team_id' => $options['away_team_id'], 'points_avg' => $awayPointsAvg, 'points_diff' => $awayPointsDiff, 'points_against' => $awayPointsAgainst, 'type' => 'all']);
        } else {
            $wpdb->update(Tables::get('team_stats'), ['points' => $awayPoints, 'points_avg' => $awayPointsAvg, 'points_diff' => $awayPointsDiff, 'points_against' => $awayPointsAgainst, 'type' => 'all'], ['team_id' => $options['away_team_id'], 'event_id' => null, 'match_id' => null]);
        }
    }


    public function getTeamPoints($teamId, $type = 'match', $typeId = null)
    {
        global $wpdb;

        if ($type === 'match') {
            $homePoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(home_team_score) FROM ".self::table()." WHERE home_team_id = %d AND match_id = %d", [$teamId, $typeId]));
            $awayPoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(away_team_score) FROM ".self::table()." WHERE away_team_id = %d AND match_id = %d", [$teamId, $typeId]));
        } elseif ($type === 'event') {
            $homePoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(home_team_score) FROM ".self::table()." WHERE home_team_id = %d AND event_id = %d", [$teamId, $typeId]));
            $awayPoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(away_team_score) FROM ".self::table()." WHERE away_team_id = %d AND event_id = %d", [$teamId, $typeId]));
        } else {
            $homePoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(home_team_score) FROM ".self::table()." WHERE home_team_id = %d", $teamId));
            $awayPoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(away_team_score) FROM ".self::table()." WHERE away_team_id = %d", $teamId));
        }

        return (int) ($homePoints + $awayPoints);
    }

    public function getTeamPointsAgainst($teamId, $type = 'match', $typeId = null): int
    {
        global $wpdb;

        if ($type === 'match') {
            $homePoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(away_team_score) FROM ".self::table()." WHERE home_team_id = %d AND match_id = %d", [$teamId, $typeId]));
            $awayPoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(home_team_score) FROM ".self::table()." WHERE away_team_id = %d AND match_id = %d", [$teamId, $typeId]));
        } elseif ($type === 'event') {
            $homePoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(away_team_score) FROM ".self::table()." WHERE home_team_id = %d AND event_id = %d", [$teamId, $typeId]));
            $awayPoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(home_team_score) FROM ".self::table()." WHERE away_team_id = %d AND event_id = %d", [$teamId, $typeId]));
        } else {
            $homePoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(away_team_score) FROM ".self::table()." WHERE home_team_id = %d", $teamId));
            $awayPoints = (int) $wpdb->get_var($wpdb->prepare("SELECT SUM(home_team_score) FROM ".self::table()." WHERE away_team_id = %d", $teamId));
        }

        return (int) ($homePoints + $awayPoints);
    }

    public function getTeamPointsDiff($teamId, $type = 'match', $typeId = null): int
    {
        return (int) ($this->getTeamPoints($teamId, $type, $typeId) - $this->getTeamPointsAgainst($teamId, $type, $typeId));
    }
}
