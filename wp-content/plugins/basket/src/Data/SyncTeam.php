<?php

namespace Creolab\Basket\Plugin\Data;

use function  Basket\log;
use Creolab\Basket\Plugin\Db\Tables;
use WP_Post;

class SyncTeam extends BaseSync
{
    protected static $table = 'teams';

    /**
     * Sync team data to custom table
     *
     * @param  WP_Post|int $post
     * @return object
     */
    public function sync($post)
    {
        global $wpdb;

        if (is_int($post)) {
            $post = get_post($post);
        }

        // Prepare the data
        $data = [
            'wp_id'      => $post->ID,
            'title'      => $post->post_title,
            'slug'       => $post->post_name,
            'body'       => $post->post_content,
            'data'       => '{}',
            'updated_at' => $post->post_modified,
        ];

        // First check if we already have this tournament in the database
        $exists = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('teams')." WHERE wp_id = %d", $post->ID));

        if (! $exists) {
            log('[DATA] Saving team to database: ' . $post->post_title);
            $wpdb->insert(Tables::get('teams'), array_merge($data, ['created_at' => $post->post_date]));
        } else {
            log('[DATA] Updating team in database: ' . $post->post_title);
            $wpdb->update(Tables::get('teams'), $data, ['wp_id' => $post->ID]);
        }

        // Get the record
        $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('teams')." WHERE wp_id = %d", $post->ID));

        // Update the coaches
        $coaches = get_field('trainers', $post->ID);
        if ($coaches) {
            foreach ($coaches as $coach) {
                $coachRecord = (new SyncCoach)->sync($coach);

                // Connect to the team
                $exists = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('coach_team')." WHERE coach_id = %d AND team_id = %d", $coachRecord->id, $item->id));

                if (! $exists) {
                    log('[DATA] Connecting coach to team: ' . $coachRecord->name . ' -> ' . $item->title);
                    $wpdb->insert(Tables::get('coach_team'), [
                        'coach_id' => $coachRecord->id,
                        'team_id'  => $item->id,
                    ]);
                }
            }
        }

        // @TODO: Find a way to connect images, maybe through a custom JSON field in the DB

        // @TODO: We also need to bind the team to the event,

        // Create the players
        $this->syncPlayers($post);

        // Get the record and return it
        return $wpdb->get_row($wpdb->prepare("SELECT * FROM ".Tables::get('teams')." WHERE wp_id = %d", $post->ID));
    }

    /**
     * Sync all team players
     *
     * @param  WP_Post|int $post
     * @return void
     */
    public function syncPlayers($post)
    {
        global $wpdb;

        if (is_int($post)) {
            $post = get_post($post);
        }

        $players = get_field('players', $post->ID);

        if ($players) {
            foreach ($players as $player) {
                $playerRecord = (new SyncPlayer)->sync($player);
            }
        }
    }
}
