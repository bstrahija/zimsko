<?php

namespace Creolab\Basket\Plugin\Repositories;

use Creolab\Basket\Plugin\Models\Article;
use Tightenco\Collect\Support\Collection;

class News
{
    /**
     * Get latest news
     *
     * @param  array  $options
     * @return Collection
     */
    public static function latest($options = [])
    {
        // Init articles
        $articles = new Collection;

        // Init arguments
        $args = [
            'post_type'      => 'post',
            'orderby'        => 'date',
            'order'          => 'desc',
            'posts_per_page' => 5,
        ];

        // Merge with options
        $args = array_merge($args, $options);

        // Get articles
        foreach (get_posts($args) as $article) {
            $articles->push(new Article($article));
        }

        return $articles;
    }

    /**
     * @return BaseRepository
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }
}
