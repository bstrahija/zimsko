<?php

namespace Creolab\Basket\Plugin\Repositories;

use Creolab\Basket\Plugin\Models\Match;
use Creolab\Basket\Plugin\Models\Player;
use Creolab\Basket\Plugin\Models\Standing;
use Creolab\Basket\Plugin\Models\Team;
use Creolab\Basket\Plugin\Models\Tournament;
use Tightenco\Collect\Support\Collection;
use WP_Post;

class Tournaments
{
    protected static $current;

    protected static $leaders;

    protected static $threePointLeaders;

    /**
     * Return current tournament
     *
     * @return Tournament
     */
    public static function current()
    {
        if (! self::$current) {
            // We need the current tournament
            $tournament = get_field('current_tournament', 'options');

            self::$current = new Tournament($tournament);
        }

        return self::$current;
    }

    /**
     * Find specific tournament
     *
     * @param  int|string $id
     * @return Tournament
     */
    public static function find($id)
    {
        $term = get_term($id);

        return new Tournament($term);
    }

    /**
     * Find specific tournament by slug
     *
     * @param  int|string $slug
     * @return Tournament
     */
    public static function findBySlug($slug)
    {
        $term = get_term_by('slug', $slug, 'tournament');

        return new Tournament($term);
    }

    /**
     * Get all tournaments
     *
     * @param  int|string $id
     * @return Collection
     */
    public static function all()
    {
        $tournaments = new Collection;
        $terms       = get_terms('tournament', []);

        if ($terms) {
            foreach ($terms as $term) {
                $tournaments->push(new Tournament($term));
            }
        }

        return $tournaments->sortByDesc('name');
    }

    /**
     * Get standings table for current tournament
     *
     * @param  array  $options
     * @return Collection
     */
    public static function standings($options = [])
    {
        return Standings::get($options);
    }

    /**
     * Get leaders for tournament
     *
     * @param  array  $options
     * @return Collection
     */
    public static function leaders($options = [])
    {
        if (! self::$leaders) {
            $limit = (int) array_get($options, 'limit', 10);
            $leaders = [];
            $tournament = Tournaments::current();
            $matches = $tournament->matches(false);

            /** @var Match $match */
            foreach ($matches as $match) {
                if ($match->isOver()) {
                    // Get all players
                    $matchPlayers = $match->players();

                    /** @var Player $matchPlayer */
                    foreach ($matchPlayers as &$matchPlayer) {
                        if ($matchPlayer->id() && ! isset($leaders[$matchPlayer->id()])) {
                            $leaders[$matchPlayer->id()] = [
                                'id'      => $matchPlayer->id(),
                                'name'    => $matchPlayer->name(),
                                'team'    => null,
                                'photo'   => '',
                                'matches' => 1,
                                'total'   => $matchPlayer->score,
                                'avg'     => 0,
                                'history' => [
                                    [
                                        'id'    => $match->id(),
                                        'title' => $match->title(),
                                        'total' => $matchPlayer->score,
                                    ]
                                ],
                            ];
                        } elseif ($matchPlayer->id() && isset($leaders[$matchPlayer->id()])) {
                            $leaders[$matchPlayer->id()]['matches']++;
                            $leaders[$matchPlayer->id()]['total']    += $matchPlayer->score;
                            $leaders[$matchPlayer->id()]['avg']       = 0;
                            $leaders[$matchPlayer->id()]['history'][] = [
                                'id'    => $match->id(),
                                'title' => $match->title(),
                                'total' => $matchPlayer->score,
                            ];
                        }
                    }
                }
            }

            // Calculate averages
            foreach ($leaders as &$leader) {
                $leader['avg'] = $leader['matches'] > 0 ? round($leader['total'] / $leader['matches'], 1) : 0;
            }

            // Order the leaders, and take by limit
            usort($leaders, function($a, $b) {
                return $a['total'] < $b['total'];
            });
            $leaders = array_slice($leaders, 0, $limit);

            // Add photos and teams
            foreach ($leaders as &$leader) {
                $leader['photo'] = get_field('player_photo', $leader['id']);

                // Teams
                $teams = get_field('teams', $leader['id']);
                if ($teams && isset($teams[0])) {
                    $leader['team'] = new Team($teams[0]);
                }

                // Check photo
                if (! $leader['photo']) {
                    $leader['photo'] = $leader['team']->logo();
                }
            }

            // And make collection
            self::$leaders = new Collection($leaders);
        }

        return self::$leaders;
    }


    /**
     * Get three point leaders for tournament
     *
     * @param  array $options
     * @return Collection
     */
    public static function threePointLeaders($options = [])
    {
        if (! self::$threePointLeaders) {
            $limit = (int) array_get($options, 'limit', 10);
            $leaders = [];
            $tournament = Tournaments::current();
            self::$threePointLeaders = new Collection;

            // Get the correct post
            $standingsPage = Standings::getPost($tournament->slug);

            if ($standingsPage) {
                // Get the field for current tournament
                $leaders = get_field('3pt_leaders', $standingsPage->ID);

                if ($leaders) {
                    foreach ($leaders as $leader) {
                        self::$threePointLeaders->push([
                            'player'          => new Player($leader['player']),
                            'shots_made'      => (int) $leader['shots_made'],
                            'shots_attempted' => (int) $leader['shots_attempted'],
                        ]);
                    }

                    self::$threePointLeaders->sortByDesc('shots_made');
                }
            }
        }

        return self::$threePointLeaders->sortByDesc('shots_made')->take($limit);
    }

    /**
     * Point history for a player in a tournament
     *
     * @param  int $playerId
     * @return Collection
     */
    public static function playerPointHistory($playerId)
    {
        $history = new Collection;
        $matches = Matches::latest(['posts_per_page' => 100, 'order' => 'asc']);

        /** @var Match $match */
        foreach ($matches as $match) {
            // Get all players
            $matchPlayers = $match->players();

            /** @var Player $matchPlayer */
            foreach ($matchPlayers as &$matchPlayer) {
                if ($matchPlayer->id() === $playerId) {
                    $history->push((object) ['label' => $match->date('%d.%m.'), 'points' => $matchPlayer->score()]);
                }
            }
        }

        return $history;
    }
}
