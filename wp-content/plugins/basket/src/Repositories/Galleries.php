<?php

namespace Creolab\Basket\Plugin\Repositories;

use Creolab\Basket\Plugin\Models\Gallery;
use Creolab\Basket\Plugin\Models\Match;
use Tightenco\Collect\Support\Collection;

class Galleries extends BaseRepository
{
    /**
     * Find a gallery by the id
     *
     * @param  int  $id
     * @return Gallery
     */
    public static function find($id)
    {
        $gallery = null;

        // Try match gallery first
        $match = Matches::find($id);

        if ($match) {
            $gallery = new Gallery([
                'id'     => $match->id(),
                'title'  => $match->title(),
                'date'   => get_field('match_date', $match->id()),
                'photos' => get_field('photos', $match->id()),
            ]);
        }

        return $gallery;
    }

    public static function all()
    {
        $all = new Collection;

        // First find match galleries
        $matchGalleries = self::matchGalleries();

        // And merge them
        $all = $all->merge($matchGalleries);

        return $all;
    }

    public static function matchGalleries()
    {
        $galleries  = new Collection;
        $tournament = Tournaments::current();
        $matches    = Matches::all();

        /** @var Match $match */
        foreach ($matches as $match) {
            $galleryPhotos = get_field('photos', $match->id());

            if ($galleryPhotos) {
                // And add to collection
                $galleries->push(new Gallery([
                    'id'     => $match->id(),
                    'title'  => $match->title(),
                    'date'   => get_field('match_date', $match->ID),
                    'photos' => $galleryPhotos,
                ]));
            }
        }

        return $galleries;
    }

    public static function matchGallery($matchId)
    {
        return new Gallery;
    }


}
