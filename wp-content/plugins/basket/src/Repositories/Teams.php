<?php

namespace Creolab\Basket\Plugin\Repositories;

use Creolab\Basket\Plugin\Models\Award;
use Creolab\Basket\Plugin\Models\Coach;
use Creolab\Basket\Plugin\Models\Match;
use Creolab\Basket\Plugin\Models\Player;
use Creolab\Basket\Plugin\Models\Team;
use Tightenco\Collect\Support\Collection;

class Teams
{
    /**
     * Find a team by it's ID
     *
     * @param  int    $teamId
     * @param  array  $options
     * @return Team
     */
    public static function find($teamId, $options = [])
    {
        $teams = self::get($options);

        foreach ($teams as $team) {
            if ($teamId == $team->ID) {
                return $team;
            }
        }
    }

    /**
     * Get teams for current tournament
     *
     * @param  array  $options
     * @return Collection
     */
    public static function get($options = [])
    {
        $teams = new Collection;

        // We need the current tournament
        $tournament = Tournaments::current();

        // Default args
        $args = [
            'post_type'      => 'team',
            'posts_per_page' => -1,
            'tax_query'      => [['taxonomy' => 'tournament', 'field' => 'term_id', 'terms' => [$tournament->id()], 'include_children' => false]],
            'orderby' => 'post_title',
            'order' => 'asc',
        ];

        // Merge it
        $args = array_merge($args, $options);

        // Fetch the posts
        $posts = get_posts($args);

        // Create objects
        if ($posts) {
            foreach ($posts as $post) {
                $teams->push(new Team($post));
            }
        }

        return $teams;
    }

    /**
     * Return all players from team
     *
     * @param  int    $teamId
     * @param  array  $options
     * @return Collection
     */
    public static function players($teamId, $options = [])
    {
        // Get the team first
        $players     = new Collection;
        $playerItems = get_field('players', $teamId);

        if ($playerItems) {
            foreach ($playerItems as $playerItem) {
                $players->push(new Player($playerItem));
            }
        }

        return $players;
    }

    /**
     * Return all coaches from team
     *
     * @param  int    $teamId
     * @param  array  $options
     * @return Collection
     */
    public static function coaches($teamId, $options = [])
    {
        // Get the team first
        $coaches    = new Collection;
        $coachItems = get_field('trainers', $teamId);

        if ($coachItems) {
            foreach ($coachItems as $coachItem) {
                $coaches->push(new Coach($coachItem));
            }
        }

        return $coaches;
    }

    /**
     * Get scoring leaders for team
     *
     * @param  int    $teamId
     * @param  array  $options
     * @return Collection
     */
    public static function leaders($teamId, $options = [])
    {
        $limit      = (int) array_get($options, 'limit', 10);
        $leaders    = new Collection;
        $tournament = \Creolab\Basket\Plugin\Repositories\Tournaments::current();
        $matches    = Matches::latest(['posts_per_page' => 100], [$tournament->slug]);

        /** @var Match $match */
        foreach ($matches as $match) {
            // Only matches that have the requested team
            if ($teamId === $match->homeTeam->ID || $teamId === $match->awayTeam->ID) {
                // Get all players
                $matchPlayers = $match->players();

                /** @var Player $matchPlayer */
                foreach ($matchPlayers as &$matchPlayer) {
                    // Check the team
                    if ($matchPlayer->team() && $matchPlayer->team()->ID === $teamId) {
                        // Check if player is already in collection
                        $matchedLeaders = $leaders->filter(function ($item) use ($matchPlayer) {
                            return $item->id() === $matchPlayer->id();
                        });

                        // Update matches played
                        $matchPlayer->matchesPlayed++;

                        // Update the score
                        if ($matchedLeaders->count()) {
                            $matchedLeaders->first()->matchesPlayed++;
                            $matchedLeaders->first()->score += $matchPlayer->score;
                        } else {
                            $leaders->push($matchPlayer);
                        }

                        // And also update the average score
                        $matchPlayer->calculateAverageScore();
                    }
                }
            }
        }

        // And finally sort the results
        if ($leaders->count()) {
            // And order the leaders by the score
            $leaders = $leaders->sort(function($a, $b) {
                if ((int) $a->score === (int) $b->score) {
                    return 0;
                }

                return ((int) $a->score < (int) $b->score) ? 1 : -1;
            });
        }

        // We also need to add players without any games played
        $players = self::players($teamId, $options = []);

        // Loop through all, and add those not in the collection
        /** @var Player $player */
        foreach ($players as $player) {
            $exists = $leaders->where('ID', $player->id())->first();

            if (! $exists) {
                $leaders->push($player);
            }
        }

        // And limit results
        $leaders = $leaders->take($limit);

        return $leaders;
    }

    /**
     * Find last match for team
     *
     * @param  int  $id
     * @return Match
     */
    public static function lastMatch($id)
    {
        // Prepare match
        $match         = null;
        $latestMatches = Matches::latest(['posts_per_page' => 50]);
        $team          = self::find($id);

        // Find match for team
        if ($latestMatches->count()) {
            /** @var Match $latestMatch */
            foreach ($latestMatches as $latestMatch) {
                if ($latestMatch->homeTeam->id() === $team->id() || $latestMatch->awayTeam->id() === $team->id()) {
                    return $latestMatch;
                }
            }
        }

        return $match;
    }

    /**
     * Point history prepared for chats
     *
     * @param  int $id
     * @return Collection
     */
    public static function pointHistory($id, $tournamentId = null)
    {
        // Prepare match
        $history    = new Collection;
        $match      = null;
        $team       = self::find($id);

        // Get matches
        if ($tournamentId) {
            $matches = Matches::latest([
                'posts_per_page' => 50,
                'order' => 'asc',
                'limit' => 200,
                'tournament_id' => $tournamentId,
            ]);
        } else {
            $matches = Matches::allInTournament([
                'posts_per_page' => 50,
                'order' => 'asc',
                'limit' => 200,
            ]);
        }


        if ($matches->count()) {
            /** @var Match $match */
            foreach ($matches as $match) {
                if ($match->isOver()) {
                    if ($match->homeTeam->id() === $team->id()) {
                        $history->push((object) ['label' => $match->date('%Y.%d.%m.'), 'points' => $match->homeTeamScore(), 'id' => $match->id()]);
                    } elseif ($match->awayTeam->id() === $team->id()) {
                        $history->push((object) ['label' => $match->date('%Y.%d.%m.'), 'points' => $match->awayTeamScore(), 'id' => $match->id()]);
                    }
                }
            }
        }

        return $history;
    }

    /**
     * Find all upcoming matches for team
     *
     * @param  int  $id
     * @return Collection
     */
    public static function upcomingMatches($id)
    {
        $matches = new Collection;
        $allUpcoming = Matches::upcoming(['posts_per_page' => 50]);

        if ($allUpcoming->count()) {
            /** @var Match $upcomingMatch */
            foreach ($allUpcoming as $upcomingMatch) {
                if ($upcomingMatch->homeTeam->id() === $id || $upcomingMatch->awayTeam->id() === $id) {
                    $matches->push($upcomingMatch);
                }
            }
        }

        return $matches;
    }

    /**
     * Get all awards the team has earned
     *
     * @return Collection
     */
    public static function awards($teamId)
    {
        $awards = new Collection;

        if (have_rows('team_awards', $teamId)) {
            while (have_rows('team_awards', $teamId)) {
                the_row();
                $awardCollection = get_sub_field('award');

                foreach ($awardCollection as $awardCollectionItem) {
                    $awards->push(new Award($awardCollectionItem));
                }
            }
        }

        return $awards;
    }
}
