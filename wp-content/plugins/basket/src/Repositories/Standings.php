<?php

namespace Creolab\Basket\Plugin\Repositories;

use Creolab\Basket\Plugin\Models\Match;
use Creolab\Basket\Plugin\Models\Standing;
use Creolab\Basket\Plugin\Models\StandingsCollection;
use Creolab\Basket\Plugin\Models\Team;
use Tightenco\Collect\Support\Collection;

class Standings extends BaseRepository
{
    /**
     * Get standings for latest tournament
     *
     * @param  array  $options
     * @return Collection
     */
    public static function get($options = [])
    {
        $standings  = new StandingsCollection;
        $tournament = Tournaments::current();

        // Get all teams in tournament
        $teams = Teams::get(['tournament_id' => $tournament->id()]);

        // Get all matches in tournament
        $matches = Matches::allInTournament(['tournament_id' => $tournament->id()]);

        // Loop through matches and assign points to teams
        /** @var Match $match */
        foreach ($matches as $match) {
            /** @var Team $team */
            foreach ($teams as &$team) {
                if ($match->isOver() && ($team->id() === $match->homeTeam->id() || $team->id() === $match->awayTeam->id())) {
                    // Add a match
                    $team->addMatches(1);

                    // Add score
                    if ($team->id() === $match->homeTeam->id()) {
                        $team->addScore($match->homeTeamScore());
                        $team->addOpponentScore($match->awayTeamScore());
                    } elseif ($team->id() === $match->awayTeam->id()) {
                        $team->addScore($match->awayTeamScore());
                        $team->addOpponentScore($match->homeTeamScore());
                    }

                    // Winner
                    if ($match->winner() && $team->id() === $match->winner()->id()) {
                        $team->addPoints(2);
                        $team->addWins(1);
                    }

                    // Loser
                    if ($match->loser() && $team->id() === $match->loser()->id()) {
                        $team->addLosses(1);
                        $team->addPoints(1);
                    }

                    // Draw
                    if ($match->isDraw()) {
                        $team->addPoints(1);
                    }
                }
            }
        }

        // Order teams by points and add required attributes
        $teams = $teams->sortByDesc(function($team) {
            /** @var Team $team */
            return $team->points();
        });

        // Now create standings collection
        foreach ($teams as $team) {
            $standings->push(new Standing([
                'points'          => $team->points(),
                'wins'            => $team->wins(),
                'losses'          => $team->losses(),
                'draws'           => $team->draws(),
                'matches'         => $team->matches(),
                'score'           => $team->score(),
                'opponentScore'   => $team->opponentScore(),
                'scoreDifference' => $team->scoreDifference(),
                'team'            => $team,
            ]));
        }

        $standings = $standings->multiOrderBy([
            ['column' => 'points',          'order' => 'desc'],
            ['column' => 'scoreDifference', 'order' => 'desc'],
        ]);

        return $standings;
    }

    /**
     * Get standings post
     *
     * @return WP_Post
     */
    public static function getPost($tournament)
    {
        // Fetch the post
        $args = [
            'post_type'      => 'standing',
            'posts_per_page' => 1,
            'tax_query' => [
                [
                    'taxonomy' => 'tournament',
                    'field'    => 'slug',
                    'terms'    => [$tournament],
                ]
            ]
        ];

        // Fetch
        $posts = get_posts($args);

        if ($posts && isset($posts[0])) {
            return $posts[0];
        }
    }
}
