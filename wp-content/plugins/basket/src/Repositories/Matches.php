<?php

namespace Creolab\Basket\Plugin\Repositories;

use Creolab\Basket\Plugin\Models\Coach;
use Creolab\Basket\Plugin\Models\Referee;
use Creolab\Basket\Plugin\Models\Match;
use Creolab\Basket\Plugin\Models\Player;
use Tightenco\Collect\Support\Collection;

class Matches extends BaseRepository
{
    protected static $latest;

    protected static $latestInTournament = [];

    /**
     * Find match by ID
     *
     * @param  int  $id
     * @return Match|null
     */
    public static function find($id)
    {
        // Prepare match
        $match = null;

        // Fetch the post
        $post = get_post($id);

        // Create new match object
        if ($post) {
            $match = new Match($post);
        }

        return $match;
    }

    /**
     * Return last match played
     *
     * @return Match
     */
    public static function last($options)
    {
        // Prepare match
        $match = null;

        // Fetch the post
        $args = [
            'post_type'      => 'match',
            'orderby'        => 'date',
            'order'          => 'desc',
            'posts_per_page' => 1,
            'meta_query'     => [[
                'key'     => 'match_finished',
                'compare' => '!=',
                'value'   => 0,
            ]],
        ];
        $posts = get_posts($args);

        // Create new match object
        foreach ($posts as $post) {
            $match = new Match($post);
        }

        return $match;
    }

    public static function lastInTournament($options = [])
    {
        // Prepare match
        $match = null;

        // Get tournament ID
        $tournamentId = (int) array_get($options, 'tournament_id');

        // Fetch the post
        $args = [
            'post_type'      => 'match',
            'orderby'        => 'date',
            'order'          => 'desc',
            'posts_per_page' => 1,
            'meta_query'     => [[
                'key'     => 'match_finished',
                'compare' => '!=',
                'value'   => 0,
            ]],
            'tax_query'      => [['taxonomy' => 'tournament', 'field' => 'term_id', 'terms' => [$tournamentId], 'include_children' => false]],
        ];
        $posts = get_posts($args);

        // Create new match object
        foreach ($posts as $post) {
            $match = new Match($post);
        }

        return $match;
    }

    /**
     * Get latest matches in tournament
     *
     * @param  array $options
     * @return Collection
     */
    public static function latest($options = [], $tournaments = [])
    {
        // Setup cache key
        $key   = null;
        $cache = self::$latest;
        if ($tournaments) {
            $key   = implode(',', (array) $tournaments);
            $cache = self::$latestInTournament[$key] ?? null;
        }

        if (! $cache) {
            // Prepare
            $matches = new Collection;

            // Fetch the post
            $args = [
                'post_type'      => 'match',
                'posts_per_page' => isset($options['limit']) ? (int) $options['limit'] : 50,
                'meta_query'     => [[
                    'key'     => 'match_date',
                    'compare' => '<',
                    'value'   => date('Y-m-d H:i:s'),
                ]],
                'orderby'			=> 'meta_value',
                'order'				=> 'desc'
            ];


            // Add tags if needed
            if ($tournaments) {
                $args['tax_query'] = [
                    [
                        'taxonomy' => 'tournament',
                        'field'    => 'slug',
                        'terms'    => $tournaments,
                    ]
                ];
            }

            // Merge args
            $args  = array_merge($args, $options);
            $posts = get_posts($args);

            foreach ($posts as $post) {
                $matches->push(new Match($post));
            }

            self::$latest = $matches;
        }

        return self::$latest;
    }

    /**
     * Get latest matches in tournament
     *
     * @param  array $options
     * @return Collection
     */
    public static function upcoming($options = [])
    {
        // Prepare
        $matches = new Collection;

        // Fetch the posts
        $args = [
            'post_type'      => 'match',
            'posts_per_page' => 50,
            'meta_query'     => [[
                'key'     => 'match_date',
                'compare' => '>',
                'value'   => date('Y-m-d H:i:s'),
            ]],
            'orderby'			=> 'meta_value',
            'order'				=> 'asc'
        ];
        $args  = array_merge($args, $options);
        $posts = get_posts($args);

        foreach ($posts as $post) {
            $matches->push(new Match($post));
        }

        return $matches;
    }

    /**
     * Get latest matches in tournament
     *
     * @param  array $options
     * @return Collection
     */
    public static function allForLiveEdit($options = [])
    {
        // Prepare
        $matches = new Collection;

        // Fetch the posts
        $args = [
            'post_type'      => 'match',
            'posts_per_page' => 200,
            'meta_query'     => [[
                'key'     => 'match_date',
                'compare' => '>',
                'value'   => date('Y-m-d H:i:s'),
            ]],
            'orderby'			=> 'meta_value',
            'order'				=> 'asc'
        ];
        $args  = array_merge($args, $options);
        $posts = get_posts($args);

        foreach ($posts as $post) {
            $matches->push(new Match($post));
        }

        // Now also get past matches
        $args['meta_query'] = [[
            'key'     => 'match_date',
            'compare' => '<',
            'value'   => date('Y-m-d H:i:s'),
        ]];
        $posts = get_posts($args);
        foreach ($posts as $post) {
            $matches->push(new Match($post));
        }

        return $matches;
    }

    /**
     * All the matches
     *
     * @return Collection
     */
    public static function all()
    {
        $matches = new Collection;

        // Get posts for tournament taxonomy
        $posts = get_posts([
            'post_type'      => 'match',
            'posts_per_page' => -1,
            'status'         => 'publish',
        ]);

        // Push to collection
        if ($posts) {
            foreach ($posts as $post) {
                $matches->push(new Match($post));
            }
        }

        return $matches;
    }

    /**
     * Return all matches in a tournament
     *
     * @param  array  $options
     * @return Collection
     */
    public static function allInTournament($options = [], $withRelations = true)
    {
        $matches = new Collection;

        // Get tournament ID
        $tournamentId = (int) array_get($options, 'tournament_id');

        // Current tournament
        if (! $tournamentId) {
            $tournament   = Tournaments::current();
            $tournamentId = $tournament->id();
        }

        // Get posts for tournament taxonomy
        $posts = get_posts([
            'post_type'      => 'match',
            'posts_per_page' => -1,
            'status'         => 'publish',
            'tax_query'      => [['taxonomy' => 'tournament', 'field' => 'term_id', 'terms' => [$tournamentId], 'include_children' => false]],
            'orderby'        => 'match_date',
            'order'          => array_get($options, 'order', 'desc')
        ]);

        // Push to collection
        if ($posts) {
            foreach ($posts as $post) {
                $matches->push(new Match($post, $withRelations));
            }
        }

        return $matches;
    }

    /**
     * Get all players in match
     *
     * @param  int  $id
     * @return Collection
     */
    public static function players($id)
    {
        // Prepare
        $players    = new Collection;
        $match      = self::find($id);

        if ($match->isOver()) {
            $allPlayers = get_field('player_scores', $id);

            // Fill collection
            foreach ($allPlayers as $teamPlayers) {
                if ($teamPlayers) {
                    foreach ($teamPlayers as $teamPlayer) {
                        if ($teamPlayer && isset($teamPlayer['player'])) {
                            $playerItem = new Player($teamPlayer['player']);
                            $playerItem->score = (int) $teamPlayer['score'];

                            // And push to collection
                            $players->push($playerItem);
                        }
                    }
                }
            }
        }

        return $players;
    }

    /**
     * Get leaders in match
     *
     * @param  int  $id
     * @return Collection
     */
    public static function leaders($id)
    {
        // First get all players
        $leaders = new Collection;
        $players = self::players($id);

        // Then sort them
        if ($players->count()) {
            // And order the leaders by the score
            $leaders = $players->sort(function($a, $b) {
                if ((int) $a->score === (int) $b->score) {
                    return 0;
                }

                return ((int) $a->score < (int) $b->score) ? 1 : -1;
            });
        }

        return $leaders;
    }

    /**
     * Return all referees for match
     *
     * @param  int    $teamId
     * @param  array  $options
     * @return Collection
     */
    public static function referees($teamId, $options = [])
    {
        // Get the team first
        $referees    = new Collection;
        $refereeItems = get_field('match_referees', $teamId);

        if ($refereeItems) {
            foreach ($refereeItems as $refereeItem) {
                $referees->push(new Referee($refereeItem));
            }
        }

        return $referees;
    }
}
