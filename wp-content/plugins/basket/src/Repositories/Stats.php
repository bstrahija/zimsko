<?php

namespace Creolab\Basket\Plugin\Repositories;

use Creolab\Basket\Plugin\Db\Tables;
use Creolab\Basket\Plugin\Services\Cache;
use Tightenco\Collect\Support\Collection;

/**
 * This class is used to build and cache stats for better speed
 *
 * @package Creolab\Basket\Plugin\Repositories
 */
class Stats
{
    /**
     * Default cache TTL is 1 day
     *
     * @var int
     */
    protected static $ttl = 1440;

    /**
     * Get leaders for tournament
     *
     * @TODO: Cache this!!!
     *
     * @param  array  $options
     * @return Collection
     */
    public static function tournamentLeaders($options = [])
    {
        $cacheKey = 'tournament_leaders';

        return Cache::remember($cacheKey, self::$ttl, function() use ($options) {
            return Tournaments::leaders($options);
        });
    }

    /**
     * Get standings for latest tournament
     *
     * @param  array  $options
     * @return Collection
     */
    public static function standings($options = [])
    {
        $cacheKey = 'standings';

        return Cache::remember($cacheKey, self::$ttl, function() use ($options) {
            return Standings::get($options);
        });
    }

    public static function newStandings($options = [])
    {
        global $wpdb;

        $tournament = Tournaments::current();

        // Get all teams in tournament
        $teamsWp    = Teams::get(['tournament_id' => $tournament->id()]);
        $teamsWpIds = $teamsWp->pluck('ID');
        $teams      = $wpdb->get_results("SELECT * FROM ".Tables::get('teams')." WHERE wp_id IN (".implode(',', $teamsWpIds->toArray()).")");

        // Get all matches in tournament
        $event   = $wpdb->get_row("SELECT * FROM ".Tables::get('events')." WHERE wp_id = {$tournament->id()} LIMIT 1");
        $matches = $wpdb->get_results("SELECT * FROM ".Tables::get('matches')." WHERE event_id = {$event->id} AND status = 'finished'");

        $standingsData = [];
        foreach ($teams as $team) {
            $teamData                   = [];
            $teamData['id']             = $team->id;
            $teamData['wp_id']          = $team->wp_id;
            $teamData['title']          = $team->title;
            $teamData['matches']        = (int) $wpdb->get_var("SELECT COUNT(*) FROM ".Tables::get('matches')." WHERE (home_team_id = {$team->id} OR away_team_id = {$team->id}) AND event_id = {$event->id} AND status = 'finished'");
            $teamData['wins']           = (int) $wpdb->get_var("SELECT COUNT(*) FROM ".Tables::get('matches')." WHERE (home_team_id = {$team->id} OR away_team_id = {$team->id}) AND event_id = {$event->id} AND status = 'finished' AND winner_id = {$team->id}");
            $teamData['losses']         = (int) $wpdb->get_var("SELECT COUNT(*) FROM ".Tables::get('matches')." WHERE (home_team_id = {$team->id} OR away_team_id = {$team->id}) AND event_id = {$event->id} AND status = 'finished' AND winner_id != {$team->id}");
            $teamData['points_diff']    = (int) $wpdb->get_var("SELECT points_diff FROM ".Tables::get('team_stats')." WHERE team_id = {$team->id} AND event_id = {$event->id} AND type = 'event'");
            $teamData['ranking_points'] = ($teamData['wins'] * 2) + ($teamData['losses'] * 1);
            $teamData['win_percentage'] = $teamData['wins'] ? $teamData['wins'] / ($teamData['wins'] + $teamData['losses']) : 0;

            // Get head-to-head record
            $teamData['head_to_head'] = [];

            foreach ($matches as $match) {
                if ($match->home_team_id == $team->id) {
                    $teamData['head_to_head'][$match->away_team_id] = $match->winner_id == $team->id ? 1 : 0;
                } elseif ($match->away_team_id == $team->id) {
                    $teamData['head_to_head'][$match->home_team_id] = $match->winner_id == $team->id ? 1 : 0;
                }
            }

            $standingsData[] = $teamData;
        }

        // Sort the teams by win percentage, then by point difference, then by head-to-head record
        usort($standingsData, function($a, $b) {
            if ($b['ranking_points'] === $a['ranking_points']) {
                /*if ($b['points_diff'] === $a['points_diff']) {
                    // Assuming 'Team A' and 'Team B' are playing, compare their head-to-head record
                    return $b['head_to_head'][$a['team']] <=> $a['head_to_head'][$b['team']];
                }*/
                return $b['points_diff'] <=> $a['points_diff'];
            }
            return $b['ranking_points'] <=> $a['ranking_points'];
        });

        return $standingsData;
    }

    /**
     * Read stats from all games and update them in the cache
     *
     * @param  array  $options
     */
    public static function refresh($options = [])
    {
        Cache::clear(true);
    }
}
