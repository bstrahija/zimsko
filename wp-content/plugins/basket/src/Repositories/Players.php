<?php

namespace Creolab\Basket\Plugin\Repositories;

use Creolab\Basket\Plugin\Models\Match;
use Creolab\Basket\Plugin\Models\Player;

class Players
{
    /**
     * Find a player
     *
     * @param  int  $id
     * @return Player
     */
    public static function find($id)
    {
        $post = get_post($id);

        return new Player($post);
    }

    public static function allInTeam($options = [])
    {

    }

    public function totalPointsInTournament($id)
    {
        // Get all past matches in tournament
        $totalScore = 0;
        $matches    = Matches::latest(['posts_per_page' => 100]);

        if ($matches->count()) {
            /** @var Match $match */
            foreach ($matches as $match) {
                $players = Matches::players($match->id());

                /** @var Player $player */
                foreach ($players as $player) {
                    if ($player->id() === $id) {
                        $totalScore += $player->score();
                    }
                }
            }
        }

        return $totalScore;
    }

    /**
     * Return average score for a player throughout the tournament
     *
     * @param  int  $id
     * @param  int  $tournamentId
     * @return int
     */
    public static function averageScoreInTournament($id, $tournamentId = null)
    {
        // Get all past matches in tournament
        $totalScore    = 0;
        $matchesPlayed = 0;
        $matches       = Matches::latest(['posts_per_page' => 100]);

        if ($matches->count()) {
            /** @var Match $match */
            foreach ($matches as $match) {
                $players = Matches::players($match->id());

                /** @var Player $player */
                foreach ($players as $player) {
                    if ($player->id() === $id) {
                        $totalScore += $player->score();
                        $matchesPlayed++;
                    }
                }
            }
        }

        return $matchesPlayed ? round($totalScore / $matchesPlayed, 2) : 0;
    }
}
