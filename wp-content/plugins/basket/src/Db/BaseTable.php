<?php

namespace Creolab\Basket\Plugin\Db;

use function Basket\log;
use Creolab\Basket\Plugin\Db\Tables;

abstract class BaseTable
{
    /**
     * Table ID
     *
     * @var string
     */
    protected $id = '';

    /**
     * Table structure version
     *
     * @var string
     */
    protected $version = '';

    /**
     * @var array
     */
    protected $columns;

    /**
     * Empty upgrade function
     *
     * @return void
     */
    public function up() {}

    /**
     * Returns table name.
     *
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * Returns table name.
     *
     * @return string
     */
    public function name()
    {
        return Tables::get($this->id);
    }

    /**
     * Return DB table prefix. Handles multi-site tables.
     *
     * @return string
     */
    protected function prefix()
    {
        return Tables::prefix($this->id);
    }

    /**
     * Check if table schema needs updating by comparing current version and
     * version from options table.
     *
     * @return bool
     */
    protected function schemaNeedsUpdating()
    {
        return version_compare(get_option(Tables::schemaVersionOptionName($this->id), '0'), $this->version, '<');
    }

    /**
     * Save schema version as an entry in WP options table.
     *
     * @return bool
     */
    protected function saveSchemaVersion()
    {
        return update_option($this->schemaVersionOptionName(), $this->version, true);
    }

    /**
     * Get option name for schema version
     *
     * @return string
     */
    public function schemaVersionOptionName(): string
    {
        return Tables::schemaVersionOptionName($this->id);
    }

    /**
     * Return charset and collation setting from global WPDB object.
     *
     * @return string
     */
    protected function getCharsetCollate()
    {
        global $wpdb;

        return $wpdb->get_charset_collate();
    }

    /**
     * Simply return the version
     *
     * @return string
     */
    public function version(): string
    {
        return $this->version;
    }

    /**
     * Execute a migration query
     *
     * @param  string  $sql
     * @return MigrationResult
     */
    public function execute(string $sql, $dbDelta = false): MigrationResult
    {
        global $wpdb;

        // Init the result object
        $result = new MigrationResult($this->name());

        // Check if we're showing errors
        $showingErrors = $wpdb->show_errors;

        // Now disable errors
        $wpdb->show_errors(false);

        // And run the migration query
        if ($dbDelta) {
            $dbResult = dbDelta($sql);
        } else {
            $dbResult = $wpdb->query($sql);
        }

        // Fetch last error
        $lastError = $wpdb->last_error;

        // Check if it concerns our table
        if ($lastError && strpos($wpdb->last_query, $this->name()) !== false) {
            $result->setError($lastError);
        } elseif (! $dbResult) {
            $result->setError("Error when executing query: " . $sql);
        }

        // And set error to how they were
        $wpdb->show_errors($showingErrors);

        return $result;
    }

    /**
     * All DB table columns
     *
     * @return array
     */
    public function columns(): array
    {
        global $wpdb;

        if (! $this->columns) {
            $this->columns = $wpdb->get_results("SHOW COLUMNS FROM `".$this->name()."`");
        }

        return $this->columns;
    }

    /**
     * Check if column exists in DB table
     *
     * @param  string  $name
     * @return bool
     */
    public function columnExists(string $name): bool
    {
        global $wpdb;

        return (bool) $wpdb->get_var("SHOW COLUMNS FROM `".$this->name()."` LIKE '".$name."'");
    }

    /**
     * Default "down" action is to drop the table
     *
     * @return void
     */
    public function down()
    {
        $this->drop();
    }

    /**
     * Remove the table and the schema version
     *
     * @return void
     */
    public function drop()
    {
        global $wpdb;

        // First delete the table
        log('Deleting table: ' . $this->name());
        $wpdb->query("DROP TABLE IF EXISTS " . $this->name());

        // Then delete schema version from options
        log('Deleting schema version: ' . $this->schemaVersionOptionName());
        delete_option($this->schemaVersionOptionName());
    }
}
