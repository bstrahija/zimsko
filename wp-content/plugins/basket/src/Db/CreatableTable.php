<?php

namespace Creolab\Basket\Plugin\Db;

interface CreatableTable
{
    /**
     * Creates and updates table.
     *
     * @return void
     */
    public function up();

    /**
     * Resets the table
     *
     * @return void
     */
    public function down();

    /**
     * Drops the table
     *
     * @return void
     */
    public function drop();

    /**
     * Returns fully qualified table name.
     *
     * @return string
     */
    public function name();
}
