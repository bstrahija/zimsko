<?php

namespace Creolab\Basket\Plugin\Db;

/**
 * Simple helper class to handle database names
 */
class Tables
{
    /**
     * Prefix for all tables
     *
     * @var string
     */
    protected static $prefix = 'zmsk_';

    /**
     * Suffix for option name controlling schema version
     *
     * @var string
     */
    protected  static $optionVersionSuffix = '_schema_version';

    /**
     * Get a table name
     *
     * @param  string $key
     * @return string
     */
    public static function get($name): string
    {
        if ($name) {
            return self::prefix() . $name;
        }

        return '';
    }

    /**
     * Get name of option for the schema version
     *
     * @param  string  $name
     * @return string
     */
    public static function schemaVersionOptionName(string $name): string
    {
        if ($name) {
            return self::$prefix . $name . self::$optionVersionSuffix;
        }

        return '';
    }

    /**
     * Get table prefix
     *
     * @return string
     */
    public static function prefix(): string
    {
        global $wpdb;

        return $wpdb->get_blog_prefix() . self::$prefix;
    }

    /**
     * Get table name with the WordPress prefix
     *
     * @param  string  $name
     * @return string
     */
    public static function wp(string $name): string
    {
        global $wpdb;

        return $wpdb->prefix . $name;
    }

    /**
     * Check if a table exists in the DB
     *
     * @param  string $tableName
     * @return bool
     */
    public static function exists(string $tableName): bool
    {
        global $wpdb;

        return $wpdb->get_var("SHOW TABLES LIKE '$tableName'") === $tableName;
    }
}
