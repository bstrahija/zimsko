<?php

namespace Creolab\Basket\Plugin\Db\Migrations;

use function Basket\log;
use Creolab\Basket\Plugin\Db\BaseTable;
use Creolab\Basket\Plugin\Db\CreatableTable;

class CreateMatchRefereeTable extends BaseTable implements CreatableTable
{
    /**
     * @var string
     */
    protected $id = 'match_referee';

    /**
     * @var string
     */
    protected $version = '0.1';

    /**
     * Creates and updates table using "dbDelta" and cached table version name.
     *
     * @return void
     */
    public function up()
    {
        if ($this->schemaNeedsUpdating()) {
            // We need to "include" upgrade file to be able to use DB delta
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            $sql = "CREATE TABLE IF NOT EXISTS " . $this->name() . " (
                `id`                    int(20)       unsigned NOT NULL AUTO_INCREMENT,
                `match_id`              int(20)       unsigned NOT NULL,
                `referee_id`            int(20)       unsigned NOT NULL,
                `created_at`            datetime      DEFAULT NULL,
                `updated_at`            timestamp     DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `" . $this->id() . "_match_id_idx`  (`match_id`),
                KEY `" . $this->id() . "_referee_id_idx`   (`referee_id`)
            ) " . $this->getCharsetCollate() . ";";

            // Run query
            $result = $this->execute($sql);

            // Check result
            if (! $result->hasErrors()) {
                log("Created '".$this->id()."' table. " . $this->version());
                $this->saveSchemaVersion();
            } else {
                log("Error when creating '".$this->id()."' table. " . $result->error());
            }
        }
    }
}
