<?php

namespace Creolab\Basket\Plugin\Db\Migrations;

use function Basket\log;
use Creolab\Basket\Plugin\Db\BaseTable;
use Creolab\Basket\Plugin\Db\CreatableTable;

class CreateMatchesTable extends BaseTable implements CreatableTable
{
    /**
     * @var string
     */
    protected $id = 'matches';

    /**
     * @var string
     */
    protected $version = '0.1';

    /**
     * Creates and updates table using "dbDelta" and cached table version name.
     *
     * @return void
     */
    public function up()
    {
        if ($this->schemaNeedsUpdating()) {
            // We need to "include" upgrade file to be able to use DB delta
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            $sql = "CREATE TABLE IF NOT EXISTS " . $this->name() . " (
                `id`                    int(20)       unsigned NOT NULL AUTO_INCREMENT,
                `wp_id`                 int(20)       unsigned DEFAULT NULL,
                `event_id`              int(20)       unsigned DEFAULT NULL,
                `slug`                  varchar(255)  DEFAULT NULL,
                `title`                 varchar(255)  NOT NULL DEFAULT '',
                `body`                  text          DEFAULT NULL,
                `home_team_id`          int(20)       unsigned DEFAULT NULL,
                `away_team_id`          int(20)       unsigned DEFAULT NULL,
                `winner_id`             int(20)       unsigned DEFAULT NULL,
                `home_team_score`       int(20)       DEFAULT 0,
                `away_team_score`       int(20)       DEFAULT 0,
                `home_team_score_q1`    int(20)       DEFAULT 0,
                `away_team_score_q1`    int(20)       DEFAULT 0,
                `home_team_score_q2`    int(20)       DEFAULT 0,
                `away_team_score_q2`    int(20)       DEFAULT 0,
                `home_team_score_q3`    int(20)       DEFAULT 0,
                `away_team_score_q3`    int(20)       DEFAULT 0,
                `home_team_score_q4`    int(20)       DEFAULT 0,
                `away_team_score_q4`    int(20)       DEFAULT 0,
                `home_team_score_ot1`   int(20)       DEFAULT 0,
                `away_team_score_ot1`   int(20)       DEFAULT 0,
                `home_team_score_ot2`   int(20)       DEFAULT 0,
                `away_team_score_ot2`   int(20)       DEFAULT 0,
                `home_team_score_ot3`   int(20)       DEFAULT 0,
                `away_team_score_ot3`   int(20)       DEFAULT 0,
                `home_team_score_ot4`   int(20)       DEFAULT 0,
                `away_team_score_ot4`   int(20)       DEFAULT 0,
                `status`                varchar(100)  NOT NULL DEFAULT 'upcoming',
                `data`                  longtext      DEFAULT NULL,
                `scheduled_at`          datetime      DEFAULT NULL,
                `created_at`            datetime      NOT NULL,
                `updated_at`            timestamp     NOT NULL,
                `deleted_at`            datetime      DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `" . $this->id() . "_event_idx`  (`event_id`),
                KEY `" . $this->id() . "_home_team_idx`  (`home_team_id`),
                KEY `" . $this->id() . "_away_team_idx` (`away_team_id`),
                KEY `" . $this->id() . "_slug_idx`     (`slug`),
                KEY `" . $this->id() . "_status_idx`   (`status`)
            ) " . $this->getCharsetCollate() . ";";

            // Run query
            $result = $this->execute($sql);

            // Check result
            if (! $result->hasErrors()) {
                log("Created '".$this->id()."' table. " . $this->version());
                $this->saveSchemaVersion();
            } else {
                log("Error when creating '".$this->id()."' table. " . $result->error());
            }
        }
    }
}
