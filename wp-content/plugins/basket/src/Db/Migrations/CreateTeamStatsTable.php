<?php

namespace Creolab\Basket\Plugin\Db\Migrations;

use function Basket\log;
use Creolab\Basket\Plugin\Db\BaseTable;
use Creolab\Basket\Plugin\Db\CreatableTable;

class CreateTeamStatsTable extends BaseTable implements CreatableTable
{
    /**
     * @var string
     */
    protected $id = 'team_stats';

    /**
     * @var string
     */
    protected $version = '0.1';

    /**
     * Creates and updates table using "dbDelta" and cached table version name.
     *
     * @return void
     */
    public function up()
    {
        if ($this->schemaNeedsUpdating()) {
            // We need to "include" upgrade file to be able to use DB delta
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            $sql = "CREATE TABLE IF NOT EXISTS " . $this->name() . " (
                `id`                    int(20)       unsigned NOT NULL AUTO_INCREMENT,
                `team_id`               int(20)       unsigned NOT NULL,
                `match_id`              int(20)       unsigned DEFAULT NULL,
                `event_id`              int(20)       unsigned DEFAULT NULL,
                `type`                  varchar(50)   DEFAULT 'match',
                `points`                int(20)       DEFAULT 0,
                `points_avg`            float(20)     DEFAULT 0,
                `points_diff`           int(20)       DEFAULT 0,
                `assists`               int(20)       DEFAULT 0,
                `assists_avg`           float(20)     DEFAULT 0,
                `rebounds`              int(20)       DEFAULT 0,
                `rebounds_avg`          float(20)     DEFAULT 0,
                `blocks`                int(20)       DEFAULT 0,
                `blocks_avg`            float(20)     DEFAULT 0,
                `turnovers`             int(20)       DEFAULT 0,
                `turnovers_avg`         float(20)     DEFAULT 0,
                `three_pointers`        int(20)       DEFAULT 0,
                `three_pointers_avg`    float(20)     DEFAULT 0,
                `created_at`            datetime      DEFAULT NULL,
                `updated_at`            timestamp     DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `" . $this->id() . "_team_id_idx` (`team_id`),
                KEY `" . $this->id() . "_match_id_idx` (`match_id`),
                KEY `" . $this->id() . "_event_id_idx`   (`event_id`)
            ) " . $this->getCharsetCollate() . ";";

            // Run query
            $result = $this->execute($sql);

            // Check result
            if (! $result->hasErrors()) {
                log("Created '".$this->id()."' table. " . $this->version());
                $this->saveSchemaVersion();
            } else {
                log("Error when creating '".$this->id()."' table. " . $result->error());
            }
        }
    }
}
