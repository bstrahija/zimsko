<?php

namespace Creolab\Basket\Plugin\Db;

class MigrationResult
{
    /**
     * @var string|null
     */
    protected $error;

    /**
     * @var string
     */
    protected $table;

    /**
     * Init new result object
     *
     * @param $table
     */
    public function __construct($table)
    {
        $this->table = $table;
    }

    /**
     * Set table name
     *
     * @param  string  $table
     * @return void
     */
    public function setTable(string $table)
    {
        $this->table = $table;
    }

    /**
     * Return table name
     *
     * @return string
     */
    public function table(): string
    {
        return $this->table;
    }

    /**
     * Set error
     *
     * @param  mixed  $error
     * @return void
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * Return the error
     *
     * @return string|null
     */
    public function error()
    {
        return $this->error;
    }

    /**
     * Check for errors in result
     *
     * @return bool
     */
    public function hasErrors(): bool
    {
        return (bool) $this->error;
    }
}
