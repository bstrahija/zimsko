<?php

namespace Creolab\Basket\Plugin\Models;

use Creolab\Basket\Plugin\Repositories\Matches;
use Tightenco\Collect\Support\Collection;
use WP_Post;

class Tournament extends BaseModel
{
    protected $teams;

    protected $matches;

    /**
     * New match
     *
     * @param  mixed  $data
     */
    public function __construct($data)
    {
        // Add post
        parent::__construct((array) $data);

        // Add extra data
//        $this->teams     = new Collection(get_field('match_teams', $post->ID));
//        $this->results   = new Collection(get_field('match_results', $post->ID));
//        $this->homeTeam  = new Team($this->teams['home_team'][0]);
//        $this->homeScore = (int) $this->results['home_score'];
//        $this->awayTeam  = new Team($this->teams['away_team'][0]);
//        $this->awayScore = (int) $this->results['away_score'];
    }

    /**
     * Return all matches in a tournament
     *
     * @return Collection
     */
    public function matches($withRelations = true)
    {
        if (! $this->matches) {
            $this->matches = Matches::allInTournament(['tournament_id' => $this->id], $withRelations);
        }

        return $this->matches;
    }
}
