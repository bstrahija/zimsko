<?php

namespace Creolab\Basket\Plugin\Models;

class Standing extends BaseModel
{
    /**
     * Total points in standing
     *
     * @var int
     */
    protected $points = 0;

    /**
     * Number of matches played
     *
     * @var int
     */
    protected $matches = 0;

    /**
     * Total wins in standings
     *
     * @var int
     */
    protected $wins = 0;

    /**
     * Total losses in standings
     *
     * @var int
     */
    protected $losses = 0;

    /**
     * Total draws in standings
     *
     * @var int
     */
    protected $draws = 0;

    /**
     * Total score in standings
     *
     * @var int
     */
    protected $score = 0;

    /**
     * Total opponent score in standings
     *
     * @var int
     */
    protected $opponentScore = 0;

    /**
     * Total score difference in standings
     *
     * @var int
     */
    protected $scoreDifference = 0;

    /**
     * @var Team
     */
    protected $team;

    /**
     * New standing
     *
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->matches           = (int) array_get($data, 'matches');
        $this->points            = (int) array_get($data, 'points');
        $this->wins              = (int) array_get($data, 'wins');
        $this->losses            = (int) array_get($data, 'losses');
        $this->draws             = (int) array_get($data, 'draws');
        $this->score             = (int) array_get($data, 'score');
        $this->opponentScore     = (int) array_get($data, 'opponentScore');
        $this->scoreDifference   = (int) array_get($data, 'scoreDifference');
        $this->team              = array_get($data, 'team');

        parent::__construct($data);
    }

    public function team()
    {
        return $this->team;
    }

    public function points()
    {
        return $this->points;
    }

    public function matches()
    {
        return $this->matches;
    }

    public function wins()
    {
        return $this->wins;
    }

    public function losses()
    {
        return $this->losses;
    }

    public function draws()
    {
        return $this->draws;
    }

    public function addPoints($points = 1)
    {
        $this->points += $points;
    }

    public function score()
    {
        return $this->score;
    }

    public function opponentScore()
    {
        return $this->opponentScore;
    }

    public function scoreDifference()
    {
        return $this->score - $this->opponentScore;
    }

    public function addMatches($matches = 1)
    {
        $this->matches += $matches;
    }

    public function addWins($losses = 1)
    {
        $this->wins += $losses;
    }

    public function addLosses($losses = 1)
    {
        $this->losses += $losses;
    }

    public function addDraws($draws = 1)
    {
        $this->draws += $draws;
    }

    public function addScore($score = 1)
    {
        $this->score += $score;
    }

    public function addOpponentScore($score = 1)
    {
        $this->opponentScore += $score;
    }
}
