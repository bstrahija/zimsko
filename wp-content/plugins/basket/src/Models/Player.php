<?php

namespace Creolab\Basket\Plugin\Models;

use Creolab\Basket\Plugin\Repositories\Matches;
use Creolab\Basket\Plugin\Repositories\Tournaments;
use Tightenco\Collect\Support\Collection;
use WP_Post;

class Player extends BaseModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var Team
     */
    public $team;

    /**
     * @var int
     */
    public $score = 0;

    /**
     * @var int
     */
    public $matchesPlayed = 0;

    /**
     * @var int
     */
    public $matchesPlayedInTournament = 0;

    /**
     * @var int
     */
    public $averageScore = 0;

    /**
     * @var int
     */
    public $averageScoreInTournament = 0;

    /**
     * New match
     *
     * @param  WP_Post|int  $post
     */
    public function __construct($post)
    {
        if (is_int($post)) {
            $post = get_post($post);
        }

        // Add post
        parent::__construct((array) $post);
    }

    /**
     * Find current team for player
     *
     * @return Team
     */
    public function team()
    {
        $playerTeam = null;
        $teams      = get_field('teams', $this->ID);

        if ($teams && isset($teams[0])) {
            $playerTeam = new Team($teams[0]);
        }

        return $playerTeam;
    }

    public function name()
    {
        return $this->post_title;
    }

    public function photo()
    {
        $photo = get_field('player_photo', $this->ID);

        if (! $photo && $this->team()) {
            $photo = $this->team()->logo();
        }

        return $photo;
    }

    public function number()
    {
        return get_field('jersey_number', $this->ID);
    }

    public function position()
    {
        $positionData = get_field('position', $this->ID);

        if ($positionData && isset($positionData['label']) && $positionData['label']) {
            return $positionData['label'];
        }
    }

    /**
     * Get a list of matches the player has played
     *
     * @param  array  $options
     * @return Collection
     */
    public function matches($options = [])
    {
        $matches = new Collection;

        return $matches;
    }

    /**
     * Get number of matches played
     *
     * @param  array  $options
     * @return int
     */
    public function matchesPlayed($options = [])
    {
        $this->matchesPlayed = 0;
        $matches = Matches::latest();

        /** @var Match $match */
        foreach ($matches as $match) {
            $players = $match->players();

            if ($players && $players->count()) {
                /** @var Player $player */
                foreach ($players as $player) {
                    if ($player->id() === $this->id()) {
                        $this->matchesPlayed++;
                    }
                }
            }
        }

        return $this->matchesPlayed;
    }

    public function matchesPlayedInTournament($tournamentId = null)
    {
        if (! $tournamentId) {
            $tournament   = Tournaments::current();
            $tournamentId = $tournament->id();
        } else {
            $tournament = Tournaments::find($tournamentId);
        }

        $this->matchesPlayedInTournament = 0;

        $matches = Matches::latest(['limit' => 50], $tournament->slug);

        /** @var Match $match */
        foreach ($matches as $match) {
            $players = $match->players();

            if ($players && $players->count()) {
                /** @var Player $player */
                foreach ($players as $player) {
                    if ($player->id() === $this->id()) {
                        $this->matchesPlayedInTournament++;
                    }
                }
            }
        }

        return $this->matchesPlayedInTournament;
    }

    /**
     * Return points scored in a tournament
     *
     * @param  array  $options
     * @return int
     */
    public function pointsInTournament($options = [])
    {
        $this->score = 0;

        // Get matches
        if (array_get($options, 'tournament_id')) {
            $matches = Matches::latest([
                'posts_per_page' => 50,
                'order' => 'asc',
                'limit' => 200,
                'tournament_id' => array_get($options, 'tournament_id'),
            ], [array_get($options, 'tournament_id')]);
        } else {
            $matches = Matches::latest();
        }

        /** @var Match $match */
        foreach ($matches as $match) {
            /** @var Player $player */
            foreach ($match->players() as $player) {
                if ($player->id() === $this->id()) {
                    $this->score += $player->score();
                }
            }
        }

        return $this->score;
    }

    /**
     * Return average points scored in a tournament
     *
     * @param  array  $options
     * @return int
     */
    public function averagePoints($options = [])
    {
        $this->calculateAverageScore();

        return round($this->averageScore, 1);
    }

    /**
     * Return points scored in a match
     *
     * @param  array  $options
     * @return int
     */
    public function pointsInMatch($options = [])
    {
        return 10;
    }

    public function score()
    {
        return (int) $this->score;
    }

    public function averageScoreInTournament($tournamentId = null)
    {
        $this->calculateAverageScoreInTournament($tournamentId);

        return $this->averageScoreInTournament;
    }

    public function calculateAverageScore()
    {
        $this->averageScore = $this->matchesPlayed > 0 ? round($this->score / $this->matchesPlayed, 1) : 0;
    }

    public function calculateAverageScoreInTournament($tournamentId = null)
    {
        $matchesPlayed = $this->matchesPlayedInTournament($tournamentId);

        $this->averageScoreInTournament = $matchesPlayed > 0 ? round($this->score / $matchesPlayed, 1) : 0;
    }
}
