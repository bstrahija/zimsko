<?php

namespace Creolab\Basket\Plugin\Models;

use WP_Post;

class Team extends BaseModel
{
    /**
     * Total matches in standing
     *
     * @var int
     */
    protected $matches = 0;

    /**
     * Total points in standing
     *
     * @var int
     */
    protected $points = 0;

    /**
     * Total wins in standings
     *
     * @var int
     */
    protected $wins = 0;

    /**
     * Total score
     *
     * @var int
     */
    protected $score = 0;

    /**
     * Total opponent score
     *
     * @var int
     */
    protected $opponentScore = 0;

    /**
     * Total score difference
     *
     * @var int
     */
    protected $scoreDifference = 0;

    /**
     * Total losses in standings
     *
     * @var int
     */
    protected $losses = 0;

    /**
     * Total draws in standings
     *
     * @var int
     */
    protected $draws = 0;

    /**
     * New article
     *
     * @param  mixed  $data
     */
    public function __construct($data)
    {
        parent::__construct((array) $data);
    }

    public function team()
    {
        return $this->team;
    }

    public function logo()
    {
        return get_field('team_logo', $this->ID);
    }

    public function photo()
    {
        return get_field('team_photo', $this->ID);
    }

    public function location()
    {
        return get_field('location', $this->ID);
    }

    public function matches()
    {
        return $this->matches;
    }

    public function points()
    {
        return $this->points;
    }

    public function wins()
    {
        return $this->wins;
    }

    public function losses()
    {
        return $this->losses;
    }

    public function draws()
    {
        return $this->draws;
    }

    public function score()
    {
        return $this->score;
    }

    public function opponentScore()
    {
        return $this->opponentScore;
    }

    public function scoreDifference()
    {
        return $this->score - $this->opponentScore;
    }

    public function addMatches($matches = 1)
    {
        $this->matches += $matches;
    }

    public function addPoints($points = 1)
    {
        $this->points += $points;
    }

    public function addWins($wins = 1)
    {
        $this->wins += $wins;
    }

    public function addLosses($losses = 1)
    {
        $this->losses += $losses;
    }

    public function addDraws($draws = 1)
    {
        $this->draws += $draws;
    }

    public function addScore($score = 1)
    {
        $this->score += $score;
    }

    public function addOpponentScore($score = 1)
    {
        $this->opponentScore += $score;
    }
}
