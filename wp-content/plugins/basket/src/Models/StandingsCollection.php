<?php

namespace Creolab\Basket\Plugin\Models;

use Creolab\Basket\Plugin\Models\Match;
use Creolab\Basket\Plugin\Models\Standing;
use Creolab\Basket\Plugin\Models\Team;
use Tightenco\Collect\Support\Collection;

class StandingsCollection extends Collection
{
    public function multiOrderBy($instructions)
    {
        return $this->sort(function($a, $b) use ($instructions) {
            foreach($instructions as $instruction) {
                if (empty($instruction['order']) or strtolower($instruction['order']) == 'asc') {
                    $x = ($a->{$instruction['column']} <=> $b->{$instruction['column']});
                } else {
                    $x = ($b->{$instruction['column']} <=> $a->{$instruction['column']});
                }

                if ($x != 0) {
                    return $x;
                }
            }

            return 0;
        });
    }
}
