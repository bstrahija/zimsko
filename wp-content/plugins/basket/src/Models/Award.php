<?php

namespace Creolab\Basket\Plugin\Models;

use WP_Post;

class Award extends BaseModel
{
    /**
     * New match
     *
     * @param  array  $post
     */
    public function __construct($post)
    {
        parent::__construct((array) $post);
    }

    public function date($format = '%d.%m.%Y.')
    {
        return strftime($format, strtotime(get_field('award_date', $this->id())));
    }

    public function image()
    {
        return get_field('trophy_image', $this->id());
    }
}
