<?php

namespace Creolab\Basket\Plugin\Models;

use Tightenco\Collect\Support\Collection;
use WP_Post;

class Gallery
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $date;

    /**
     * @var Collection
     */
    protected $photos;

    /**
     * New article
     *
     * @param  array  $data
     */
    public function __construct($data)
    {
        $this->id     = array_get($data, 'id');
        $this->title  = array_get($data, 'title');
        $this->photos = new Collection((array) array_get($data, 'photos'));
        $this->date   = array_get($data, 'date');
    }

    public function id()
    {
        return $this->id;
    }

    public function title()
    {
        return $this->title;
    }

    public function image($size = 'large')
    {
        if ($this->photos->count()) {
            $photo = $this->photos->first();

            if ($photo && isset($photo['sizes'])&& isset($photo['sizes'][$size])) {
                return $photo['sizes'][$size];
            }
        }
    }

    public function photos()
    {
        return $this->photos;
    }

    public function permalink()
    {
        return home_url('galerije?id=' . $this->id());
    }

    /**
     * Return formatted date of match
     *
     * @param  string  $format
     * @return false|string
     */
    public function date($format = '%A %d.%m.%Y.')
    {
        return strftime($format, strtotime($this->date));
    }
}
