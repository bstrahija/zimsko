<?php

namespace Creolab\Basket\Plugin\Models;

use WP_Post;

class BaseModel extends DataObject
{
    /**
     * @var int
     */
    protected $id;

    /**
     * New model
     *
     * @param  mixed  $data
     */
    public function __construct($data)
    {
        $this->id = array_get((array) $data, 'ID');
        parent::__construct((array) $data);
    }

    public function id()
    {
        return $this->ID ?: $this->term_id;
    }

    public function title()
    {
        return $this->post_title ?: $this->name;
    }

    public function permalink()
    {
        return get_the_permalink($this->ID);
    }
}
