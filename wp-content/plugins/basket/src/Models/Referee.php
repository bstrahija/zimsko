<?php

namespace Creolab\Basket\Plugin\Models;

use WP_Post;

class Referee extends BaseModel
{
    /**
     * @var Match
     */
    public $match;

    /**
     * New referee
     *
     * @param  WP_Post  $post
     */
    public function __construct(WP_Post $post)
    {
        // Add post
        parent::__construct((array) $post);
    }

    /**
     * Find current match for referee
     *
     * @return Match
     */
    public function match()
    {
        $this->match;
    }

    public function name()
    {
        return $this->post_title;
    }
}
