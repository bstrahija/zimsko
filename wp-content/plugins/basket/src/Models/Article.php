<?php

namespace Creolab\Basket\Plugin\Models;

use WP_Post;

class Article extends BaseModel
{
    /**
     * New article
     *
     * @param  WP_Post  $post
     */
    public function __construct(WP_Post $post)
    {
        parent::__construct((array) $post);
    }
}
