<?php

namespace Creolab\Basket\Plugin\Models;

use Creolab\Basket\Plugin\Repositories\Matches;
use Creolab\Basket\Plugin\Repositories\Teams;
use Tightenco\Collect\Support\Collection;
use WP_Post;

class Match extends BaseModel
{
    /**
     * @var Collection
     */
    public $teams;

    /**
     * @var Collection
     */
    public $results;

    /**
     * @var Team
     */
    public $homeTeam;

    /**
     * @var int
     */
    public $homeScore;

    /**
     * @var Team
     */
    public $awayTeam;

    /**
     * @var int
     */
    public $awayScore;

    /**
     * @var Collection
     */
    public $quarterScores;

    /**
     * New match
     *
     * @param  WP_Post  $post
     */
    public function __construct(WP_Post $post, $loadRelations = true)
    {
        // Add post
        parent::__construct((array) $post);

        // Always load results
        $this->results   = new Collection(get_field('match_results', $post->ID));
        $this->homeScore = (int) $this->results['home_score'];
        $this->awayScore = (int) $this->results['away_score'];

        // Load relations if needed
        if ($loadRelations) {
            $this->loadRelations($post);
        }
    }

    /**
     * Load match relations if needed
     *
     * @param  WP_POST  $post
     * @return void
     */
    public function loadRelations($post)
    {
        // Add extra data
        $this->teams     = new Collection(get_field('match_teams', $post->ID));
        $this->homeTeam  = new Team($this->teams['home_team'][0]);
        $this->awayTeam  = new Team($this->teams['away_team'][0]);

        // Also add quarter scores
        $q1Scores = get_field('match_results_q1', $post->ID);
        $q2Scores = get_field('match_results_q2', $post->ID);
        $q3Scores = get_field('match_results_q3', $post->ID);
        $q4Scores = get_field('match_results_q4', $post->ID);
        $p1Scores = get_field('match_results_p1', $post->ID);
        $this->quarterScores = new Collection([
            'home_q1' => array_get((array) $q1Scores,  'home_score_q1'),
            'away_q1' => array_get((array) $q1Scores,  'away_score_q1'),
            'home_q2' => array_get((array) $q2Scores,  'home_score_q2'),
            'away_q2' => array_get((array) $q2Scores,  'away_score_q2'),
            'home_q3' => array_get((array) $q3Scores,  'home_score_q3'),
            'away_q3' => array_get((array) $q3Scores,  'away_score_q3'),
            'home_q4' => array_get((array) $q4Scores,  'home_score_q4'),
            'away_q4' => array_get((array) $q4Scores,  'away_score_q4'),
            'home_p1' => array_get((array) $p1Scores,  'home_score_p1'),
            'away_p1' => array_get((array) $p1Scores,  'away_score_p1'),
        ]);
    }

    /**
     * Check if match is over
     *
     * @return bool
     */
    public function isOver()
    {
        return $this->homeTeamScore() > 0 || $this->awayTeamScore() > 0;
    }

    public function status()
    {
        if (get_field('match_finished', $this->ID)) {
            return 'finished';
        }

        return 'upcoming';
    }

    public function hadOvertime()
    {
        return $this->homeTeamOvertimeScore() != '' || $this->awayTeamOvertimeScore() != '';
    }

    public function homeTeam()
    {
        return get_the_title($this->homeTeam->ID);
    }

    public function awayTeam()
    {
        return get_the_title($this->awayTeam->ID);
    }

    /**
     * Return score for home team
     *
     * @return int
     */
    public function homeTeamScore()
    {
        return (int) $this->homeScore;
    }

    public function winningTeamScore()
    {
        if ($this->isWinner('home')) {
            return $this->homeScore;
        }

        if ($this->isWinner('away')) {
            return $this->awayScore;
        }

        return 0;
    }

    public function losingTeamScore()
    {
        if ($this->isWinner('home')) {
            return $this->awayScore;
        }

        if ($this->isWinner('away')) {
            return $this->homeScore;
        }

        return 0;
    }

    /**
     * Return score for home team in quarter
     *
     * @param  int $quarter
     * @return int
     */
    public function homeTeamQuarterScore($quarter)
    {
        return $this->quarterScores->get('home_q' . $quarter);
    }

    /**
     * Return score for overtime
     *
     * @return int
     */
    public function homeTeamOvertimeScore()
    {
        return $this->quarterScores->get('home_p1');
    }

    /**
     * Return score for away team
     *
     * @return int
     */
    public function awayTeamScore()
    {
        return (int) $this->awayScore;
    }

    /**
     * Return score for away team in quarter
     *
     * @param  int $quarter
     * @return int
     */
    public function awayTeamQuarterScore($quarter)
    {
        return $this->quarterScores->get('away_q' . $quarter);
    }

    /**
     * Return score for away team in overtime
     *
     * @return int
     */
    public function awayTeamOvertimeScore()
    {
        return $this->quarterScores->get('away_p1');
    }

    /**
     * Match winner
     *
     * @return Team|null
     */
    public function winner()
    {
        if ($this->homeScore > $this->awayScore) {
            return $this->homeTeam;
        }

        if ($this->awayScore > $this->homeScore) {
            return $this->awayTeam;
        }
    }

    /**
     * Match loser
     *
     * @return Team
     */
    public function loser()
    {
        if ($this->homeScore > $this->awayScore) {
            return $this->awayTeam;
        }

        if ($this->awayScore > $this->homeScore) {
            return $this->homeTeam;
        }
    }

    /**
     * Check if home/away team is the winner
     *
     * @param  string  $team
     * @return bool
     */
    public function isWinner($team = 'home')
    {
        if ($team === 'home') {
            return $this->homeScore > $this->awayScore;
        }

        if ($team === 'away') {
            return $this->homeScore < $this->awayScore;
        }
    }

    /**
     * Check if match is a draw
     *
     * @return bool
     */
    public function isDraw()
    {
        return $this->homeScore === $this->awayScore;
    }

    /**
     * Return formatted date of match
     *
     * @param  string  $format
     * @return false|string
     */
    public function date($format = '%A %d.%m.%Y.')
    {
        return strftime($format, strtotime(get_field('match_date', $this->ID)));
    }

    /**
     * Return formatted date of match
     *
     * @param  string  $format
     * @return false|string
     */
    public function dateAndTime($format = '%A %d.%m.%Y. %H:%M')
    {
        return strftime($format, strtotime(get_field('match_date', $this->ID)));
    }

    /**
     * Return date and time for countdown script
     *
     * @return false|string
     */
    public function countdownTime()
    {
        return date('F d, Y H:i:s', strtotime(get_field('match_date', $this->ID)));
    }

    /**
     * Return formatted time of match
     *
     * @param  string  $format
     * @return false|string
     */
    public function time($format = '%H:%M')
    {
        return strftime($format, strtotime(get_field('match_date', $this->ID)));
    }

    /**
     * Return all players in matches
     *
     * @return Collection
     */
    public function players()
    {
        return Matches::players($this->id());
    }

    public function liveDetails()
    {
        $details = [
            'id'     => $this->id(),
            'title'  => $this->title(),
            'status' => $this->status(),
            'homeTeam' => [
                'title' => $this->homeTeam->title(),
                'logo'  => $this->homeTeam->logo(),
                'players' => [
                    ['name' => 'Test', 'pts' => 0],
                ],
            ],
            'awayTeam' => [
                'title' => $this->awayTeam->title(),
                'logo'  => $this->awayTeam->logo(),
                'players' => [
                    ['name' => 'Test', 'pts' => 0],
                ],
            ],
            'homeScore' => (int) $this->homeScore,
            'awayScore' => (int) $this->awayScore,
            'quarterScores' => [
                'home_q1' => (int) $this->homeTeamQuarterScore(1),
                'away_q1' => (int) $this->awayTeamQuarterScore(1),
                'home_q2' => (int) $this->homeTeamQuarterScore(2),
                'away_q2' => (int) $this->awayTeamQuarterScore(2),
                'home_q3' => (int) $this->homeTeamQuarterScore(3),
                'away_q3' => (int) $this->awayTeamQuarterScore(3),
                'home_q4' => (int) $this->homeTeamQuarterScore(4),
                'away_q4' => (int) $this->awayTeamQuarterScore(4),
                'home_ot1' => (int) $this->homeTeamOvertimeScore(1),
                'away_ot1' => (int) $this->awayTeamOvertimeScore(1),
                'home_ot2' => (int) $this->homeTeamOvertimeScore(2),
                'away_ot2' => (int) $this->awayTeamOvertimeScore(2),
                'home_ot3' => (int) $this->homeTeamOvertimeScore(3),
                'away_ot3' => (int) $this->awayTeamOvertimeScore(3),
                'home_ot4' => (int) $this->homeTeamOvertimeScore(4),
                'away_ot4' => (int) $this->awayTeamOvertimeScore(4),
            ],
            'referees' => [

            ],
        ];

        return $details;
    }
}
