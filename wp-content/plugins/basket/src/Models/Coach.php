<?php

namespace Creolab\Basket\Plugin\Models;

use Creolab\Basket\Plugin\Repositories\Tournaments;
use Tightenco\Collect\Support\Collection;
use WP_Post;

class Coach extends BaseModel
{
    /**
     * @var Team
     */
    public $team;

    /**
     * New match
     *
     * @param  WP_Post  $post
     */
    public function __construct(WP_Post $post)
    {
        // Add post
        parent::__construct((array) $post);
    }

    /**
     * Find current team for player
     *
     * @return Team
     */
    public function team()
    {
        $playerTeam = null;
        $teams      = get_field('teams', $this->ID);

        if ($teams && isset($teams[0])) {
            $playerTeam = new Team($teams[0]);
        }

        return $playerTeam;
    }

    public function name()
    {
        return $this->post_title;
    }
}
