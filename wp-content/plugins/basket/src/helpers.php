<?php

namespace Basket;

use Creolab\Basket\Plugin\Repositories\Teams;

function teams($options = [])
{
    return Teams::get($options);
}

function player_photo($playerId = null)
{

}

function player_photo_url($playerId = null)
{

}

function player_jersey_number($playerId = null)
{

}

function team_logo($teamId = null)
{

}

function team_logo_url($teamId = null)
{

}

function team_photo($teamId = null)
{

}

function team_photo_url($teamId = null)
{

}

function log($message, $level = 'debug')
{
    if (true === WP_DEBUG) {
        if (is_array($message) || is_object($message)) {
            error_log('zmsk.' . strtoupper($level) . ' ' . print_r($message, true));
        } else {
            error_log('zmsk.' . strtoupper($level) . ' ' . $message);
        }
    }
}
