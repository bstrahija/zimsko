<?php

namespace Creolab\Basket\Plugin\Providers;

class BasketProvider
{
    /**
     * Init the Basket theme and plugin
     */
    public function init()
    {
        // Handle database upgrades
        add_action('admin_init', [new DatabaseProvider, 'run']);

        // Init admin interface
        (new AdminProvider())->init();

        // Initialize content structure
        (new ContentProvider)->init();

        // Init data provider for saving custom data
        add_action('admin_init', [new DataProvider, 'init']);
        // (new DataProvider())->init();

        // Enable featured images
        add_theme_support('post-thumbnails');



        // Add options page
        if (function_exists('\acf_add_options_page')) {
            \acf_add_options_page();
        }
    }
}
