<?php

namespace Creolab\Basket\Plugin\Providers;

use Creolab\Basket\Plugin\Services\Cache;

class ContentProvider
{
    /**
     * Define bidirectional sync for ACF relationship properties
     * eg.
     * ['name_of_cpt' => []]
     *
     * @var array
     */
    protected $syncRelationships = [
        // When saving tournament, then sync matches
        'tournament' => ['source_post_type' => 'tournament', 'source_field' => 'matches', 'target_post_type' => 'match', 'target_field' => 'tournament'],

        // When saving match, then sync tournaments
        'match' => ['source_post_type' => 'match', 'source_field' => 'tournament', 'target_post_type' => 'tournament', 'target_field' => 'matches'],


        'team_players_xxx' => [
            // When saving team, then sync players
            'players' => ['source_post_type' => 'team',   'target_post_type' => 'player', 'target_field' => 'teams'],

            // When saving player, then sync teams
            'teams'   => ['source_post_type' => 'player', 'target_post_type' => 'team',   'target_field' => 'players'],
        ],

        'tournament_xxx' => [
            // When saving tournament, then sync matches
            'matches' => ['source_post_type' => 'tournament', 'target_post_type' => 'match', 'target_field' => 'tournament'],

            // When saving match, then sync tournaments
            'tournaments' => ['source_post_type' => 'match', 'target_post_type' => 'tournament', 'target_field' => 'matches'],
        ],
    ];

    /**
     * Initialize content structure
     */
    public function init()
    {
        /* Register a hook to fire only when the "my-cpt-slug" post type is saved */
        add_action('post_updated', function($postId) {
            Cache::clear(true);
        });

        // Register custom post type and taxonomies
        add_action('init', [$this, 'registerPostTypes']);
        add_action('init', [$this, 'registerTaxonomies']);
        add_action('init', [$this, 'registerCustomFields']);

        // Sync ACF relationships
//        add_filter('acf/update_value', [$this, 'syncAcfRelationships'], 10, 3);
    }

    /**
     * Register custom post types here
     */
    public function registerPostTypes()
    {
        $configPath = BKT_PLUGIN_PATH . '/config/cpt/';
        $configs = scandir($configPath);

        if ($configs) {
            foreach ($configs as $configFile) {
                if (is_file($configPath . $configFile)) {
                    $config = include($configPath . $configFile);

                    if ($config) {
                        register_post_type($config['name'], $config);
                    }
                }
            }
        }
    }

    /**
     * Register taxonomies here
     */
    public function registerTaxonomies()
    {
        $configPath = BKT_PLUGIN_PATH . '/config/tax/';
        $configs = scandir($configPath);

        if ($configs) {
            foreach ($configs as $configFile) {
                if (is_file($configPath . $configFile)) {
                    $config = include($configPath . $configFile);

                    if ($config) {
                        register_taxonomy($config['name'], $config['post_types'], $config);
                    }
                }
            }
        }
    }

    /**
     * Register custom fields
     */
    public function registerCustomFields()
    {
        $configPath = BKT_PLUGIN_PATH . '/config/fields/';
        $configs = scandir($configPath);

        if ($configs) {
            foreach ($configs as $configFile) {
                if (is_file($configPath . $configFile)) {
                    include($configPath . $configFile);
                }
            }
        }
    }

    /**
     * Bidirectional syncing of relations
     *
     * @param  mixed  $value
     * @param  int    $postId
     * @param  mixed  $field
     * @return mixed
     */
    public function syncAcfRelationships($value, $postId, $field)
    {
        if ($field && $field['type'] === 'relationship') {
            $sourcePostId = (int) $postId;
            $sourcePost   = get_post($sourcePostId);
            $sourcePostType = $sourcePost->post_type;

            // Loop through all configured relations
            // if (isset($config[$field['name']])) {
            foreach ($this->syncRelationships as $postType => $config) {
                if ($postType === $sourcePostType) {
                    $targetPostType  = $this->syncRelationships[$postType]['target_post_type'];
                    $targetFieldName = $this->syncRelationships[$postType]['target_field'];
                    echo '<pre>'; print_r($targetPostType); echo '</pre>';
                    echo '<pre>'; print_r($targetFieldName); echo '</pre>';

                    /*if ($targetPostType && $targetFieldName) {
                        // Find all posts of the target post type
                        $targetPosts = get_posts(['post_type' => $targetPostType, 'posts_per_page' => -1, 'post_status' => ['publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash']]);

                        // Loop through all the target posts
                        foreach ($targetPosts as $targetPost) {
                            // Get existing relations
                            $existingRelations = (array) get_field($targetFieldName, $targetPost->ID);
                            $relationsIds      = array_map(function($element) { return $element->ID; }, $existingRelations);

                            if (in_array($targetPost->ID, (array) $value)) {
        //                            echo '<pre>'; print_r("Add  " . $sourcePostId . " to " . $targetPost->post_title); echo '</pre>';
                                $relationsIds[] = $sourcePostId;
                            } else {
        //                            echo '<pre>'; print_r("Remove " . $sourcePostId . " from " . $targetPost->post_title); echo '</pre>';
                                if (($key = array_search($sourcePostId, $relationsIds)) !== false) {
                                    unset($relationsIds[$key]);
                                }
                            }

                            // Make the array unique
                            $relationsIds = array_unique($relationsIds);

                            // And update the post meta value
                            update_post_meta($targetPost->ID, $targetFieldName, $relationsIds);
                        }
                    }*/
                }
            }
        }
        die();

        return $value;
    }
}
