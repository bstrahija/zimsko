<?php

namespace Creolab\Basket\Plugin\Providers;

use WP_Post;

use function Basket\log;
use Creolab\Basket\Plugin\Db\Tables;
use Creolab\Basket\Plugin\Data\SyncTeam;
use Creolab\Basket\Plugin\Data\SyncCoach;
use Creolab\Basket\Plugin\Data\SyncEvent;
use Creolab\Basket\Plugin\Data\SyncMatch;
use Creolab\Basket\Plugin\Data\SyncPlayer;
use Creolab\Basket\Plugin\Data\SyncReferee;

class DataProvider
{
    /**
     * Init data syncing to custom tables
     *
     * @return void
     */
    public function init()
    {
        add_action('saved_term', [$this, 'saveEvent'], 10, 3);
        add_action('save_post', [$this, 'savePost'], 10, 3);
    }

    /**
     * Sync event/tournament data to custom table
     *
     * @param  int $termId
     * @return void
     */
    public function saveEvent($termId, $test = null)
    {
        global $wpdb;

        $term = get_term($termId);

        if ($term && $term->taxonomy == 'tournament') {
            (new SyncEvent)->sync($term);
        }
    }

    /**
     * Sync post data to custom tables
     *
     * @param  int     $postId
     * @param  WP_Post $post
     * @return void
     */
    public function savePost($postId, $post)
    {
        if ($post) {
            try {
                if ($post->post_type == 'team') {
                    (new SyncTeam)->sync($post);
                } elseif ($post->post_type == 'match') {
                    (new SyncMatch)->sync($post);
                } elseif ($post->post_type == 'player') {
                    (new SyncPlayer)->sync($post);
                } elseif ($post->post_type == 'coach') {
                    (new SyncCoach)->sync($post);
                } elseif ($post->post_type == 'referee') {
                    (new SyncReferee)->sync($post);
                }
            } catch(\Exception $e) {
                log('[DATA] Failed to update post: ' . $post->post_title . ' / ' . $e->getMessage(), 'error');
            }
        }
    }

    public function savePlayer($post)
    {

    }

    public function saveCoach($post)
    {

    }

    public function saveReferee($post)
    {

    }
}
