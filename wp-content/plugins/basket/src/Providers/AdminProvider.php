<?php

namespace Creolab\Basket\Plugin\Providers;

use Creolab\Basket\Plugin\Repositories\Teams;

class AdminProvider
{
    /**
     * Init admin interface
     */
    public function init()
    {
        $this->addPlayerColumns();
    }

    /**
     * Add player columns to admin
     *
     * @return void
     */
    public function addPlayerColumns()
    {
        // Add columns first
        add_filter('manage_player_posts_columns', function($columns) {
            return array_merge($columns, [
                'teams'         => __('Teams'),
                'position'      => __('Position'),
                'jersey_number' => __('Jersey'),
            ]);
        });

        // Then add the meta fields
        add_action ( 'manage_player_posts_custom_column', function($column, $postId) {
            switch ( $column ) {
                case 'teams':
                    echo get_player_team_names($postId);
                    break;
                case 'position':
                    echo get_player_short_position_name($postId);
                    break;
                case 'jersey_number':
                    echo get_post_meta ($postId, 'jersey_number', true);
                    break;
            }
        }, 10, 2 );
    }

    /**
     * Add filters for players
     */
    public function addPlayerFilters()
    {
        // Add dropdown
        add_action('restrict_manage_posts', function() {
            $type = isset($_GET['post_type']) ? $_GET['post_type'] : 'post';

            // Add filter for players
            if ('player' === $type){
                $teams = Teams::get();
                $values = [];

                foreach ($teams as $team) {
                    $values[$team->post_title] = $team->ID;
                }

                ?>
                <select name="basket_team">
                    <option value="">All teams</option>
                    <?php
                    $currentValue = isset($_GET['basket_team']) ? $_GET['basket_team'] : '';
                    foreach ($values as $label => $value) {
                        printf
                        (
                            '<option value="%s"%s>%s</option>',
                            $value,
                            $value == $currentValue ? ' selected="selected"' : '',
                            $label
                        );
                    }
                    ?>
                </select>
                <?php
            }
        });
    }

    /**
     * Filter players by meta data
     *
     * @return void
     */
    public function filterPlayers()
    {
        add_filter('parse_query', function($query) {
            global $pagenow;
            $type = isset($_GET['post_type']) ? $_GET['post_type'] : 'post';

            if ('player' === $type && is_admin() && $pagenow === 'edit.php' && isset($_GET['basket_team']) && $_GET['basket_team'] != '') {
                $query->query_vars['meta_key'] = 'teams';
                $query->query_vars['meta_value'] = $_GET['basket_team'];
            }
        });
    }
}
