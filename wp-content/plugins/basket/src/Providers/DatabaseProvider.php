<?php

namespace Creolab\Basket\Plugin\Providers;

use function Basket\log;

use Creolab\Basket\Plugin\Db\Migrations\CreateAwardsTable;
use Creolab\Basket\Plugin\Db\Migrations\CreateCoachesTable;
use Creolab\Basket\Plugin\Db\Migrations\CreateCoachTeamTable;
use Creolab\Basket\Plugin\Db\Migrations\CreateEventsTable;
use Creolab\Basket\Plugin\Db\Tables;
use Creolab\Basket\Plugin\Db\Migrations\CreateMatchesTable;
use Creolab\Basket\Plugin\Db\Migrations\CreateMatchRefereeTable;
use Creolab\Basket\Plugin\Db\Migrations\CreatePlayersTable;
use Creolab\Basket\Plugin\Db\Migrations\CreatePlayerStatsTable;
use Creolab\Basket\Plugin\Db\Migrations\CreatePlayerTeamTable;
use Creolab\Basket\Plugin\Db\Migrations\CreateRefereesTable;
use Creolab\Basket\Plugin\Db\Migrations\CreateTeamsTable;
use Creolab\Basket\Plugin\Db\Migrations\CreateTeamStatsTable;

class DatabaseProvider
{
    /**
     * Run all needed DB updates according to db version.
     *
     * @return void
     */
    public function run()
    {
        (new CreateEventsTable)->up();
        (new CreateTeamsTable)->up();
        (new CreatePlayersTable)->up();
        (new CreatePlayerTeamTable)->up();
        (new CreateMatchesTable)->up();
        (new CreateRefereesTable)->up();
        (new CreateCoachesTable)->up();
        (new CreateCoachTeamTable)->up();
        (new CreateAwardsTable)->up();
        (new CreatePlayerStatsTable)->up();
        (new CreateTeamStatsTable)->up();
        (new CreateMatchRefereeTable)->up();
    }

    /**
     * Remove all DB tables
     *
     * @return void
     */
    public function drop()
    {
        (new CreateMatchRefereeTable)->down();
        (new CreatePlayerStatsTable)->down();
        (new CreateTeamStatsTable)->down();
        (new CreateAwardsTable)->down();
        (new CreateCoachesTable)->down();
        (new CreateRefereesTable)->down();
        (new CreateMatchesTable)->down();
        (new CreatePlayersTable)->down();
        (new CreateTeamsTable)->down();
        (new CreateEventsTable)->down();
        (new CreateCoachesTable)->down();
        (new CreatePlayerTeamTable)->down();
    }

    /**
     * Deletes all tables and re-creates them
     *
     * @return void
     */
    public function reset()
    {
        log(' ==> Resetting database tables from DatabaseProvider', 'warning');

        // First delete all tables
        $this->drop();

        // Then re-create them from scratch
        $this->run();
    }
}
