<?php

return [
    'name'              => 'tournament',
    'post_types'        => ['team', 'match', 'award', 'standing', 'referee'],
    'hierarchical'      => false,
    'labels'            => [
        'name'          => __('Tournaments', 'basket'),
        'singular_name' => __('Tournament', 'basket'),
    ],
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => ['slug' => 'tournaments'],
];
