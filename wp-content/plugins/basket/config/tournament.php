<?php

return [

    'points_for_win' => 2,

    'points_for_draw' => 1,

    'points_for_loss' => 1,

];
