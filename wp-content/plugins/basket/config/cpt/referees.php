<?php

return [
    'name' => 'referee',
    'labels' => [
        'name'          => __('Referees', 'basket'),
        'singular_name' => __('Referee', 'basket'),
    ],
    'public'      => true,
    'has_archive' => true,
    'taxonomies' => [],
    'hierarchical' => false,
    'menu_position' => 25,
    'rewrite'     => ['slug' => 'suci'],
    'capability_type' => 'page',
    'supports' => ['title'],
];
