<?php

return [
    'name' => 'team',
    'labels' => [
        'name'          => __('Teams', 'basket'),
        'singular_name' => __('Team', 'basket'),
    ],
    'public'      => true,
    'has_archive' => true,
    'taxonomies' => [],
    'hierarchical' => false,
    'menu_position' => 22,
    'rewrite'     => ['slug' => 'ekipe'],
    'capability_type' => 'page',
    'supports' => ['title'],
];
