<?php

return [
    'name' => 'award',
    'labels' => [
        'name'          => __('Awards', 'basket'),
        'singular_name' => __('Award', 'basket'),
    ],
    'public'      => true,
    'has_archive' => true,
    'taxonomies' => [],
    'hierarchical' => false,
    'menu_position' => 24,
    'rewrite'     => ['slug' => 'titule'],
    'capability_type' => 'page',
    'supports' => ['title'],
];
