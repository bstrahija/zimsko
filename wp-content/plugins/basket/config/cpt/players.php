<?php

return [
    'name' => 'player',
    'labels' => [
        'name'          => __('Players', 'basket'),
        'singular_name' => __('Player', 'basket'),
    ],
    'public'      => true,
    'has_archive' => true,
    'taxonomies' => [],
    'hierarchical' => false,
    'menu_position' => 24,
    'rewrite'     => ['slug' => 'igraci'],
    'capability_type' => 'page',
    'supports' => ['title'],
];
