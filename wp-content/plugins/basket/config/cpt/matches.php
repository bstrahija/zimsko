<?php

return [
    'name' => 'match',
    'labels' => [
        'name'          => __('Matches', 'basket'),
        'singular_name' => __('Match', 'basket'),
    ],
    'public'      => true,
    'has_archive' => true,
    'taxonomies' => [],
    'hierarchical' => false,
    'menu_position' => 26,
    'rewrite'     => ['slug' => 'tekme'],
    'capability_type' => 'page',
    'supports' => ['title'],
];
