<?php

return [
    'name' => 'standing',
    'labels' => [
        'name'          => __('Standings', 'basket'),
        'singular_name' => __('Standing', 'basket'),
    ],
    'public'      => true,
    'has_archive' => false,
    'taxonomies' => [],
    'hierarchical' => false,
    'menu_position' => 29,
    'rewrite'     => ['slug' => 'tablice'],
    'capability_type' => 'page',
    'supports' => ['title'],
];
