<?php

return [
    'name' => 'trainer',
    'labels' => [
        'name'          => __('Trainers', 'basket'),
        'singular_name' => __('Trainer', 'basket'),
    ],
    'public'      => true,
    'has_archive' => true,
    'taxonomies' => [],
    'hierarchical' => false,
    'menu_position' => 24,
    'rewrite'     => ['slug' => 'treneri'],
    'capability_type' => 'page',
    'supports' => ['title'],
];
