<?php

/**
 * Basket Plugin
 *
 * Plugin Name: Basket
 * Description: Manage your basketball tournaments
 * Plugin URI: https://creolab.hr/
 * Author: Boris Strahija, Creo
 * Version: 1.0.0
 * Author URI: https://creolab.hr/
 * Text Domain: basket
 * Domain Path: /lang
 */

// Add autoloader
require_once __DIR__.'/vendor/autoload.php';

define('BKT_PLUGIN_VERSION', '1.0.0');
define('BKT_PLUGIN_SLUG', plugin_basename(__FILE__));
define('BKT_PLUGIN_FILE', __FILE__);
define('BKT_PLUGIN_BASE', plugin_basename(BKT_PLUGIN_FILE));
define('BKT_PLUGIN_PATH', __DIR__);
define('BKT_PLUGIN_URL' , plugin_dir_url(__FILE__));
define('BKT_PLUGIN_PHP_VERSION', '7.0');

// Plugin activation and deactivation
register_activation_hook(__FILE__,   [\Creolab\Basket\Plugin\Providers\ActivationProvider::class, 'activate']);
register_deactivation_hook(__FILE__, [\Creolab\Basket\Plugin\Providers\ActivationProvider::class, 'deactivate']);
add_action('activated_plugin',       [\Creolab\Basket\Plugin\Providers\ActivationProvider::class, 'afterActivate']);
add_action('deactivated_plugin',     [\Creolab\Basket\Plugin\Providers\ActivationProvider::class, 'afterDeactivate']);

// Initialize the plugin
(new Creolab\Basket\Plugin\Providers\BasketProvider)->init();
