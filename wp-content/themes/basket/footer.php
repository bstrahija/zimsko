<?php if (get_template_name() !== 'tpl-game-edit.php') : ?>
    <!-- Footer
        ================================================== -->
    <footer id="footer" class="footer">

        <!-- Footer Widgets -->
        <div class="footer-widgets">
            <div class="footer-widgets__inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-lg-2">
                            <div class="footer-col-inner">
                                <!-- Footer Logo -->
                                <div class="footer-logo">
                                    <a href="<?php echo home_url() ?>" style="display: inline-block; width: 100%;"><img src="<?php echo get_template_directory_uri() ?>/assets/images/logo.png" srcset="<?php echo get_template_directory_uri() ?>/assets/images/logo@2x.png 2x" alt="Zimsko" class="footer-logo__img" style="width: 100%; max-width: 100px;"></a>
                                </div>
                                <!-- Footer Logo / End -->
                            </div>
                        </div>

                        <div class="col-sm-4 col-lg-3">
                            <div class="footer-col-inner">
                                <?php partial('partials/footer/posts') ?>
                            </div>
                        </div>

                        <div class="col-sm-4 col-lg-3">
                            <div class="footer-col-inner">
                                <?php partial('partials/footer/contact') ?>
                            </div>
                        </div>

                        <div class="col-sm-4 col-lg-3">
                            <div class="footer-col-inner">
                                <?php partial('partials/footer/instagram') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Widgets / End -->

        <?php partial('partials/footer/secondary') ?>
    </footer>
    <!-- Footer / End -->
</div>
<?php endif; ?>

<?php wp_footer(); ?>

<script>
    jQuery(function($) {
        /*new cookieNoticeJS({

            // Localizations of the notice message
            'messageLocales': {
                'en': 'Ove web stranice koriste kolačiće (tzv. cookies) kako bismo Vam omogućili najbolje iskustvo korištenja. Kliknite na \'Prihvaćam\' ako prihvaćate upotrebu kolačića i želite nastaviti koristiti ovu web stranicu.',
            },

            // Localizations of the dismiss button text
            'buttonLocales': {
                'en': 'Prihvaćam',
            },

            // Position for the cookie-notifier (default=bottom)
            'cookieNoticePosition': 'bottom',

            // Shows the "learn more button (default=false)
            'learnMoreLinkEnabled': false,

            // The href of the learn more link must be applied if (learnMoreLinkEnabled=true)
            'learnMoreLinkHref': '/izjava-o-privatnosti',

            // Text for optional learn more button
            'learnMoreLinkText': {
                'en': 'Saznaj više',
            },

            // The message will be shown again in X days
            'expiresIn': 30,

            // Specify a custom font family
            'fontFamily': 'inherit',

            // Dismiss button background color
            'buttonBgColor': '#1e78dd',

            // Dismiss button text color
            'buttonTextColor': '#eee',

            // Notice background color
            'noticeBgColor': '#1e2024',

            // Notice text color
            'noticeTextColor': '#eee',

            // the learnMoreLink color (default='#009fdd')
            'linkColor': '#f00',

            // The target of the learn more link (default='', or '_blank')
            'linkTarget': '',

            // Print debug output to the console (default=false)
            'debug': false
        });*/
    });
</script>

</body>
</html>
