<?php
/** Template Name: Results */

get_header(); ?>

<?php partial('partials/results/hero') ?>


<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-8">
                <?php partial('partials/results/latest_matches') ?>

                <hr>

                <?php partial('partials/global/upcoming_matches', ['limit' => 200]) ?>
            </div>
            <!-- Content / End -->

            <!-- Sidebar -->
            <div id="sidebar" class="sidebar col-lg-4">
                <?php  partial('partials/global/featured_news') ?>

                <?php  partial('partials/global/standings') ?>

                <?php  partial('partials/aside/tournament_leaders', ['limit' => 20]) ?>

                <?php partial('partials/aside/3pt_leaders', ['limit' => 20]) ?>

                <?php  partial('partials/aside/social') ?>
            </div>
            <!-- Sidebar / End -->
        </div>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
