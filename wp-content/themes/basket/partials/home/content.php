<?php wp_reset_query(); ?>

<?php while (have_posts()) : the_post(); ?>
    <article class="card card--lg post post--single">
        <header class="card__header">
            <h4>Info</h4>
        </header>

        <?php if (has_post_thumbnail()) : ?>
            <figure class="post__thumbnail">
                <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="" style="width: 100%;">
            </figure>
        <?php endif; ?>

        <div class="card__content">
            <?php the_content() ?>
        </div>
    </article>
<?php endwhile; ?>
