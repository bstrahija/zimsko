<!-- Hero Unit
    ================================================== -->
<div class="hero-unit" style="background-image: url(<?php the_field('homepage_hero_background', 'options'); ?>);  background-position: 50% 50%; position: relative;">
    <div class="container hero-unit__container">
        <div class="hero-unit__content hero-unit__content--left-center">
            <h5 class="hero-unit__subtitle">Zimsko košarkaško prvenstvo</h5>
            <h1 class="hero-unit__title"><span class="text-primary">Čakovec</span></h1>
<!--            <div class="hero-unit__desc"><p class="color: #fff;">Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore .</p></div>-->
<!--            <a href="#" class="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed hero-unit__btn">Read More <i class="fa fa-plus text-primary"></i></a>-->
        </div>

        <!-- <figure class="hero-unit__img">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/header_player.png" alt="Hero Unit Image">
        </figure> -->
    </div>

    <div class="video-bg-container">
        <iframe src="https://www.youtube.com/embed/2NXwPtblEbs?rel=0&listType=playlist" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

<style>
.video-bg-container {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
}

.video-bg-container iframe {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
}
</style>
