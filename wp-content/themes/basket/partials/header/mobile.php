<!-- Header Mobile -->
<div class="header-mobile clearfix" id="header-mobile">
    <div class="header-mobile__logo">
        <a href="<?php echo home_url() ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/images/logo_2024.png" srcset="<?php echo get_template_directory_uri() ?>/assets/images/logo_2024@2x.png 2x" alt="Zimsko" class="header-mobile__logo-img"></a>
    </div>
    <div class="header-mobile__inner">
        <a id="header-mobile__toggle" class="burger-menu-icon"><span class="burger-menu-icon__line"></span></a>
<!--        <span class="header-mobile__search-icon" id="header-mobile__search-icon"></span>-->
    </div>
</div>

<style>
.header-mobile__logo { top: 33px !important; }
</style>
