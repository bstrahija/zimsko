<!-- Header Desktop -->
<header class="header header--layout-1">

    <?php // partial('partials/header/top_bar') ?>

    <?php // partial('partials/header/secondary') ?>

    <!-- Header Primary -->
    <div class="header__primary">
        <div class="container">
            <div class="header__primary-inner">
                <!-- Header Logo -->
                <div class="header-logo">
                    <a href="<?php echo home_url() ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/images/logo_2024.png" alt="Zimsko" srcset="<?php echo get_template_directory_uri() ?>/assets/images/logo_2024@2x.png 2x" class="header-logo__img"></a>
                </div>
                <!-- Header Logo / End -->

                <?php partial('partials/header/navigation') ?>
            </div>
        </div>
    </div>
    <!-- Header Primary / End -->

    <nav class="sponsor-bar clearfix">
        <div class="container">
            <a href="https://www.facebook.com/burgerweekendcakovec/" target="_blank" style="background-image: url(<?php echo get_template_directory_uri() ?>/assets/images/bg_bw2024.jpg)">
                <img src="<?php echo get_template_directory_uri() ?>/assets/images/bg_bw2024.jpg" alt="Burger Weekend Čakovec">
            </a>
        </div>
    </nav>
</header>
<!-- Header / End -->

<style>
.sponsor-bar { text-align: center; background: #009dba; padding: 0; }
.sponsor-bar .container { max-width: 1200px; text-align: center; overflow: hidden; }
.sponsor-bar a { display: block; height: 90px; overflow: hidden; background-repeat: no-repeat; background-position: center; background-size: cover; }
.sponsor-bar img { display: none; }
@media (max-width: 767px) {
    .sponsor-bar a { height: 45px; }
}
</style>
