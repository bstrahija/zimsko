<?php

$matches = \Creolab\Basket\Plugin\Repositories\Matches::latest();

?>

<!-- Header Featured News
		================================================== -->
<div class="posts posts--carousel-featured featured-carousel">
    <?php foreach ($matches as $match) : ?>
        <div class="posts__item posts__item--category-1">
            <a href="<?php echo get_permalink($match->ID) ?>" class="posts__link-wrapper">
                <!-- Game Result -->
                <div class="game-result" style="padding-left: 20px; padding-right: 20px; min-height: 135px;">
                    <section class="game-result__section">
                        <div class="game-result__content">

                            <!-- 1st Team -->
                            <div class="game-result__team game-result__team--first">
                                <figure class="game-result__team-logo">
                                    <img src="<?php echo $match->homeTeam->logo() ?>" alt="" style="max-width: 40px">
                                </figure>
                                <div class="game-result__team-info">
                                    <h5 class="game-result__team-name" style="font-size: 14px;"><?php echo get_the_title($match->homeTeam->ID) ?></h5>
                                    <?php /*<div class="game-result__team-desc"><?php the_field('location', $match->homeTeam->ID) ?></div>*/ ?>
                                </div>
                            </div>
                            <!-- 1st Team / End -->

                            <div class="game-result__score-wrap">
                                <div class="game-result__score" style="font-size: 26px;">
                                    <span class="game-result__score-result game-result__score-result--<?php echo $match->isWinner('home') ? 'winner' : 'loser' ?>"><?php echo $match->homeTeamScore() ?></span>
                                    <span class="game-result__score-dash">-</span>
                                    <span class="game-result__score-result game-result__score-result--<?php echo $match->isWinner('away') ? 'winner' : 'loser' ?>"><?php echo $match->awayTeamScore() ?></span>
                                </div>
                            </div>

                            <!-- 2nd Team -->
                            <div class="game-result__team game-result__team--second">
                                <figure class="game-result__team-logo">
                                    <img src="<?php echo $match->awayTeam->logo() ?>" alt="" style="max-width: 40px">
                                </figure>
                                <div class="game-result__team-info">
                                    <h5 class="game-result__team-name" style="font-size: 14px;"><?php echo get_the_title($match->awayTeam->ID) ?></h5>
                                    <?php /*<div class="game-result__team-desc"><?php the_field('location', $match->awayTeam->ID) ?></div>*/ ?>
                                </div>
                            </div>
                            <!-- 2nd Team / End -->
                        </div>
                    </section>
                </div>
                <!-- Game Result / End -->
            </a>
        </div>
    <?php endforeach; ?>

</div>
