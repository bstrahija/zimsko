<!-- Main Navigation -->
<nav class="main-nav clearfix">
    <ul class="main-nav__list">
        <!-- <li class="<?php echo is_front_page() ? 'active' : null ?>"><a href="<?php echo home_url() ?>">Početna</a> -->
            <!-- <ul class="main-nav__sub">
                <li class="active"><a href="index.html">Home - version 1</a></li>
                <li class=""><a href="index-2.html">Home - version 2</a></li>
                <li class=""><a href="index-3.html">Home - version 3</a></li>
            </ul> -->
        </li>
        <li class="<?php echo is_page('novosti') ? 'active' : null ?>"><a href="<?php echo home_url('novosti') ?>">Novosti</a></li>
        <li class="<?php echo is_page('rezultati') ? 'active' : null ?>"><a href="<?php echo home_url('rezultati') ?>">Rezultati</a></li>
        <li class="<?php echo is_page('raspored') ? 'active' : null ?>"><a href="<?php echo home_url('raspored') ?>">Raspored</a></li>
        <li class="<?php echo is_post_type_archive('team') ? 'active' : null ?>">
            <a href="<?php echo home_url('ekipe') ?>">Ekipe <?php // echo gmdate('Y') ?></a>
            <ul class="main-nav__sub">
                <?php foreach (Basket\teams() as $team) : ?>
                    <li><a href="<?php the_permalink($team->ID); ?>"><?php echo $team->post_title ?></a></li>
                <?php endforeach; ?>
            </ul>
        </li>
        <!-- <li class="<?php echo is_page('arhiva') ? 'active' : null ?>">
            <a href="<?php echo home_url('arhiva') ?>">Arhiva</a>
            <ul class="main-nav__sub">
                <li class="active"><a href="<?php home_url('arhiva/zimsko-2019'); ?>">Zimsko 2019.</a></li>
            </ul>
        </li> -->
        <li class="<?php echo is_page('galerije') ? 'active' : null ?>"><a href="<?php echo home_url('galerije') ?>">Galerije</a></li>
        <li class="<?php echo is_page('poredak-i-pobjednici') ? 'active' : null ?>"><a href="<?php echo home_url('poredak-i-pobjednici') ?>">Povijest</a></li>
        <li class="<?php echo is_page('prijava') ? 'active' : null ?>"><a href="<?php echo home_url('prijava') ?>">Kontakt</a></li>

        <?php /*<li class=""><a href="#">Features</a>
            <div class="main-nav__megamenu clearfix">
                <ul class="col-lg-2 col-md-3 col-12 main-nav__ul">
                    <li class="main-nav__title">Features</li>
                    <li><a href="features-shortcodes.html">Shortcodes</a></li>
                    <li><a href="features-typography.html">Typography</a></li>
                    <li><a href="features-widgets-blog.html">Widgets - Blog</a></li>
                    <li><a href="features-widgets-shop.html">Widgets - Shop</a></li>
                    <li><a href="features-widgets-sports.html">Widgets - Sports</a></li>
                    <li><a href="features-404.html">404 Error Page</a></li>
                    <li><a href="features-search-results.html">Search Results</a></li>
                    <li><a href="page-contacts.html">Contact Us</a></li>
                </ul>
                <ul class="col-lg-2 col-md-3 col-12 main-nav__ul">
                    <li class="main-nav__title">Other Pages</li>
                    <li><a href="page-sponsors.html">Sponsors</a></li>
                    <li><a href="page-faqs.html">FAQs</a></li>
                    <li><a href="staff-single.html">Staff Member</a></li>
                    <li><a href="event-tournament.html">Tournament</a></li>
                    <li><a href="shop-list.html">Shop Page</a></li>
                    <li><a href="shop-cart.html">Shopping Cart</a></li>
                    <li><a href="shop-wishlist.html">Wishlist</a></li>
                    <li><a href="shop-checkout.html">Checkout</a></li>
                </ul>
                <div class="col-lg-4 col-md-3 col-12">

                    <div class="posts posts--simple-list posts--simple-list--lg">
                        <div class="posts__item posts__item--category-1">
                            <div class="posts__inner">
                                <div class="posts__cat">
                                    <span class="label posts__cat-label">The Team</span>
                                </div>
                                <h6 class="posts__title"><a href="#">The team is starting a new power breakfast regimen</a></h6>
                                <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
                                <div class="posts__excerpt">
                                    Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </div>
                            </div>
                            <div class="posts__footer card__footer">
                                <div class="post-author">
                                    <figure class="post-author__avatar">
                                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/avatar-1.jpg" alt="Post Author Avatar">
                                    </figure>
                                    <div class="post-author__info">
                                        <h4 class="post-author__name">James Spiegel</h4>
                                    </div>
                                </div>
                                <ul class="post__meta meta">
                                    <li class="meta__item meta__item--likes"><a href="#"><i class="meta-like meta-like--active icon-heart"></i> 530</a></li>
                                    <li class="meta__item meta__item--comments"><a href="#">18</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-md-3 col-12">
                    <ul class="posts posts--simple-list">
                        <li class="posts__item posts__item--category-1">
                            <figure class="posts__thumb">
                                <a href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/post-img3-xs.jpg" alt=""></a>
                            </figure>
                            <div class="posts__inner">
                                <div class="posts__cat">
                                    <span class="label posts__cat-label">The Team</span>
                                </div>
                                <h6 class="posts__title"><a href="#">The new eco friendly stadium won a Leafy Award in 2016</a></h6>
                                <time datetime="2016-08-21" class="posts__date">August 21st, 2016</time>
                            </div>
                        </li>
                        <li class="posts__item posts__item--category-2">
                            <figure class="posts__thumb">
                                <a href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/post-img1-xs.jpg" alt=""></a>
                            </figure>
                            <div class="posts__inner">
                                <div class="posts__cat">
                                    <span class="label posts__cat-label">Injuries</span>
                                </div>
                                <h6 class="posts__title"><a href="#">Mark Johnson has a Tibia Fracture and is gonna be out</a></h6>
                                <time datetime="2016-08-23" class="posts__date">August 23rd, 2016</time>
                            </div>
                        </li>
                        <li class="posts__item posts__item--category-1">
                            <figure class="posts__thumb">
                                <a href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/post-img4-xs.jpg" alt=""></a>
                            </figure>
                            <div class="posts__inner">
                                <div class="posts__cat">
                                    <span class="label posts__cat-label">The Team</span>
                                </div>
                                <h6 class="posts__title"><a href="#">The team is starting a new power breakfast regimen</a></h6>
                                <time datetime="2016-08-21" class="posts__date">August 21st, 2016</time>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
        <li class=""><a href="#">The Team</a>
            <ul class="main-nav__sub">
                <li><a href="team-overview.html">Team</a>
                    <ul class="main-nav__sub-2">
                        <li><a href="team-overview.html">Overview</a></li>
                        <li><a href="team-roster-2.html">Roster</a>
                            <ul class="main-nav__sub-2">
                                <li><a href="team-roster-1.html">Roster - 1</a></li>
                                <li><a href="team-roster-2.html">Roster - 2</a></li>
                                <li><a href="team-roster-3.html">Roster - 3</a></li>
                            </ul>
                        </li>
                        <li><a href="team-standings.html">Standings</a></li>
                        <li><a href="team-last-results.html">Latest Results</a></li>
                        <li><a href="team-schedule.html">Schedule</a></li>
                        <li><a href="team-gallery.html">Gallery</a>
                            <ul class="main-nav__sub-3">
                                <li><a href="team-gallery-album.html">Single Album</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="player-overview.html">Player</a>
                    <ul class="main-nav__sub-2">
                        <li><a href="player-overview.html">Overview</a></li>
                        <li><a href="player-stats.html">Full Statistics</a></li>
                        <li><a href="player-bio.html">Biography</a></li>
                        <li><a href="player-news.html">Related News</a></li>
                        <li><a href="player-gallery.html">Gallery</a></li>
                    </ul>
                </li>
                <li><a href="staff-single.html">Staff Member</a></li>
                <li><a href="event-tournament.html">Tournament &nbsp; <span class="label label-danger">New</span></a></li>
            </ul>
        </li>
        <li class=""><a href="#">News</a>
            <ul class="main-nav__sub">
                <li class=""><a href="blog-1.html">News - version 1</a></li>
                <li class=""><a href="blog-2.html">News - version 2</a></li>
                <li class=""><a href="blog-3.html">News - version 3</a></li>
                <li class=""><a href="blog-4.html">News - version 4</a></li>
                <li><a href="#">Post</a>
                    <ul class="main-nav__sub-2">
                        <li><a href="blog-post-1.html">Single Post - version 1</a></li>
                        <li><a href="blog-post-2.html">Single Post - version 2</a></li>
                        <li><a href="blog-post-3.html">Single Post - version 3</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class=""><a href="shop-grid.html">Shop</a>
            <ul class="main-nav__sub">
                <li class=""><a href="shop-grid.html">Shop - Grid</a></li>
                <li class=""><a href="shop-list.html">Shop - List</a></li>
                <li class=""><a href="shop-fullwidth.html">Shop - Full Width</a></li>
                <li class=""><a href="shop-product.html">Single Product</a></li>
                <li class=""><a href="shop-cart.html">Shopping Cart</a></li>
                <li class=""><a href="shop-checkout.html">Checkout</a></li>
                <li class=""><a href="shop-wishlist.html">Wishlist</a></li>
                <li class=""><a href="shop-login.html">Login</a></li>
                <li class=""><a href="shop-account.html">Account</a></li>
            </ul>
        </li>
        */ ?>
    </ul>

    <!-- Social Links -->
    <ul class="social-links social-links--inline social-links--main-nav">
        <li class="social-links__item">
            <a href="<?php the_field('facebook', 'options') ?>" class="social-links__link" data-toggle="tooltip" data-placement="bottom" title="Facebook" target="_blank"><i class="fa fa-facebook-f"></i></a>
        </li>
        <li class="social-links__item">
            <a href="<?php the_field('instagram', 'options') ?>" class="social-links__link" data-toggle="tooltip" data-placement="bottom" title="Instagram" target="_blank"><i class="fa fa fa-instagram"></i></a>
        </li>
    </ul>
    <!-- Social Links / End -->

    <!-- Pushy Panel Toggle -->
    <!-- <a href="#" class="pushy-panel__toggle">
        <span class="pushy-panel__line"></span>
    </a> -->
    <!-- Pushy Panel Toggle / Eng -->
</nav>
<!-- Main Navigation / End -->
