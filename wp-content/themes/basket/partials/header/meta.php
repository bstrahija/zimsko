<?php

    use Creolab\Basket\Plugin\Repositories\Teams;

    wp_reset_query();

    $requestUri      = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $pageTitle       = wp_title('|', false, 'right');
    $metaTitle       = $pageTitle;
    $metaUrl         = current_url();
    $metaKeywords    = 'zimsko, zima, košarka, prvenstvo, čakovec, međimurje, basketball';
    $metaImage       = get_the_post_thumbnail_url(null, 'large') ?:  home_url('/wp-content/uploads/2020/01/Parks-BC2.jpg');

    if (get_post_type() === 'team') {
        $team      = Teams::find(get_the_ID());
        $metaImage = $team ? $team->photo() : null;
    }

    if (is_category()) {
        $metaImage = get_image_src_for_category_from_posts();
    }

    if ( have_posts() ) : while ( have_posts() ) : the_post();
        $metaDescription = substr(str_replace(['"', "\n", "&nbsp;"], "", strip_tags(get_the_content())), 0, 160);
    endwhile; endif;
    wp_reset_query();

    if (! isset($metaDescription) or ! $metaDescription) {
        $metaDescription = 'Zimsko košarkaško prvenstvo je zamišljeno ko amatersko prvenstvo u košarci koje se odigrava nedjeljom u jutarnjim satima u dvorani ETŠ Čakovec.';
    }
?>

<meta charset="<?php bloginfo('charset'); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="<?php echo home_url('favicon.ico') ?>" rel="shortcut icon" type="image/vnd.microsoft.icon" />
<title><?php basket_wp_title( '|', true, 'right'); ?> Zimsko Košarkaško Prvenstvo Čakovec</title>

<meta name="app-url" content="<?php echo home_url() ?>">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">

<meta name="author" content="Zimsko">
<meta name="copyright" content="Zimsko">
<meta name="application-name" content="Zimsko">
<meta name="description" content="<?php echo $metaDescription ?>">
<meta name="keywords" content="<?php echo $metaKeywords; ?>">

<meta property="og:locale" content="hr_HR" />
<meta property="og:type" content="object" />
<meta property="og:title" content="<?php basket_wp_title( '|', true, 'right'); ?> Zimsko Košarkaško Prvenstvo Čakovec" />
<meta property="og:url" content="<?php echo $metaUrl ?>" />
<meta property="og:site_name" content="Zimsko Košarkaško Prvenstvo Čakovec" />
<meta property="og:description" content="<?php echo $metaDescription ?>" />
<meta property="og:image" content="<?php echo $metaImage ?>" />

<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="application-name" content="Zimsko">
<meta name="apple-mobile-web-app-title" content="Zimsko Košarkaško Prvenstvo Čakovec">
<meta name="msapplication-navbutton-color" content="#ffffff">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri() ?>/assets/images/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri() ?>/assets/images/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri() ?>/assets/images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri() ?>/assets/images/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri() ?>/assets/images/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri() ?>/assets/images/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri() ?>/assets/images/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri() ?>/assets/images/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/assets/images/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri() ?>/assets/images/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/assets/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri() ?>/assets/images/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/assets/images/favicon-16x16.png">
<meta name="msapplication-TileColor" content="#111">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri() ?>/assets/images/ms-icon-144x144.png">
<meta name="theme-color" content="#111">


<?php wp_head(); ?>
