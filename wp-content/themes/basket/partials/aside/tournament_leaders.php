<?php

use Creolab\Basket\Plugin\Repositories\Stats;

    $limit      = isset($limit) ? $limit : 10;
    $tournament = \Creolab\Basket\Plugin\Services\Cache::remember('current-tournament', BASKET_VIEW_CACHE, function() {
        return \Creolab\Basket\Plugin\Repositories\Tournaments::current();
    });
    $leaders = \Creolab\Basket\Plugin\Services\Cache::remember('tournament-leaders--'.$tournament->slug.'--'.$limit, BASKET_VIEW_CACHE, function() use ($limit, $tournament) {
        return \Creolab\Basket\Plugin\Repositories\Tournaments::leaders(['limit' => $limit, 'tournaments' => $tournament->slug]);
    });
?>

<?php if (isset($leaders) && $leaders->count()) : ?>
    <!-- Widget: Team Leaders -->
    <aside class="widget widget--sidebar card card--has-table widget-leaders">
        <div class="widget__title card__header">
            <h4>Vodeći igrači turnira</h4>
        </div>
        <div class="widget__content card__content">
            <!-- Leader: Points -->
            <div class="table-responsive">
                <table class="table team-leader">
                    <thead>
                    <tr>
                        <th class="team-leader__type"></th>
                        <th class="team-leader__gp">U</th>
                        <th class="team-leader__gp">P</th>
                        <th class="team-leader__avg">AVG</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $topScore = 0; ?>
                    <?php foreach ($leaders as $player) : ?>
                        <?php if (! $topScore) $topScore = $player['total']; ?>
                        <tr>
                            <td class="team-leader__player">
                                <div class="team-leader__player-info">
                                    <figure class="team-leader__player-img"><a href="<?php the_permalink($player['id']); ?>"><img src="<?php echo $player['photo'] ?>" alt=""></a></figure>
                                    <div class="team-leader__player-inner">
                                        <h5 class="team-leader__player-name"><a href="<?php the_permalink($player['id']); ?>"><?php echo $player['name'] ?></a></h5>

                                        <?php if ($player['team']) : ?>
                                            <span class="team-leader__player-position"><a href="<?php the_permalink($player['team']->id()); ?>"><?php echo $player['team']->title() ?></a></span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </td>
                            <td class="team-leader__gp"><?php echo $player['matches'] ?></td>
                            <td class="team-leader__gp"><strong><?php echo $player['total'] ?></strong></td>
                            <td class="team-leader__avg">
                                <div class="circular">
                                    <div class="circular__bar" data-percent="<?php echo round(($player['total'] / $topScore) * 100) ?>">
                                        <span class="circular__percents"><?php echo $player['avg'] ?></span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php

                ?>
            </div>
            <!-- Leader: Points / End -->

            <?php // if (isset($_GET['debug'])) dump($leaders); ?>
        </div>
    </aside>
    <!-- Widget: Team Leaders / End -->
<?php endif; ?>







