<?php
    $limit   = isset($limit) ? $limit : 6;
    $matches = \Creolab\Basket\Plugin\Repositories\Matches::upcoming(['posts_per_page' => $limit]);
?>

<?php if ($matches->count()) : ?>
    <!-- Widget: Latest Results -->
    <aside class="widget card widget--sidebar widget-results">
        <div class="widget__title card__header card__header--has-btn">
            <h4>Nadolazeće tekme</h4>
            <a href="<?php echo home_url('tekme') ?>" class="btn btn-default btn-outline btn-xs card-header__button">Sve tekme</a>
        </div>
        <div class="widget__content card__content">
            <ul class="widget-results__list">

                <?php foreach ($matches as $match) : ?>
                    <!-- Game #0 -->
                    <li class="widget-results__item">
                        <div class="widget-results__header justify-content-center">
                            <div class="widget-results__title"><?php echo $match->dateAndTime() ?></div>
                        </div>
                        <div class="widget-results__content">
                            <div class="widget-results__team widget-results__team--first">
                                <figure class="widget-results__team-logo">
                                    <img src="<?php echo $match->homeTeam->logo() ?>" alt="">
                                </figure>
                                <div class="widget-results__team-details">
                                    <h5 class="widget-results__team-name"><?php echo $match->homeTeam() ?></h5>
<!--                                    <span class="widget-results__team-info">Elric Bros School</span>-->
                                </div>
                            </div>
                            <div class="widget-results__result">

                            </div>
                            <div class="widget-results__team widget-results__team--second">
                                <figure class="widget-results__team-logo">
                                    <img src="<?php echo $match->awayTeam->logo() ?>" alt="">
                                </figure>
                                <div class="widget-results__team-details">
                                    <h5 class="widget-results__team-name"><?php echo $match->awayTeam() ?></h5>
<!--                                    <span class="widget-results__team-info">Marine College</span>-->
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Game #0 / End -->
                <?php endforeach; ?>
            </ul>
        </div>
    </aside>
    <!-- Widget: Latest Results / End -->
<?php endif; ?>
