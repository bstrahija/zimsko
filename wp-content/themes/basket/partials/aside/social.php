<!-- Widget: Social Buttons -->
<aside class="widget widget--sidebar widget-social">
    <a href="<?php the_field('facebook', 'options') ?>" class="btn-social-counter btn-social-counter--fb" target="_blank">
        <div class="btn-social-counter__icon">
            <i class="fa fa-facebook"></i>
        </div>
        <h6 class="btn-social-counter__title">Pratite nas na Facebooku</h6>
        <span class="btn-social-counter__add-icon"></span>
    </a>
    <a href="<?php the_field('instagram', 'options') ?>" class="btn-social-counter btn-social-counter--instagram" target="_blank">
        <div class="btn-social-counter__icon">
            <i class="fa fa-instagram"></i>
        </div>
        <h6 class="btn-social-counter__title">Pratite nas na Instagramu</h6>
        <span class="btn-social-counter__add-icon"></span>
    </a>
</aside>
<!-- Widget: Social Buttons / End -->
