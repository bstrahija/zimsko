<?php

$matches = \Creolab\Basket\Plugin\Repositories\Matches::latest(['posts_per_page' => 5]);

?>

<?php if ($matches and $matches->count()) : ?>
    <!-- Widget: Latest Results -->
    <aside class="widget card widget--sidebar widget-results">
        <div class="widget__title card__header card__header--has-btn">
            <h4>Zadnje utakmice</h4>
            <a href="<?php echo home_url('rezultati') ?>" class="btn btn-default btn-outline btn-xs card-header__button">Svi rezultati</a>
        </div>
        <div class="widget__content card__content">
            <ul class="widget-results__list">

                <?php foreach ($matches as $match) : ?>
                    <!-- Game #0 -->
                    <li class="widget-results__item">
                        <a href="<?php echo $match->permalink() ?>">
                            <div class="widget-results__header justify-content-center">
                                <div class="widget-results__title"><?php echo $match->date() ?></div>
                            </div>
                            <div class="widget-results__content">
                                <div class="widget-results__team widget-results__team--first">
                                    <figure class="widget-results__team-logo">
                                        <img src="<?php echo $match->homeTeam->logo() ?>" alt="">
                                    </figure>
                                    <div class="widget-results__team-details">
                                        <h5 class="widget-results__team-name"><?php echo $match->homeTeam() ?></h5>
    <!--                                    <span class="widget-results__team-info">Elric Bros School</span>-->
                                    </div>
                                </div>
                                <div class="widget-results__result">
                                    <div class="widget-results__score">
                                        <span class="widget-results__score-<?php echo $match->isWinner('home') ? 'winner' : 'loser' ?>"><?php echo $match->homeTeamScore() ?></span>
                                        -
                                        <span class="widget-results__score-<?php echo $match->isWinner('away') ? 'winner' : 'loser' ?>"><?php echo $match->awayTeamScore() ?></span>
    <!--                                    <div class="widget-results__status">Home</div>-->
                                    </div>
                                </div>
                                <div class="widget-results__team widget-results__team--second">
                                    <figure class="widget-results__team-logo">
                                        <img src="<?php echo $match->awayTeam->logo() ?>" alt="">
                                    </figure>
                                    <div class="widget-results__team-details">
                                        <h5 class="widget-results__team-name"><?php echo $match->awayTeam() ?></h5>
    <!--                                    <span class="widget-results__team-info">Marine College</span>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <!-- Game #0 / End -->
                <?php endforeach; ?>

            </ul>
        </div>
    </aside>
    <!-- Widget: Latest Results / End -->
<?php endif; ?>
