<?php
    $limit      = isset($limit) ? $limit : 10;
    $tournament = \Creolab\Basket\Plugin\Services\Cache::remember('current-tournament', BASKET_VIEW_CACHE, function() {
        return \Creolab\Basket\Plugin\Repositories\Tournaments::current();
    });
    $threePointLeaders = \Creolab\Basket\Plugin\Services\Cache::remember('3pts-leaders--'.$tournament->slug.'--'.$limit, BASKET_VIEW_CACHE, function() use ($limit, $tournament) {
        return \Creolab\Basket\Plugin\Repositories\Tournaments::threePointLeaders(['limit' => $limit, 'tournaments' => $tournament->slug]);
    });
?>

<?php if (isset($threePointLeaders) && $threePointLeaders->count()) : ?>
    <!-- Widget: Team Leaders -->
    <aside class="widget widget--sidebar card card--has-table widget-leaders">
        <div class="widget__title card__header">
            <h4>Trice</h4>
        </div>
        <div class="widget__content card__content">
            <!-- Leader: Points -->
            <div class="table-responsive">
                <table class="table team-leader">
                    <thead>
                    <tr>
                        <th class="team-leader__type"></th>
                        <th class="team-leader__avg">3PT</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($threePointLeaders as $leader) : ?>
                        <tr>
                            <td class="team-leader__player">
                                <div class="team-leader__player-info">
                                    <figure class="team-leader__player-img"><a href="<?php the_permalink($leader['player']->id()); ?>"><img src="<?php echo $leader['player']->photo() ?>" alt=""></a></figure>
                                    <div class="team-leader__player-inner">
                                        <h5 class="team-leader__player-name"><a href="<?php the_permalink($leader['player']->id()); ?>"><?php echo $leader['player']->name() ?></a></h5>

                                        <?php if ($leader['player']->team()) : ?>
                                            <span class="team-leader__player-position"><a href="<?php the_permalink($leader['player']->team()->id()); ?>"><?php echo $leader['player']->team()->title() ?></a></span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </td>
                            <td class="team-leader__gp"><strong><?php echo $leader['shots_made'] ?></strong></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- Leader: Points / End -->
        </div>
    </aside>
    <!-- Widget: Team Leaders / End -->
<?php endif; ?>
