<?php
    $matches = \Creolab\Basket\Plugin\Repositories\Teams::upcomingMatches(get_the_ID());
?>

<?php if ($matches and $matches->count()) : ?>
    <!-- Schedule & Tickets -->
    <div class="card card--has-table">
        <div class="card__header card__header--has-btn">
            <h4>Nadolazeće utakmice</h4>
            <a href="<?php echo home_url('matches') ?>" class="btn btn-default btn-outline btn-xs card-header__button">Sve utakmice</a>
        </div>

        <div class="card__content">
            <div class="table-responsive">
                <table class="table table-hover team-schedule">
                    <thead>
                        <tr>
                            <th class="team-schedule__date">Datum</th>
                            <th class="team-schedule__versus">Protiv</th>
                            <th class="team-schedule__time">Vrijeme</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($matches as $match) : ?>
                            <?php
                                $opposingTeam = $match->homeTeam->id() === get_the_ID() ? $match->awayTeam : $match->homeTeam;
                            ?>

                            <tr>
                                <td class="team-schedule__date"><?php echo $match->date() ?></td>
                                <td class="team-schedule__versus" style="text-align: left;">
                                    <div class="team-meta">
                                        <figure class="team-meta__logo">
                                            <img src="<?php echo $opposingTeam->logo() ?>" alt="">
                                        </figure>
                                        <div class="team-meta__info">
                                            <h6 class="team-meta__name"><?php echo $opposingTeam->title() ?></h6>
                                            <span class="team-meta__place"><?php echo $opposingTeam->location() ?></span>
                                        </div>
                                    </div>
                                </td>
                                <td class="team-schedule__time"><?php echo $match->date('%H:%M') ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Schedule & Tickets / End -->
<?php endif; ?>
