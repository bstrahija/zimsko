<?php
    $players = \Creolab\Basket\Plugin\Repositories\Teams::players(get_the_ID());
?>

<?php if ($players->count()) : ?>
    <div class="card">
        <div class="card__header" style="text-align: center;">
            <img src="<?php echo get_field('team_logo', get_the_ID()) ?>" alt="<?php the_title() ?>" style="max-width: 150px; max-height: 100px;">
        </div>
    </div>
<?php endif; ?>
