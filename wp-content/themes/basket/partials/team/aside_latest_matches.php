<?php
    $matches = \Creolab\Basket\Plugin\Repositories\Matches::latest(['limit' => 12]);
?>

<?php if ($matches and $matches->count()) : ?>
    <!-- Widget: Latest Results -->
    <aside class="widget card widget--sidebar widget-results">
        <div class="widget__title card__header card__header--has-btn">
            <h4>Zadnje utakmice</h4>
            <a href="#" class="btn btn-default btn-outline btn-xs card-header__button">Svi rezultati</a>
        </div>
        <div class="widget__content card__content">
            <ul class="widget-results__list">

                <?php foreach ($matches as $match) : ?>
                    <!-- Game #0 -->
                    <li class="widget-results__item">
                        <div class="widget-results__header justify-content-center">
                            <div class="widget-results__title"><?php echo $match->date('%A %d.%m.%Y.') ?></div>
                        </div>
                        <div class="widget-results__content">
                            <div class="widget-results__team widget-results__team--first">
                                <figure class="widget-results__team-logo">
                                    <a href="<?php echo $match->homeTeam->permalink() ?>"><img src="<?php echo $match->homeTeam->logo() ?>" alt=""></a>
                                </figure>
                                <div class="widget-results__team-details">
                                    <h5 class="widget-results__team-name"><a href="<?php echo $match->homeTeam->permalink() ?>" style="color: #333;"><?php echo $match->homeTeam() ?></a></h5>
                                    <!--                                    <span class="widget-results__team-info">Elric Bros School</span>-->
                                </div>
                            </div>
                            <div class="widget-results__result">
                                <div class="widget-results__score">
                                    <a href="<?php echo $match->permalink() ?>" style="color: #333;">
                                        <span class="widget-results__score-<?php echo $match->isWinner('home') ? 'winner' : 'loser' ?>"><?php echo $match->homeTeamScore() ?></span>
                                        -
                                        <span class="widget-results__score-<?php echo $match->isWinner('away') ? 'winner' : 'loser' ?>"><?php echo $match->awayTeamScore() ?></span>
                                    </a>
                                    <!--                                    <div class="widget-results__status">Home</div>-->
                                </div>
                            </div>
                            <div class="widget-results__team widget-results__team--second">
                                <figure class="widget-results__team-logo">
                                    <a href="<?php echo $match->awayTeam->permalink() ?>"><img src="<?php echo $match->awayTeam->logo() ?>" alt=""></a>
                                </figure>
                                <div class="widget-results__team-details">
                                    <h5 class="widget-results__team-name"><a href="<?php echo $match->awayTeam->permalink() ?>" style="color: #333"><?php echo $match->awayTeam() ?></a></h5>
                                    <!--                                    <span class="widget-results__team-info">Marine College</span>-->
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Game #0 / End -->
                <?php endforeach; ?>

            </ul>
        </div>
    </aside>
    <!-- Widget: Latest Results / End -->
<?php endif; ?>
