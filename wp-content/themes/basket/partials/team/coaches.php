<?php
    $coaches = \Creolab\Basket\Plugin\Repositories\Teams::coaches(get_the_ID());
?>

<?php if ($coaches->count()) : ?>
    <!-- Team Roster: Table -->
    <div class="card card--has-table">
        <div class="card__header card__header--has-btn">
            <h4>Treneri</h4>
        </div>
        <div class="card__content">
            <div class="table-responsive">
                <table class="table table--lg team-roster-table">
                    <tbody>
                    <?php /** @var \Creolab\Basket\Plugin\Models\Coach $coach */ ?>
                    <?php foreach ($coaches as $coach) : ?>
                        <tr>
                            <?php if (get_field('trainer_photo', $coach->ID)) : ?>
                                <td style="width: 1%;"><img src="<?php the_field('trainer_photo', $coach->ID) ?>" alt="" style="max-width: 40px"></td>
                            <?php endif; ?>

                            <td class="team-roster-table__name"><?php echo $coach->name() ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Team Roster: Table / End -->
<?php endif; ?>
