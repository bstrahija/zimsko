<?php
    $teamId = isset($teamId) ? $teamId : get_the_ID();
    $awards = \Creolab\Basket\Plugin\Repositories\Teams::awards($teamId);
?>

<?php if ($awards->count()) : ?>
    <!-- Widget: Awards -->
    <aside class="widget card widget--sidebar widget-awards">
        <div class="widget__title card__header">
            <h4>Titule</h4>
        </div>
        <div class="widget__content card__content">
            <div class="awards awards--slider">
                <?php foreach ($awards as $award) : ?>
                    <?php /** @var \Creolab\Basket\Plugin\Models\Award $award */ ?>
                    <div class="awards__item">
                        <figure class="awards__figure">
                            <img src="<?php echo $award->image() ?>" alt="" style="max-width: 66%; max-height: 280px;">
                        </figure>
                        <div class="awards__desc">
                            <h5 class="awards__name"><?php echo $award->title() ?></h5>
                            <div class="awards__date"><?php echo $award->date() ?></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </aside>
    <!-- Widget: Awards / End -->
<?php endif; ?>
