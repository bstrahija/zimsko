<?php
    // $players = \Creolab\Basket\Plugin\Repositories\Teams::players(get_the_ID());
    $players = \Creolab\Basket\Plugin\Repositories\Teams::leaders(get_the_ID(), ['limit' => 30]);
?>

<?php if ($players->count()) : ?>
    <!-- Team Roster: Table -->
    <div class="card card--has-table">
        <div class="card__header card__header--has-btn">
            <h4>Igrači</h4>
        </div>
        <div class="card__content">
            <div class="table-responsive">
                <table class="table table--lg team-roster-table">
                    <thead>
                        <tr>
                            <th style="width: 1%;"></th>
                            <th class="team-roster-table__number">Broj</th>
                            <th class="team-roster-table__name">Ime igrača</th>
                            <th class="team-roster-table__position hidden-xs hidden-sm">Pozicija</th>
                            <th class="team-leader__points">P</th>
                            <th class="team-leader__points">U</th>
                            <th class="team-leader__avg">AVG</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $topScore = 0; ?>
                        <?php foreach ($players as $player) : ?>
                            <?php /** @var \Creolab\Basket\Plugin\Models\Player $player */ ?>
                            <?php if (! $topScore) $topScore = $player->score(); ?>
                            <tr>
                                <td class="team-roster-table__number"><figure class="team-roster__player-img"><a href="<?php echo $player->permalink() ?>"><img src="<?php echo $player->photo() ?>" alt=""></a></figure></td>
                                <td class="team-roster-table__number"><?php echo $player->number() ?></td>
                                <td class="team-roster-table__name"><a href="<?php echo $player->permalink() ?>"><?php echo $player->name() ?></a></td>
                                <td class="team-roster-table__position hidden-xs hidden-sm"><?php echo $player->position() ?></td>
                                <td class="team-leader__points"><?php echo $player->pointsInTournament(['tournament_id' => \Creolab\Basket\Plugin\Repositories\Tournaments::current()->slug]) ?></td>
                                <td class="team-leader__points"><?php echo $player->matchesPlayedInTournament(\Creolab\Basket\Plugin\Repositories\Tournaments::current()->id) ?></td>
                                <td class="team-leader__avg">
                                    <div class="circular">
                                        <div class="circular__bar" data-percent="<?php echo round(($player->score() / $topScore) * 100) ?>">
                                            <span class="circular__percents"><?php echo $player->averagePoints() ?></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Team Roster: Table / End -->
<?php endif; ?>
