<?php
    // Check specific team BG
    $bgTeam = get_field('teams_background');
    $bg     = get_field('teams_background', 'options');
?>

<?php if ($bgTeam) : ?>
    <div class="page-heading-team-bg">
        <img src="<?php echo $bgTeam ?>">
    </div>
<?php else : ?>
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <h1 class="page-heading__title">
                        <figure>
                            <img src="<?php echo get_field('team_logo', get_the_ID()) ?>" alt="<?php the_title() ?>" style="max-width: 150px; max-height: 100px;">
                        </figure>
                        <br>
                        <?php the_title() ?>
                    </h1>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>


<style>
.page-heading-team-bg {
    padding: 0;
    height: auto;
}

.page-heading-team-bg img {
    width: 100%;
    object-fit: cover;
    max-height: 66vh;
}
</style>
