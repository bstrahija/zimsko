<article class="card card--lg post post--single">
    <figure class="post__thumbnail">
        <img src="<?php echo get_field('team_photo') ?>" alt="">
    </figure>
</article>
