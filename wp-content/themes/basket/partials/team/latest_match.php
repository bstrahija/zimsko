<?php
    $teamId = isset($teamId) ? $teamId : get_the_ID();
    $match  = \Creolab\Basket\Plugin\Repositories\Teams::lastMatch($teamId);
?>

<?php if ($match && $match->winner()) : ?>
    <!-- Game Scoreboard -->
    <div class="card">
        <header class="card__header card__header--has-btn">
            <h4>Posljednja utakmica</h4>
            <a href="<?php echo home_url('matches') ?>" class="btn btn-default btn-outline btn-xs card-header__button">Sve utakmice</a>
        </header>
        <div class="card__content">

            <!-- Game Result -->
            <div class="game-result">
                <section class="game-result__section">
                    <header class="game-result__header">
                        <a href="<?php echo $match->permalink() ?>">
                            <h3 class="game-result__title"><?php echo get_the_title($match->ID) ?></h3>
                            <time class="game-result__date" datetime="<?php echo $match->date() ?>" style="color: #666"><?php echo $match->date() ?></time>
                        </a>
                    </header>
                    <div class="game-result__content">

                        <!-- 1st Team -->
                        <div class="game-result__team game-result__team--first">
                            <a href="<?php echo $match->winner()->permalink() ?>">
                                <figure class="game-result__team-logo">
                                    <img src="<?php echo $match->winner()->logo() ?>" alt="">
                                </figure>
                                <div class="game-result__team-info">
                                    <h5 class="game-result__team-name"><?php echo $match->winner()->title() ?></h5>
                                    <?php /*<div class="game-result__team-desc"><?php the_field('location', $match->homeTeam->ID) ?></div>*/ ?>
                                </div>
                            </a>
                        </div>
                        <!-- 1st Team / End -->

                        <div class="game-result__score-wrap">
                            <a href="<?php echo $match->permalink() ?>">
                                <div class="game-result__score">
                                    <span class="game-result__score-result game-result__score-result--winner"><?php echo $match->winningTeamScore() ?></span>
                                    <span class="game-result__score-dash">-</span>
                                    <span class="game-result__score-result game-result__score-result--loser"><?php echo $match->losingTeamScore() ?></span>
                                </div>
                                <div class="game-result__score-label">Finalni rezultat</div>
                            </a>
                        </div>

                        <!-- 2nd Team -->
                        <div class="game-result__team game-result__team--second">
                            <a href="<?php echo $match->loser()->permalink() ?>">
                                <figure class="game-result__team-logo">
                                    <img src="<?php echo $match->loser()->logo() ?>" alt="">
                                </figure>
                                <div class="game-result__team-info">
                                    <h5 class="game-result__team-name"><?php echo $match->loser()->title() ?></h5>
                                    <?php /*<div class="game-result__team-desc"><?php the_field('location', $match->awayTeam->ID) ?></div>*/ ?>
                                </div>
                            </a>
                        </div>
                        <!-- 2nd Team / End -->
                    </div>

                    <div class="game-result__stats">
                        <div class="row">
                            <div class="col-12 col-md-12 order-md-12 game-result__stats-scoreboard">
                                <div class="game-result__table-stats">
                                    <div class="table-responsive">
                                        <table class="table table__cell-center table-wrap-bordered table-thead-color">
                                            <thead>
                                            <tr>
                                                <th>Poeni</th>
                                                <th>Q1</th>
                                                <th>Q2</th>
                                                <th>Q3</th>
                                                <th>Q4</th>
                                                <th>T</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th><?php echo $match->homeTeam() ?></th>
                                                <td><?php echo $match->homeTeamQuarterScore(1) ?></td>
                                                <td><?php echo $match->homeTeamQuarterScore(2) ?></td>
                                                <td><?php echo $match->homeTeamQuarterScore(3) ?></td>
                                                <td><?php echo $match->homeTeamQuarterScore(4) ?></td>
                                                <td><?php echo $match->homeTeamScore() ?></td>
                                            </tr>
                                            <tr>
                                                <th><?php echo $match->awayTeam() ?></th>
                                                <td><?php echo $match->awayTeamQuarterScore(1) ?></td>
                                                <td><?php echo $match->awayTeamQuarterScore(2) ?></td>
                                                <td><?php echo $match->awayTeamQuarterScore(3) ?></td>
                                                <td><?php echo $match->awayTeamQuarterScore(4) ?></td>
                                                <td><?php echo $match->awayTeamScore() ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!-- Game Result / End -->

        </div>
    </div>
    <!-- Game Scoreboard / End -->
<?php endif; ?>

