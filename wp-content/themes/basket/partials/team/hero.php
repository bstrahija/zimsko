<div class="page-heading" style="<?php echo (($bg = get_field('teams_background', 'options')) ? 'background-image: url(' . $bg . ')' : '') ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <h1 class="page-heading__title">Ekipe</h1>
            </div>
        </div>
    </div>
</div>
