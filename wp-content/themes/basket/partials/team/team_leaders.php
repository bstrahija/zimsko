<?php
    $leaders = \Creolab\Basket\Plugin\Repositories\Teams::leaders(get_the_ID());
?>

<?php if ($leaders->count()) : ?>
    <!-- Widget: Team Leaders -->
    <aside class="widget widget--sidebar card card--has-table widget-leaders">
        <div class="widget__title card__header">
            <h4>Vodeći igrači</h4>
        </div>
        <div class="widget__content card__content">

            <!-- Leader: Points -->
            <div class="table-responsive">
                <table class="table team-leader">
                    <thead>
                        <tr>
                            <th class="team-leader__type"></th>
                            <th class="team-leader__gp">U</th>
                            <th class="team-leader__points">P</th>
                            <th class="team-leader__avg">AVG</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $topScore = 0; ?>
                        <?php foreach ($leaders as $player) : ?>
                            <?php /** @var \Creolab\Basket\Plugin\Models\Player $player */ ?>
                            <?php if (! $topScore) $topScore = $player->score(); ?>
                            <tr>
                                <td class="team-leader__player">
                                    <div class="team-leader__player-info">
                                        <figure class="team-leader__player-img"><a href="<?php echo $player->permalink() ?>"><img src="<?php echo $player->photo() ?>" alt=""></a></figure>
                                        <div class="team-leader__player-inner">
                                            <h5 class="team-leader__player-name"><a href="<?php echo $player->permalink() ?>"><?php echo $player->name() ?></a></h5>
                                            <span class="team-leader__player-position"><?php echo $player->position() ?></span>
                                        </div>
                                    </div>
                                </td>
                                <td class="team-leader__gp"><?php echo $player->matchesPlayed() ?></td>
                                <td class="team-leader__gp"><?php echo $player->score() ?></td>
                                <td class="team-leader__avg">
                                    <div class="circular">
                                        <div class="circular__bar" data-percent="<?php echo round(($player->score() / $topScore) * 100) ?>">
                                            <span class="circular__percents"><?php echo $player->averagePoints() ?></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- Leader: Points / End -->


        </div>
    </aside>
    <!-- Widget: Team Leaders / End -->
<?php endif; ?>
