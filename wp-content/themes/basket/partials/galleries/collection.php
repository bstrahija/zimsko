<?php
    $galleries = \Creolab\Basket\Plugin\Repositories\Galleries::all();
?>

<?php if ($galleries->count()) : ?>
    <!-- Gallery -->
    <div class="gallery row">
        <?php /** @var \Creolab\Basket\Plugin\Models\Gallery $gallery */ ?>
        <?php foreach ($galleries as $gallery) : ?>
            <div class="gallery__item col-12 col-md-4 col-sm-4">
                <a href="<?php echo $gallery->permalink() ?>" class="gallery__item-inner card">
                    <figure class="gallery__thumb">
                        <img src="<?php echo $gallery->image() ?>" alt="">
                        <span class="btn-fab gallery__btn-fab"></span>
                    </figure>
                    <div class="gallery__content card__content">
                        <span class="gallery__icon">
                            <span class="icon-camera"></span>
                        </span>
                        <div class="gallery__details">
                            <h4 class="gallery__name"><?php echo $gallery->title(); ?></h4>
                            <div class="gallery__date"><?php echo $gallery->date(); ?></div>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
    <!-- Gallery / End -->
<?php endif; ?>

<style>
.gallery__thumb img {
    width: 100%;
    height: 300px;
    object-fit: cover;
}

@media only screen and (max-width: 600px) {
    .gallery__thumb img {
        height: 180px;
    }
}
</style>
