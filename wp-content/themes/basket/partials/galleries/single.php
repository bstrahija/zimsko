<?php
    $gallery = \Creolab\Basket\Plugin\Repositories\Galleries::find($id);

    if ($gallery) {
        $photos = $gallery->photos();
    }
?>

<?php if (isset($photos) && $photos) : ?>
    <div class="card card--clean">
        <header class="card__header card__header--has-btn">
            <h4><?php echo $gallery->title() ?></h4>
            <a href="<?php echo home_url('galerije') ?>" class="btn btn-default btn-outline btn-xs card-header__button">Povratak</a>
        </header>
    </div>

    <!-- Album -->
    <div class="album album--condensed container-fluid js-album-masonry">
        <div class="row">
            <?php foreach ($photos as $photo) : ?>
                <div class="album__item col-xs-6 col-sm-4">
                    <div class="album__item-holder">
                        <a href="<?php echo esc_url($photo['url']) ?>" class="album__item-link mp_gallery">
                            <figure class="album__thumb">
                                <img src="<?php echo esc_url($photo['sizes']['large']) ?>" alt="">
                            </figure>
                            <!-- <div class="album__item-desc">
                                <h4 class="album__item-title">The team is taking a summer vacation on Woody Valley</h4>
                                <time class="album__item-date" datetime="2016-08-23">August 23rd, 2016</time>
                                <span class="album__item-btn-fab btn-fab btn-fab--clean"></span>
                            </div> -->
                        </a>
                        <!-- <ul class="album__item-meta meta">
                            <li class="meta__item meta__item--likes"><a href="#"><i class="meta-like meta-like--active icon-heart"></i> 530</a></li>
                        </ul> -->
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <!-- Gallery Album / End -->

    <br><br>
<?php endif; ?>
