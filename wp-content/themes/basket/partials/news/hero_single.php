<div class="page-heading" style="<?php echo (($bg = get_field('news_background', 'options')) ? 'background-image: url(' . $bg . ')' : '') ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <h1 class="page-heading__title">Novosti</h1>
                <ol class="page-heading__breadcrumb breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo home_url() ?>">Početna</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo home_url('novosti') ?>">Novosti</a></li>
                    <li class="breadcrumb-item active"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></li>
                </ol>
            </div>
        </div>
    </div>
</div>
