<?php
    $leaders = \Creolab\Basket\Plugin\Repositories\Matches::leaders(get_the_ID());
?>

<?php if ($leaders->count()) : ?>
    <!-- Widget: Team Leaders -->
    <aside class="widget widget--sidebar card card--has-table widget-leaders">
        <div class="widget__title card__header">
            <h4>Najbolji strijelci</h4>
        </div>
        <div class="widget__content card__content">

            <!-- Leader: Points -->
            <div class="table-responsive">
                <table class="table team-leader">
                    <thead>
                    <tr>
                        <th class="team-leader__type"></th>
                        <th class="team-leader__gp">Poena</th>
<!--                        <th class="team-leader__avg">Prosjek</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($leaders as $player) : ?>
                        <?php /** @var \Creolab\Basket\Plugin\Models\Player $player */ ?>
                        <?php if ($player && $player->team()) : ?>
                            <tr>
                                <td class="team-leader__player">
                                    <div class="team-leader__player-info">
                                        <figure class="team-leader__player-img">
                                            <img src="<?php echo $player->photo() ?>" alt="">
                                        </figure>
                                        <div class="team-leader__player-inner">
                                            <h5 class="team-leader__player-name"><?php echo $player->name() ?></h5>
                                            <span class="team-leader__player-position"><?php echo $player->team()->title() ?></span>
                                        </div>
                                    </div>
                                </td>
                                <td class="team-leader__gp"><?php echo $player->score() ?></td>
                                <!-- <td class="team-leader__avg">
                                    <div class="circular">
                                        <div class="circular__bar" data-percent="85">
                                            <span class="circular__percents"><?php echo $player->averagePoints() ?></span>
                                        </div>
                                    </div>
                                </td> -->
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- Leader: Points / End -->


        </div>
    </aside>
    <!-- Widget: Team Leaders / End -->
<?php endif; ?>
