<?php
    $match = Creolab\Basket\Plugin\Repositories\Matches::find(get_the_ID());
?>

<?php if ($match) : ?>
    <?php if ($match->isOver()) : ?>
        <!-- Game Scoreboard -->
        <div class="card">
            <header class="card__header card__header--has-btn">
                <h4>Rezultat</h4>
                <a href="<?php echo home_url('rezultati') ?>" class="btn btn-default btn-outline btn-xs card-header__button">Prošli rezultati</a>
            </header>
            <div class="card__content">
                <?php while (have_posts()) : the_post(); ?>
                    <!-- Game Result -->
                    <div class="game-result">
                        <section class="game-result__section">
                            <header class="game-result__header">
                                <h3 class="game-result__title"><?php the_title() ?></h3>
                                <time class="game-result__date" datetime="2017-03-17"><?php echo $match->dateAndTime() ?></time>
                            </header>
                            <div class="game-result__content">
                                <!-- 1st Team -->
                                <div class="game-result__team game-result__team--first">
                                    <figure class="game-result__team-logo">
                                        <img src="<?php echo $match->winner()->logo() ?>" alt="">
                                    </figure>
                                    <div class="game-result__team-info">
                                        <h5 class="game-result__team-name"><?php echo $match->winner()->title() ?></h5>
                                        <?php /*<div class="game-result__team-desc"><?php the_field('location', $match->homeTeam->ID) ?></div>*/ ?>
                                    </div>
                                </div>
                                <!-- 1st Team / End -->

                                <div class="game-result__score-wrap">
                                    <div class="game-result__score">
                                        <span class="game-result__score-result game-result__score-result--winner"><?php echo $match->winningTeamScore() ?></span>
                                        <span class="game-result__score-dash">-</span>
                                        <span class="game-result__score-result game-result__score-result--loser"><?php echo $match->losingTeamScore() ?></span>
                                        <div class="game-result__score-label">Finalni rezultat</div>
                                    </div>
                                </div>

                                <!-- 2nd Team -->
                                <div class="game-result__team game-result__team--second">
                                    <figure class="game-result__team-logo">
                                        <img src="<?php echo $match->loser()->logo() ?>" alt="">
                                    </figure>
                                    <div class="game-result__team-info">
                                        <h5 class="game-result__team-name"><?php echo $match->loser()->title() ?></h5>
                                        <?php /*<div class="game-result__team-desc"><?php the_field('location', $match->awayTeam->ID) ?></div>*/ ?>
                                    </div>
                                </div>
                                <!-- 2nd Team / End -->
                            </div>

                            <div class="game-result__stats">
                                <div class="row">
                                    <div class="col game-result__stats-scoreboard">
                                        <div class="game-result__table-stats">
                                            <div class="table-responsive">
                                                <table class="table table__cell-center table-wrap-bordered table-thead-color">
                                                    <thead>
                                                    <tr>
                                                        <th>Poeni</th>
                                                        <th>Č1</th>
                                                        <th>Č2</th>
                                                        <th>Č3</th>
                                                        <th>Č4</th>
                                                        <?php if ($match->hadOvertime()) : ?>
                                                            <th>P</th>
                                                        <?php endif; ?>
                                                        <th>T</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <th><?php echo $match->homeTeam() ?></th>
                                                        <td><?php echo $match->homeTeamQuarterScore(1) ?></td>
                                                        <td><?php echo $match->homeTeamQuarterScore(2) ?></td>
                                                        <td><?php echo $match->homeTeamQuarterScore(3) ?></td>
                                                        <td><?php echo $match->homeTeamQuarterScore(4) ?></td>
                                                        <?php if ($match->hadOvertime()) : ?>
                                                            <td><?php echo $match->homeTeamOvertimeScore() ?></td>
                                                        <?php endif ?>
                                                        <td><?php echo $match->homeTeamScore() ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th><?php echo $match->awayTeam() ?></th>
                                                        <td><?php echo $match->awayTeamQuarterScore(1) ?></td>
                                                        <td><?php echo $match->awayTeamQuarterScore(2) ?></td>
                                                        <td><?php echo $match->awayTeamQuarterScore(3) ?></td>
                                                        <td><?php echo $match->awayTeamQuarterScore(4) ?></td>
                                                        <?php if ($match->hadOvertime()) : ?>
                                                            <td><?php echo $match->awayTeamOvertimeScore() ?></td>
                                                        <?php endif ?>
                                                        <td><?php echo $match->awayTeamScore() ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- Game Result / End -->
                <?php endwhile; ?>

            </div>
        </div>
        <!-- Game Scoreboard / End -->
    <?php else : ?>
        <div class="widget widget--sidebar card widget-preview">
            <div class="widget__title card__header">
                <h4>Informacije o tekmi</h4>
            </div>
            <div class="widget__content card__content">

                <!-- Match Preview -->
                <div class="match-preview">
                    <section class="match-preview__body">
                        <header class="match-preview__header">
                            <h3 class="match-preview__title"><?php echo $match->title() ?></h3>
                            <time class="match-preview__date" datetime="<?php echo $match->date('Y-m-d') ?>"><?php echo $match->dateAndTime() ?></time>
                        </header>
                        <div class="match-preview__content">

                            <!-- 1st Team -->
                            <div class="match-preview__team match-preview__team--first">
                                <figure class="match-preview__team-logo" style="text-align: center; margin: 0 auto;">
                                    <img src="<?php echo $match->homeTeam->logo() ?>" alt="">
                                </figure>
                                <h5 class="match-preview__team-name"><?php echo $match->homeTeam->title() ?></h5>
<!--                                <div class="match-preview__team-info">Elric Bros School</div>-->
                            </div>
                            <!-- 1st Team / End -->

                            <div class="match-preview__vs">
                                <div class="match-preview__conj">VS</div>
                                <div class="match-preview__match-info">
                                    <time class="match-preview__match-time" datetime="<?php echo $match->date('Y-m-d H:i:s') ?>"><?php echo $match->time() ?></time>
                                    <div class="match-preview__match-place">Dvorana Ekonomske Škole Čakovec</div>
                                </div>
                            </div>

                            <!-- 2nd Team -->
                            <div class="match-preview__team match-preview__team--second">
                                <figure class="match-preview__team-logo" style="text-align: center; margin: 0 auto;">
                                    <img src="<?php echo $match->awayTeam->logo() ?>" alt="">
                                </figure>
                                <h5 class="match-preview__team-name"><?php echo $match->awayTeam->title() ?></h5>
<!--                                <div class="match-preview__team-info">ST Paddy's Institute</div>-->
                            </div>
                            <!-- 2nd Team / End -->

                        </div>
                        <!-- <div class="match-preview__action">
                            <a href="#" class="btn btn-default btn-block">Buy Tickets Now</a>
                        </div> -->
                    </section>
                    <section class="match-preview__countdown countdown">
                        <h4 class="countdown__title">Do tekme:</h4>
                        <div class="countdown__content">
                            <div class="countdown-counter" data-date="<?php echo $match->countdownTime(); ?>"></div>
<!--                            June 18, 2020 21:00:00 -->
                        </div>
                    </section>
                </div>
                <!-- Match Preview / End -->

            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
