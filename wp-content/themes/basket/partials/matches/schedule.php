<?php
    use Creolab\Basket\Plugin\Repositories\Tournaments;

    $limit   = isset($limit) ? $limit : 20;
    $matches = \Creolab\Basket\Plugin\Repositories\Matches::allInTournament([
        'tournament_id' => Tournaments::current()->id(),
        'order' => 'asc',
        'posts_per_page' => $limit
    ]);
    $day = 1;
?>

<?php if ($matches and $matches->count()) : ?>
    <!-- Schedule & Tickets -->
    <div class="card card--has-table">
        <div class="card__header card__header--has-btn">
            <h4>Raspored prvenstva</h4>
        </div>
        <div class="card__content">
            <div class="table-responsive">
                <table class="table table-hover team-schedule">
                    <thead>
                    <tr>
                        <th class="team-schedule__versus">Domaćini</th>
                        <th class="team-schedule__versus">Gosti</th>
                        <th class="team-schedule__date">Datum</th>
                        <th class="team-schedule__time">Vrijeme</th>
<!--                        <th class="team-schedule__venue">Venue</th>-->
<!--                        <th class="team-schedule__tickets">Tickets</th>-->
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($matches as $index => $match) : ?>
                            <?php 
                                // Check date for previous match
                                if (isset($matches[$index - 1])) {
                                    $previousMatch     = $matches[$index - 1];
                                    $previousMatchDate = gmdate('Y-m-d', strtotime(get_field('match_date', $previousMatch->ID)));
                                    $matchDate         = gmdate('Y-m-d', strtotime(get_field('match_date', $match->ID)));

                                    if ($matchDate !== $previousMatchDate) {
                                        $day++;
                                        ?>
                                            <tr>
                                                <td colspan="4" style="background: #eee; padding: 25px 20px 20px 20px;">
                                                    <a href="<?php echo $match->permalink() ?>" style="color: #333; font-size: 20px;">
                                                        <?php echo $day; ?>. Kolo - <?php echo strftime('%d.%m.%Y.', strtotime(get_field('match_date', $match->ID))); ?>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                        <tr>
                                            <td colspan="4" style="background: #eee; padding: 25px 20px 20px 20px;">
                                                <a href="<?php echo $match->permalink() ?>" style="color: #333; font-size: 20px;">
                                                    <?php echo $day; ?>. Kolo - <?php echo strftime('%d.%m.%Y.', strtotime(get_field('match_date', $match->ID))); ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php
                                }
                            ?>

                            <tr>
                                <td colspan="4"><a href="<?php echo $match->permalink() ?>" style="color: #888"><?php echo $match->title() ?></a></td>
                            </tr>
                            <tr>
                                <td class="team-schedule__versus">
                                    <a href="<?php echo $match->homeTeam->permalink(); ?>">
                                        <div class="team-meta">
                                            <figure class="team-meta__logo">
                                                <img src="<?php echo $match->homeTeam->logo() ?>" alt="">
                                            </figure>
                                            <div class="team-meta__info">
                                                <h6 class="team-meta__name"><?php echo $match->homeTeam() ?></h6>
    <!--                                            <span class="team-meta__place">St. Patrick’s Institute</span>-->
                                            </div>
                                        </div>
                                    </a>
                                </td>
                                <td class="team-schedule__versus">
                                    <a href="<?php echo $match->awayTeam->permalink(); ?>">
                                        <div class="team-meta">
                                            <figure class="team-meta__logo">
                                                <img src="<?php echo $match->awayTeam->logo() ?>" alt="">
                                            </figure>
                                            <div class="team-meta__info">
                                                <h6 class="team-meta__name"><?php echo $match->awayTeam() ?></h6>
    <!--                                            <span class="team-meta__place">St. Patrick’s Institute</span>-->
                                            </div>
                                        </div>
                                    </a>
                                </td>
                                <td class="team-schedule__date"><a href="<?php echo $match->permalink() ?>" style="color: #888;"><?php echo $match->date() ?></a></td>
                                <td class="team-schedule__time"><a href="<?php echo $match->permalink() ?>" style="color: #888;"><?php echo $match->time(); ?></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Schedule & Tickets / End -->
<?php endif; ?>
