<?php
    $referees = \Creolab\Basket\Plugin\Repositories\Matches::referees(get_the_ID());
?>

<?php if ($referees && $referees->count()) : ?>
    <div class="card card--has-table">
        <div class="card__header card__header--has-btn">
            <h4>Suci</h4>
        </div>
        <div class="card__content">
            <div class="table-responsive">
                <table class="table table--lg team-roster-table">
                    <tbody>
                    <?php /** @var \Creolab\Basket\Plugin\Models\Referee $referee */ ?>
                    <?php foreach ($referees as $referee) : ?>
                        <tr>
                            <?php if (get_field('referee_photo', $referee->ID)) : ?>
                                <td style="width: 1%;"><img src="<?php the_field('referee_photo', $referee->ID) ?>" alt="" style="max-width: 40px"></td>
                            <?php else : ?>
                                <td style="width: 1%;"><img src="<?php echo get_template_directory_uri('/') ?>/assets/images/samples/widget-featured-player.png" style="max-width: 40px" alt=""></td>
                            <?php endif; ?>

                            <td class="team-roster-table__name"><?php echo $referee->name() ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>
