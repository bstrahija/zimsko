<div class="page-heading" style="<?php echo (($bg = get_field('teams_background', 'options')) ? 'background-image: url(' . $bg . ')' : '') ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <h1 class="page-heading__title">
                    <figure>
                        <img src="<?php echo get_field('team_logo', $team->id()) ?>" alt="<?php the_title() ?>" style="max-width: 150px; max-height: 100px;">
                    </figure>
                    <br>
                    <?php the_title() ?>
                </h1>
            </div>
        </div>
    </div>
</div>
