<?php
    $playerId = isset($playerId) ? $playerId : get_the_ID();
    $player   = \Creolab\Basket\Plugin\Repositories\Players::find($playerId);
?>

<?php if ($player) : ?>
<!-- Widget: Team Info -->
<aside class="widget widget--sidebar card card--no-paddings widget-team-info">
    <div class="widget__title card__header">
        <h4>Informacije</h4>
    </div>
    <div class="widget__content card__content">
        <figure style="text-align: center; padding: 20px; border-radius: 50%; overflow: hidden"><img src="<?php echo $player->photo() ?>" alt="" style="max-width: 150px; border-top-left-radius: 50% 50%; border-top-right-radius: 50% 50%; border-bottom-right-radius: 50% 50%; border-bottom-left-radius: 50% 50%;"></figure>

        <ul class="team-info-list list-unstyled">
            <li class="team-info__item">
                <span class="team-info__label">Ime:</span>
                <span class="team-info__value "><?php echo $player->name(); ?></span>
            </li>
            <li class="team-info__item">
                <span class="team-info__label">Starost:</span>
                <span class="team-info__value ">-</span>
            </li>
            <li class="team-info__item">
                <span class="team-info__label">Mjesto:</span>
                <span class="team-info__value ">Hrvatska</span>
            </li>
            <li class="team-info__item">
                <span class="team-info__label">Ukupno poena:</span>
                <span class="team-info__value "><?php echo $player->pointsInTournament() ?></span>
            </li>
            <li class="team-info__item">
                <span class="team-info__label">Utakmica:</span>
                <span class="team-info__value "><?php echo $player->matchesPlayed() ?></span>
            </li>
            <li class="team-info__item">
                <span class="team-info__label">Prosjek poena:</span>
                <span class="team-info__value "><?php echo $player->averagePoints() ?></span>
            </li>
        </ul>
    </div>
</aside>
<!-- Widget: Team Info / End -->
<?php endif; ?>
