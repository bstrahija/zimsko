<?php
    $pointHistory = \Creolab\Basket\Plugin\Repositories\Tournaments::playerPointHistory(get_the_ID());
?>

<!-- Points History -->
<div class="card">
    <div class="card__header card__header--has-btn">
        <h4>Povijest poena</h4>
    </div>
    <div class="card__content">
        <canvas class="points-history-chart chartable" data-source="" height="135"></canvas>
    </div>
</div>
<!-- Points History / End -->

<script>
    window.teamPointsHistoryData = {
        labels: [
            <?php foreach ($pointHistory as $item) : ?>
            '<?php echo $item->label; ?>',
            <?php endforeach; ?>
        ],
        data: [
            <?php foreach ($pointHistory as $item) : ?>
            '<?php echo $item->points; ?>',
            <?php endforeach; ?>
        ],
    };
</script>
