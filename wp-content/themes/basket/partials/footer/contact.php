<!-- Widget: Contact Info -->
<div class="widget widget--footer widget-contact-info">
    <h4 class="widget__title">Prijavite ekipu!</h4>
    <div class="widget__content">
        <?php /*<div class="widget-contact-info__desc">
            <p>Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
        </div> */ ?>
        <div class="widget-contact-info__body info-block">
            <div class="info-block__item">
                <svg role="img" class="df-icon df-icon--basketball">
                    <use xlink:href="<?php echo get_template_directory_uri() ?>/assets/images/icons-basket.svg#basketball"/>
                </svg>
                <h6 class="info-block__heading">E-Mail</h6>
                <a class="info-block__link" href="mailto:<?php the_field('contact_email', 'options') ?>"><?php the_field('contact_email', 'options') ?></a>
            </div>
            <div class="info-block__item">
                <svg role="img" class="df-icon df-icon--jersey">
                    <use xlink:href="<?php echo get_template_directory_uri() ?>/assets/images/icons-basket.svg#jersey"/>
                </svg>
                <h6 class="info-block__heading">Prijava!</h6>
                <a class="info-block__link" href="<?php echo home_url('prijava') ?>">Prijavi se ovdje</a>
            </div>
            <div class="info-block__item info-block__item--nopadding">
                <ul class="social-links">

                    <li class="social-links__item">
                        <a href="#" class="social-links__link"><i class="fa fa-facebook"></i> Facebook</a>
                    </li>
                    <li class="social-links__item">
                        <a href="#" class="social-links__link"><i class="fa fa-instagram"></i> Instagram</a>
                    </li>


                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Widget: Contact Info / End -->
