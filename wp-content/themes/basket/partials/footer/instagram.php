<!-- Widget: Instagram -->
<div class="widget widget--footer widget-instagram">
    <h4 class="widget__title">Instagram Feed</h4>
    <div class="widget__content">
        <ul id="instagram-feed" class="widget-instagram__list"></ul>
        <a href="<?php the_field('instagram', 'options') ?>" class="btn btn-sm btn-instagram btn-icon-right">Instagram <i class="icon-arrow-right"></i></a>
    </div>
</div>
<!-- Widget: Instagram / End -->
