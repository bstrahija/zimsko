<?php
use Creolab\Basket\Plugin\Repositories\News;
?>

<!-- Widget: Popular Posts / End -->
<div class="widget widget--footer widget-popular-posts">
    <h4 class="widget__title">Najnovije vijesti</h4>
    <div class="widget__content">
        <ul class="posts posts--simple-list">
            <?php foreach (News::latest(['posts_per_page' => 3]) as $news) : ?>
                <li class="posts__item posts__item--category-2">
                    <!-- <div class="posts__cat">
                        <span class="label posts__cat-label">Injuries</span>
                    </div> -->
                    <h6 class="posts__title posts__title--color-hover"><a href="<?php echo get_permalink($news->ID) ?>"><?php echo $news->post_title ?></a></h6>
                    <time datetime="2017-08-23" class="posts__date"><?php echo get_the_date('', $news) ?></time>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<!-- Widget: Popular Posts / End -->
