<!-- Footer Secondary -->
<div class="footer-secondary footer-secondary--has-decor">
    <div class="container">
        <div class="footer-secondary__inner">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <ul class="footer-nav">
                        <li class="footer-nav__item"><a href="<?php echo home_url() ?>">Početna</a></li>
                        <li class="footer-nav__item"><a href="<?php echo home_url() ?>/novosti">Novosti</a></li>
                        <li class="footer-nav__item"><a href="<?php echo home_url() ?>/rezultati">Rezultati</a></li>
                        <li class="footer-nav__item"><a href="<?php echo home_url() ?>/ekipe">Ekipe</a></li>
                        <li class="footer-nav__item"><a href="<?php echo home_url() ?>/prijava">Prijavite Ekipu</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer Secondary / End -->
