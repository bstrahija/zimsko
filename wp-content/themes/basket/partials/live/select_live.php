<?php

use Creolab\Basket\Plugin\Repositories\Matches;

// Get all matches
$matches = Matches::all();
?>

<?php partial('partials/results/hero') ?>

<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-12">
                <div class="card card--has-table">
                    <div class="card__header card__header--has-btn">
                        <h4>Live Score Utakmice</h4>
                    </div>
                    <div class="card__content">
                        <div class="table-responsive">
                            <table class="table table-hover team-schedule">
                                <thead>
                                    <tr>
                                        <th class="team-schedule__versus"></th>
                                        <th class="team-schedule__versus">Naslov</th>
                                        <th class="team-schedule__date">Datum</th>
                                        <th class="team-schedule__time">Vrijeme</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($matches as $index => $match) : ?>
                                        <tr>
                                        <td style="text-align: left">Završeno</td>
                                            <td style="text-align: left">
                                                <a href="?match_id=<?php echo $match->id() ?>">
                                                    <?php echo $match->title() ?>
                                                </a>
                                            </td>
                                            <td style="text-align: left">
                                                <a href="?match_id=<?php echo $match->id() ?>">
                                                    <?php echo $match->date() ?>
                                                </a>
                                            </td>
                                            <td style="text-align: left">
                                                <a href="?match_id=<?php echo $match->id() ?>">
                                                    <?php echo $match->time() ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Content / End -->
        </div>
    </div>
</div>
<!-- Content / End -->
