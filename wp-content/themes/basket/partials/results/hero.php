<div class="page-heading" style="<?php echo (($bg = get_field('results_background', 'options')) ? 'background-image: url(' . $bg . ')' : '') ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <h1 class="page-heading__title"><?php the_title() ?></h1>
                <ol class="page-heading__breadcrumb breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo home_url() ?>">Početna</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo home_url('novosti') ?>"><?php the_title() ?></a></li>
                </ol>
            </div>
        </div>
    </div>
</div>
