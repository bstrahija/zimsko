<?php
    // If we pass a get variable for a tournament, we will only show results for that tournament
    $tournamentSlug = isset($_GET['tournament']) ? $_GET['tournament'] : null;

    if ($tournamentSlug) {
        $tournament = \Creolab\Basket\Plugin\Repositories\Tournaments::findBySlug($tournamentSlug);
    } else {
        $tournaments = \Creolab\Basket\Plugin\Repositories\Tournaments::all();
        $tournament = $tournaments->first();
    }
?>

<?php //foreach ($tournaments as $tournament) : ?>
    <?php $matches = \Creolab\Basket\Plugin\Repositories\Matches::latest([], [$tournament->slug]); ?>

    <?php if ($matches and $matches->count()) : ?>
        <!-- Widget: Latest Results -->
        <aside class="widget card widget--sidebar widget-results">
            <div class="widget__title card__header card__header--has-btn">
                <h4>Svi rezultati - <?php echo $tournament->name ?></h4>
            </div>
            <div class="widget__content card__content">
                <?php foreach ($matches as $match) : ?>
                    <?php if ($match->winner() && $match->loser()) : ?>
                    <!-- Game Result -->
                        <div class="game-result" style="padding-left: 25px; padding-right: 25px; border-bottom: 1px solid #ddd;">
                            <section class="game-result__section">
                                <?php /*<header class="game-result__header">
                                    <h3 class="game-result__title"><?php echo $match->title() ?></h3>
        <!--                            <time class="game-result__date" datetime="2017-03-17">Saturday, March 17th, 2017</time>-->
                                </header>*/ ?>
                                <div class="game-result__content">
                                    <!-- 1st Team -->
                                    <div class="game-result__team game-result__team--first">
                                        <a href="<?php echo $match->winner()->permalink() ?>">
                                            <figure class="game-result__team-logo">
                                                <img src="<?php echo $match->winner()->logo() ?>" alt="">
                                            </figure>
                                            <div class="game-result__team-info">
                                                <h5 class="game-result__team-name"><?php echo $match->winner()->title() ?></h5>
                                                <?php /*<div class="game-result__team-desc"><?php the_field('location', $match->homeTeam->ID) ?></div>*/ ?>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- 1st Team / End -->

                                    <div class="game-result__score-wrap">
                                        <a href="<?php echo $match->permalink() ?>">
                                            <div class="game-result__score">
                                                <span class="game-result__score-result game-result__score-result--winner"><?php echo $match->winningTeamScore() ?></span>
                                                <span class="game-result__score-dash">-</span>
                                                <span class="game-result__score-result game-result__score-result--loser"><?php echo $match->losingTeamScore() ?></span>
                                                <div class="game-result__score-label"><?php echo $match->dateAndTime() ?></div>
                                            </div>
                                        </a>
                                    </div>

                                    <!-- 2nd Team -->
                                    <div class="game-result__team game-result__team--second">
                                        <a href="<?php echo $match->loser()->permalink() ?>">
                                            <figure class="game-result__team-logo">
                                                <img src="<?php echo $match->loser()->logo() ?>" alt="">
                                            </figure>
                                            <div class="game-result__team-info">
                                                <h5 class="game-result__team-name"><?php echo $match->loser()->title() ?></h5>
                                                <?php /*<div class="game-result__team-desc"><?php the_field('location', $match->awayTeam->ID) ?></div>*/ ?>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- 2nd Team / End -->
                                </div>
                            </section>
                        </div>
                        <!-- Game Result / End -->
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </aside>
        <!-- Widget: Latest Results / End -->
    <?php else : ?>
        <aside class="widget card widget--sidebar widget-results">
            <div class="widget__title card__header card__header--has-btn">
                <h4>Svi rezultati - <?php echo $tournament->name ?></h4>
            </div>
            <div class="widget__content card__content" style="padding: 30px 20px">
                <p>Trenutno nema rezultata.</p>

                <?php $tournaments = \Creolab\Basket\Plugin\Repositories\Tournaments::all(); ?>
                <h5>Arhiva Rezultata</h5>
                <ul>
                    <?php foreach ($tournaments as $tournament) : ?>
                        <li><a href="<?php echo site_url('rezultati') ?>?tournament=<?php echo $tournament->slug; ?>"><?php echo $tournament->name ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </aside>
    <?php endif; ?>
<?php //endforeach; ?>
