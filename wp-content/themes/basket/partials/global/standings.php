<?php
    // New leaders
    $standings =  \Creolab\Basket\Plugin\Repositories\Stats::newStandings();
?>

<?php if ($standings) : ?>
    <!-- Widget: Standings -->
    <aside class="widget card widget--sidebar widget-standings">
        <div class="widget__title card__header card__header--has-btn">
            <h4>Ljestvica</h4>
        </div>
        <div class="widget__content card__content">
            <div class="table-responsive">
                <table class="table table-hover table-standings">
                    <thead>
                        <tr>
                            <th>Pozicija</th>
                            <th title="Utakmice">U</th>
                            <th title="Pobjede">P</th>
                            <th title="Porazi">I</th>
                            <th title="Koš razlika">K</th>
                            <th title="Bodovi">B</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($standings as $standing) : ?>
                            <?php $teamPost = get_post($standing['wp_id']) ?>
                            <?php $team     = $teamPost ? \Creolab\Basket\Plugin\Repositories\Teams::find($teamPost->ID) : null; ?>

                            <?php if ($team) : ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo $team->permalink() ?>">
                                            <div class="team-meta">
                                                <figure class="team-meta__logo">
                                                    <a href="<?php echo $team->permalink() ?>"><img src="<?php echo get_field('team_logo', $team->id()) ?>" alt="<?php echo $team->title() ?>"></a>
                                                </figure>
                                                <div class="team-meta__info">
                                                    <h6 class="team-meta__name"><a href="<?php echo $team->permalink() ?>"><?php echo $team->title() ?></a></h6>
                                                    <span class="team-meta__place"><?php the_field('location', $team->wp_id) ?></span>
                                                </div>
                                            </div>
                                        </a>
                                    </td>
                                    <td><?php echo $standing['matches'] ?></td>
                                    <td><?php echo $standing['wins'] ?></td>
                                    <td><?php echo $standing['losses'] ?></td>
                                    <td><?php echo $standing['points_diff'] ?></td>
                                    <td><?php echo $standing['ranking_points'] ?></td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </aside>
    <!-- Widget: Standings / End -->
<?php endif; ?>
