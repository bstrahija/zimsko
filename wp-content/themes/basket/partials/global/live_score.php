<?php

use Creolab\Basket\Plugin\Db\Tables;

global $wpdb;

// Use the current time, and add 1 hour to it
$now = (new DateTime())->add(new DateInterval('PT1H'));

if (isset($_GET['debug']) && isset($_GET['time'])) {
    $now = DateTime::createFromFormat('Y-m-d H:i:s', $_GET['time']);
}
// $now = DateTime::createFromFormat('Y-m-d H:i:s', '2024-02-18 11:55:00');

// Get todays games
$matches = $wpdb->get_results("SELECT * FROM ".Tables::get('matches')." WHERE scheduled_at LIKE '{$now->format('Y-m-d')}%' ORDER BY scheduled_at ASC");

// And find the current one
$currentMatch = null;
foreach ($matches as $index => $match) {
    $nextMatch   = $matches[$index + 1] ?? null;
    $targetStart = DateTime::createFromFormat('Y-m-d H:i:s', $match->scheduled_at);
    $targetEnd   = $nextMatch ? DateTime::createFromFormat('Y-m-d H:i:s', $nextMatch->scheduled_at) : DateTime::createFromFormat('Y-m-d H:i:s', $match->scheduled_at)->add(new DateInterval('PT1H'));

    if ($now > $targetStart && $now < $targetEnd) {
        $currentMatch = $match;
        break;
    }
}
?>

<?php if ($currentMatch) : ?>
    <div class="widget__live-score" data-id="<?php echo $currentMatch->id ?>">
        <h5>UŽIVO</h5>

        <div class="widget__live-score__teams">
            <div class="widget__live-score__team">
                <div class="widget__live-score__team-logo widget__live-score__team-home-logo"><img src=""></div>
                <div class="widget__live-score__team-name widget__live-score__team-home-name"></div>
            </div>

            <div class="widget__live-score__team">
                <div class="widget__live-score__team-logo widget__live-score__team-away-logo"><img src=""></div>
                <div class="widget__live-score__team-name widget__live-score__team-away-name"></div>
            </div>
        </div>

        <div class="widget__live-score__total">
            <span class="widget__live-score__total-home">0</span>
            <span class="widget__live-score__total-away">0</span>
        </div>

        <div class="widget__live-score__quarters">
            <div class="widget__live-score__quarter">
                <span class="widget__live-score__quarter1-home">0</span>
                <i>Q1</i>
                <span class="widget__live-score__quarter1-away">0</span>
            </div>

            <div class="widget__live-score__quarter">
                <span class="widget__live-score__quarter2-home">0</span>
                <i>Q2</i>
                <span class="widget__live-score__quarter2-away">0</span>
            </div>

            <div class="widget__live-score__quarter">
                <span class="widget__live-score__quarter3-home">0</span>
                <i>Q3</i>
                <span class="widget__live-score__quarter3-away">0</span>
            </div>

            <div class="widget__live-score__quarter">
                <span class="widget__live-score__quarter4-home">0</span>
                <i>Q4</i>
                <span class="widget__live-score__quarter4-away">0</span>
            </div>
        </div>
    </div>

<?php endif; ?>

