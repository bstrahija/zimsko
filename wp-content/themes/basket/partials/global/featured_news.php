<?php
    $posts = \Creolab\Basket\Plugin\Services\Cache::remember('featured-news-1', BASKET_VIEW_CACHE, function() {
        return get_posts(['ignore_sticky_posts' => false, 'post_type' => 'post', 'posts_per_page' => 1]);
    });
?>

<?php if ($posts) : ?>
    <aside class="widget widget--sidebar card widget-popular-posts">
        <div class="widget__title card__header">
            <h4>Istaknuto</h4>
        </div>
        <div class="widget__content card__content">
            <ul class="posts posts--simple-list">
                <?php foreach ($posts as $post) : ?>
                    <li class="posts__item posts__item--category-2">
                        <figure class="posts__thumb">
                            <a href="<?php echo get_the_permalink($post->ID) ?>"><img src="<?php echo get_the_post_thumbnail_url($post->ID) ?>" alt="" style="max-width: 80px;"></a>
                        </figure>
                        <div class="posts__inner">
                            <h6 class="posts__title"><a href="<?php echo get_the_permalink($post->ID) ?>"><?php echo get_the_title($post->ID) ?></a></h6>
                            <p><?php echo limit_string(get_the_excerpt($post->ID), 100) ?></p>
<!--                            <time datetime="2016-08-23" class="posts__date">--><?php //echo get_the_date('d.m.Y.', $post->ID) ?><!--</time>-->
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </aside>
    <!-- Widget: Popular News / End -->
<?php endif; ?>
