<?php wp_reset_query(); ?>
<?php if (have_rows('sponsors', 'options')) : ?>
    <div class="card card--clean">
        <header class="card__header card__header--has-filter">
            <h4>Sponzori</h4>
        </header>
        <div class="card__content" style="background: #fff; border: 1px solid #eee; border-top: none; padding-bottom: 10px;">
            <!-- Carousel -->
            <div class="sponsors__list">
                <?php while (have_rows('sponsors', 'options')) : the_row(); ?>
                    <?php $image = get_sub_field('image'); ?>
                    <?php if ($image) : ?>
                        <a href="<?php the_sub_field('link'); ?>" target="_blank" class="posts__item">
                            <figure class="posts__thumb" style="text-align: center; background-color: transparent;">
                                <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php the_sub_field('title'); ?>" style="display: inline-block;">
                            </figure>
                        </a>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
