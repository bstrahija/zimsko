<?php
    $limit  = isset($limit) ? $limit : 20;
    $matches = \Creolab\Basket\Plugin\Services\Cache::remember('upcoming-matches-'.$limit, BASKET_VIEW_CACHE, function() use ($limit) {
        return \Creolab\Basket\Plugin\Repositories\Matches::upcoming(['posts_per_page' => $limit]);
    });
?>

<?php if ($matches and $matches->count()) : ?>
    <!-- Schedule & Tickets -->
    <div class="card card--has-table">
        <div class="card__header card__header--has-btn">
            <h4>Nadolazeće utakmice</h4>
            <a href="<?php echo home_url('tekme') ?>" class="btn btn-default btn-outline btn-xs card-header__button">Sve utakmice</a>
        </div>
        <div class="card__content">
            <div class="table-responsive">
                <table class="table table-hover team-schedule">
                    <thead>
                    <tr>
                        <th class="team-schedule__versus">Domaćini</th>
                        <th class="team-schedule__versus">Gosti</th>
                        <th class="team-schedule__date">Datum</th>
                        <th class="team-schedule__time">Vrijeme</th>
<!--                        <th class="team-schedule__venue">Venue</th>-->
<!--                        <th class="team-schedule__tickets">Tickets</th>-->
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($matches as $match) : ?>
                            <tr>
                                <td colspan="4"><a href="<?php echo $match->permalink() ?>" style="color: #888"><?php echo $match->title() ?></a></td>
                            </tr>
                            <tr>
                                <td class="team-schedule__versus">
                                    <a href="<?php echo $match->homeTeam->permalink(); ?>">
                                        <div class="team-meta">
                                            <figure class="team-meta__logo">
                                                <img src="<?php echo $match->homeTeam->logo() ?>" alt="">
                                            </figure>
                                            <div class="team-meta__info">
                                                <h6 class="team-meta__name"><?php echo $match->homeTeam() ?></h6>
    <!--                                            <span class="team-meta__place">St. Patrick’s Institute</span>-->
                                            </div>
                                        </div>
                                    </a>
                                </td>
                                <td class="team-schedule__versus">
                                    <a href="<?php echo $match->awayTeam->permalink(); ?>">
                                        <div class="team-meta">
                                            <figure class="team-meta__logo">
                                                <img src="<?php echo $match->awayTeam->logo() ?>" alt="">
                                            </figure>
                                            <div class="team-meta__info">
                                                <h6 class="team-meta__name"><?php echo $match->awayTeam() ?></h6>
    <!--                                            <span class="team-meta__place">St. Patrick’s Institute</span>-->
                                            </div>
                                        </div>
                                    </a>
                                </td>
                                <td class="team-schedule__date"><a href="<?php echo $match->permalink() ?>" style="color: #888;"><?php echo $match->date() ?></a></td>
                                <td class="team-schedule__time"><a href="<?php echo $match->permalink() ?>" style="color: #888;"><?php echo $match->time(); ?></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Schedule & Tickets / End -->
<?php endif; ?>
