<?php
    $articles = \Creolab\Basket\Plugin\Services\Cache::remember('latest-news-4', BASKET_VIEW_CACHE, function() {
        return \Creolab\Basket\Plugin\Repositories\News::latest(['posts_per_page' => 4]);
    });
?>

<!-- Latest News -->
<div class="card card--clean">
    <header class="card__header card__header--has-btn">
        <h4>Novosti</h4>
        <a href="<?php echo home_url('novosti') ?>" class="btn btn-default btn-outline btn-xs card-header__button">Sve novosti</a>
    </header>
    <div class="card__content">
        <!-- Posts List -->
        <div class="posts posts--cards posts--cards-thumb-left post-list">
            <?php foreach ($articles as $article) : ?>
                <div class="post-list__item">
                    <div class="posts__item posts__item--card posts__item--category-1 card card--block">
                        <?php if (has_post_thumbnail($article->ID)) : ?>
                            <figure class="posts__thumb">
                                <a href="<?php the_permalink($article->id); ?>"><img src="<?php echo get_the_post_thumbnail_url($article->ID, 'news-list') ?>" alt=""></a>
<!--                                <a href="#" class="posts__cta"></a>-->
                            </figure>
                        <?php endif; ?>

                        <div class="posts__inner">
                            <div class="card__content">
                                <?php if (get_the_category()) : ?>
                                    <div class="posts__cat">
                                        <?php foreach (get_the_category() as $category) : ?>
                                            <span class="label posts__cat-label"><?php echo $category->cat_name ?></span>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                                <h6 class="posts__title"><a href="<?php the_permalink($article->ID); ?>"><?php echo get_the_title($article->ID) ?></a></h6>
                                <time datetime="2016-08-17" class="posts__date"><?php echo get_the_date('', $article->ID) ?></time>
                                <!-- <div class="posts__excerpt">
                                    <?php echo get_the_excerpt($article->ID) ?>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <!-- Posts List / End -->
    </div>
</div>
<!-- Lates News / End -->
