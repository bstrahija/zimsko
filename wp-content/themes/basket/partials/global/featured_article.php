<!-- Main News Banner -->
<div class="main-news-banner main-news-banner--img-left">
    <figure class="main-news-banner__img">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/main-news-banner__img.png" alt="">
    </figure>
    <div class="main-news-banner__inner">
        <div class="posts posts--simple-list posts--simple-list--xlg">
            <div class="posts__item posts__item--category-1">
                <div class="posts__inner">
                    <div class="posts__cat">
                        <span class="label posts__cat-label">The Team</span>
                    </div>
                    <h6 class="posts__title"><a href="#">Take a look at the brand <span class="text-primary">New Uniforms</span> for next season</a></h6>
                    <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
                    <div class="posts__excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipisi ng elit, sed do eiusmod tempor.
                    </div>
                    <div class="posts__more">
                        <a href="#" class="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed">Read More <i class="fa fa-plus text-primary"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Main News Banner / End -->
