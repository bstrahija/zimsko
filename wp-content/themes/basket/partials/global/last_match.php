<?php

    use Creolab\Basket\Plugin\Repositories\Tournaments;

    $tournament = Tournaments::current();
    $tournamentId = $tournament->id();
    $match     = \Creolab\Basket\Plugin\Repositories\Matches::lastInTournament(['tournament_id' => $tournamentId]);
?>

<?php if ($match) : ?>
    <!-- Last Game Results -->
    <div class="card">
        <header class="card__header card__header--has-btn">
            <h4>Zadnja utakmica</h4>
            <a href="<?php echo home_url('rezultati') ?>" class="btn btn-default btn-outline btn-xs card-header__button">Svi rezultati</a>
        </header>
        <div class="card__content">

            <!-- Game Result -->
            <div class="game-result">
                <section class="game-result__section">
                    <header class="game-result__header">
                        <a href="<?php echo $match->permalink() ?>">
                            <h3 class="game-result__title"><?php echo $match->title() ?></h3>
                            <time class="game-result__date" datetime="<?php echo $match->date() ?>" style="color: #999;"><?php echo $match->dateAndTime() ?></time>
                        </a>
                    </header>
                    <div class="game-result__content">

                        <!-- 1st Team -->
                        <div class="game-result__team game-result__team--first">
                            <a href="<?php echo $match->winner()->permalink() ?>">
                                <figure class="game-result__team-logo">
                                    <img src="<?php echo $match->winner()->logo() ?>" alt="">
                                </figure>
                                <div class="game-result__team-info">
                                    <h5 class="game-result__team-name"><?php echo $match->winner()->title() ?></h5>
                                    <div class="game-result__team-desc"></div>
                                </div>
                            </a>
                        </div>
                        <!-- 1st Team / End -->

                        <div class="game-result__score-wrap">
                            <a href="<?php echo $match->permalink() ?>">
                                <div class="game-result__score">
                                    <span class="game-result__score-result game-result__score-result--winner"><?php echo $match->winningTeamScore() ?></span> <span class="game-result__score-dash">-</span> <span class="game-result__score-result game-result__score-result--loser"><?php echo $match->losingTeamScore() ?></span>
                                </div>
                                <div class="game-result__score-label">Finalni rezultat</div>
                            </a>
                        </div>

                        <!-- 2nd Team -->
                        <div class="game-result__team game-result__team--second">
                            <a href="<?php echo $match->loser()->permalink() ?>">
                                <figure class="game-result__team-logo">
                                    <img src="<?php echo $match->loser()->logo() ?>" alt="">
                                </figure>
                                <div class="game-result__team-info">
                                    <h5 class="game-result__team-name"><?php echo $match->loser()->title() ?></h5>
                                    <div class="game-result__team-desc"></div>
                                </div>
                            </a>
                        </div>
                        <!-- 2nd Team / End -->
                    </div>

                    <div class="game-result__stats">
                        <div class="game-result__table-stats">
                            <div class="table-responsive">
                                <table class="table table__cell-center table-wrap-bordered table-thead-color">
                                    <thead>
                                    <tr>
                                        <th>Scoreboard</th>
                                        <th>Q1</th>
                                        <th>Q2</th>
                                        <th>Q3</th>
                                        <th>Q4</th>
                                        <th>T</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th><?php echo $match->homeTeam() ?></th>
                                        <td><?php echo $match->homeTeamQuarterScore(1) ?></td>
                                        <td><?php echo $match->homeTeamQuarterScore(2) ?></td>
                                        <td><?php echo $match->homeTeamQuarterScore(3) ?></td>
                                        <td><?php echo $match->homeTeamQuarterScore(4) ?></td>
                                        <td><?php echo $match->homeTeamScore() ?></td>
                                    </tr>
                                    <tr>
                                        <th><?php echo $match->awayTeam() ?></th>
                                        <td><?php echo $match->awayTeamQuarterScore(1) ?></td>
                                        <td><?php echo $match->awayTeamQuarterScore(2) ?></td>
                                        <td><?php echo $match->awayTeamQuarterScore(3) ?></td>
                                        <td><?php echo $match->awayTeamQuarterScore(4) ?></td>
                                        <td><?php echo $match->awayTeamScore() ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!-- Game Result / End -->

        </div>
    </div>
    <!-- Last Game Results / End -->
<?php endif; ?>
