<!-- Video Slideshow -->
<div class="card card--clean">
    <header class="card__header card__header--has-filter">
        <h4>Video Slideshow</h4>
    </header>
    <div class="card__content">

        <!-- Carousel -->
        <div class="slick posts posts--carousel video-carousel">

            <div class="posts__item">
                <a href="#" class="posts__item-play"><i class="fa fa-play"></i></a>
                <a href="https://www.youtube.com/watch?v&#x3D;XE0fU9PCrWE" class="posts__link-wrapper mp_iframe">
                    <figure class="posts__thumb">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/video-slide1.jpg" alt="">
                    </figure>
                    <div class="posts__inner">
                        <div class="posts__cat">
                            <span class="label posts__cat-label">The Team</span>
                        </div>
                        <h3 class="posts__title">Cheerleader tryouts will start next Friday at 5pm</h3>
                        <time datetime="2017-08-28" class="posts__date">August 28th, 2018</time>
                    </div>
                </a>
            </div>
            <div class="posts__item">
                <a href="#" class="posts__item-play"><i class="fa fa-play"></i></a>
                <a href="https://vimeo.com/68384616" class="posts__link-wrapper mp_iframe">
                    <figure class="posts__thumb">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/video-slide2.jpg" alt="">
                    </figure>
                    <div class="posts__inner">
                        <div class="posts__cat">
                            <span class="label posts__cat-label">Playoffs</span>
                        </div>
                        <h3 class="posts__title">Spectacular view from the top of the stadium</h3>
                        <time datetime="2017-08-28" class="posts__date">August 28th, 2018</time>
                    </div>
                </a>
            </div>
            <div class="posts__item">
                <a href="#" class="posts__item-play"><i class="fa fa-play"></i></a>
                <a href="https://www.youtube.com/watch?v&#x3D;87FAolfQ0LY" class="posts__link-wrapper mp_iframe">
                    <figure class="posts__thumb">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/video-slide3.jpg" alt="">
                    </figure>
                    <div class="posts__inner">
                        <div class="posts__cat">
                            <span class="label posts__cat-label">The Team</span>
                        </div>
                        <h3 class="posts__title">&quot;Shiw Wow&quot; is the Alchemists new pet, check him out!</h3>
                        <time datetime="2017-08-28" class="posts__date">August 28th, 2018</time>
                    </div>
                </a>
            </div>
            <div class="posts__item">
                <a href="#" class="posts__item-play"><i class="fa fa-play"></i></a>
                <a href="https://www.youtube.com/watch?v&#x3D;ck0MCCIFD-o" class="posts__link-wrapper mp_iframe">
                    <figure class="posts__thumb">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/video-slide4.jpg" alt="">
                    </figure>
                    <div class="posts__inner">
                        <div class="posts__cat">
                            <span class="label posts__cat-label">Injuries</span>
                        </div>
                        <h3 class="posts__title">Mark Johnson has a Tibia Fracture and is gonna be out</h3>
                        <time datetime="2017-08-28" class="posts__date">August 28th, 2018</time>
                    </div>
                </a>
            </div>
            <div class="posts__item">
                <a href="#" class="posts__item-play"><i class="fa fa-play"></i></a>
                <a href="https://www.youtube.com/watch?v&#x3D;XE0fU9PCrWE" class="posts__link-wrapper mp_iframe">
                    <figure class="posts__thumb">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/video-slide5.jpg" alt="">
                    </figure>
                    <div class="posts__inner">
                        <div class="posts__cat">
                            <span class="label posts__cat-label">The Team</span>
                        </div>
                        <h3 class="posts__title">Cheerleader tryouts will start next Friday at 5pm</h3>
                        <time datetime="2017-08-28" class="posts__date">August 28th, 2018</time>
                    </div>
                </a>
            </div>
            <div class="posts__item">
                <a href="#" class="posts__item-play"><i class="fa fa-play"></i></a>
                <a href="https://www.youtube.com/watch?v&#x3D;ab0TSkLe-E0" class="posts__link-wrapper mp_iframe">
                    <figure class="posts__thumb">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/video-slide6.jpg" alt="">
                    </figure>
                    <div class="posts__inner">
                        <div class="posts__cat">
                            <span class="label posts__cat-label">Playoffs</span>
                        </div>
                        <h3 class="posts__title">Spectacular view from the top of the stadium</h3>
                        <time datetime="2017-08-28" class="posts__date">August 28th, 2018</time>
                    </div>
                </a>
            </div>
            <div class="posts__item">
                <a href="#" class="posts__item-play"><i class="fa fa-play"></i></a>
                <a href="https://vimeo.com/68384616" class="posts__link-wrapper mp_iframe">
                    <figure class="posts__thumb">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/video-slide7.jpg" alt="">
                    </figure>
                    <div class="posts__inner">
                        <div class="posts__cat">
                            <span class="label posts__cat-label">The Team</span>
                        </div>
                        <h3 class="posts__title">&quot;Shiw Wow&quot; is the Alchemists new pet, check him out!</h3>
                        <time datetime="2017-08-28" class="posts__date">August 28th, 2018</time>
                    </div>
                </a>
            </div>
            <div class="posts__item">
                <a href="#" class="posts__item-play"><i class="fa fa-play"></i></a>
                <a href="https://www.youtube.com/watch?v&#x3D;XE0fU9PCrWE" class="posts__link-wrapper mp_iframe">
                    <figure class="posts__thumb">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/samples/video-slide8.jpg" alt="">
                    </figure>
                    <div class="posts__inner">
                        <div class="posts__cat">
                            <span class="label posts__cat-label">Injuries</span>
                        </div>
                        <h3 class="posts__title">Mark Johnson has a Tibia Fracture and is gonna be out</h3>
                        <time datetime="2017-08-28" class="posts__date">August 28th, 2018</time>
                    </div>
                </a>
            </div>

        </div>
        <!-- Carousel / End -->

    </div>
</div>
<!-- Video Slideshow / End -->
