<div class="page-heading" style="<?php echo (($bg = get_field('archive_background', 'options')) ? 'background-image: url(' . $bg . ')' : '') ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <h1 class="page-heading__title"><?php the_title() ?></h1>
                <ol class="page-heading__breadcrumb breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo home_url() ?>">Početna</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo home_url('arhiva') ?>">Arhiva</a></li>
                </ol>
            </div>
        </div>
    </div>
</div>


<nav class="content-filter">
    <div class="container">
        <a href="#" class="content-filter__toggle"></a>
        <ul class="content-filter__list">
            <li class="content-filter__item content-filter__item--active"><a href="team-overview.html" class="content-filter__link"><small>Zimsko 2019</small>Rezultati</a></li>
            <li class="content-filter__item"><a href="team-roster-1.html" class="content-filter__link"><small>Zimsko 2019</small>Statistika</a></li>
            <li class="content-filter__item"><a href="team-standings.html" class="content-filter__link"><small>Zimsko 2019</small>Vijesti</a></li>
            <li class="content-filter__item"><a href="team-last-results.html" class="content-filter__link"><small>Zimsko 2019</small>Multimedija</a></li>
        </ul>
    </div>
</nav>
