<?php
/** Template Name: Homepage */

if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('header');
get_header();
if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('header');
?>

<?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('hero'); ?>
<?php partial('partials/home/hero') ?>
<?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('hero'); ?>

<?php // partial('partials/header/featured_matches') ?>


<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-8">
                <?php // partial('partials/global/last_match') ?>

                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('daily-matches'); ?>
                <?php partial('partials/results/daily_matches', ['limit' => 5, 'title' => 'Rezultati zadnjeg kola']) ?>
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('daily-matches'); ?>

                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('upcoming-matches'); ?>
                <?php partial('partials/global/upcoming_matches', ['limit' => 9]) ?>
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('upcoming-matches'); ?>

                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('latest-news'); ?>
                <?php partial('partials/global/latest_news') ?>
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('latest-news'); ?>

                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('content'); ?>
                <?php partial('partials/home/content') ?>
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('content'); ?>


            </div>
            <!-- Content / End -->

            <!-- Sidebar -->
            <div id="sidebar" class="sidebar col-lg-4">
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('live-score'); ?>
                <?php partial('partials/global/live_score') ?>
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('live-score'); ?>

                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('featured-news'); ?>
                <?php partial('partials/global/featured_news') ?>
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('featured-news'); ?>

                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('standings'); ?>
                <?php partial('partials/global/standings') ?>
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('standings'); ?>

                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('social'); ?>
                <?php partial('partials/aside/social') ?>
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('social'); ?>

                <?php  // partial('partials/aside/latest_matches') ?>

                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('tournament-leaders'); ?>
                <?php partial('partials/aside/tournament_leaders', ['limit' => 10]) ?>
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('tournament-leaders'); ?>

                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('3pt-leaders'); ?>
                <?php partial('partials/aside/3pt_leaders', ['limit' => 10]) ?>
                <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('3pt-leaders'); ?>
            </div>
            <!-- Sidebar / End -->
        </div>

        <?php // partial('partials/global/videos') ?>

        <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::start('sponsors'); ?>
        <?php partial('partials/global/sponsors') ?>
        <?php if (isset($_GET['debug'])) \Creolab\Basket\Theme\Services\Benchmark::endAndOutput('sponsors'); ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
