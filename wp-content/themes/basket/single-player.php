<?php get_header(); ?>

<?php
    // Find player and current team
    $player = \Creolab\Basket\Plugin\Repositories\Players::find(get_the_ID());
    $team   = $player->team();
?>

<?php partial('partials/player/hero', ['team' => $team]) ?>

<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-8">
                <?php partial('partials/team/latest_match', ['teamId' => $team->id()]) ?>

                <?php partial('partials/player/points', ['team' => $team]) ?>
            </div>
            <!-- Content / End -->

            <!-- Sidebar -->
            <div id="sidebar" class="sidebar col-lg-4">
                <?php  partial('partials/player/info', ['playerId' => $player->id()]) ?>

                <?php  partial('partials/team/awards', ['teamId' => $team->id()]) ?>

                <?php  // partial('partials/global/standings') ?>

                <?php  partial('partials/aside/social') ?>
            </div>
            <!-- Sidebar / End -->
        </div>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
