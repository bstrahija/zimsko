<?php

// Set the locale
setlocale(LC_ALL, 'hr_HR');

// Add autoloader
require_once __DIR__.'/vendor/autoload.php';

// Some error settings
ini_set('display_errors', 1);
error_reporting(2047);

// Bootstrap theme
(new \Creolab\Basket\Theme\Providers\BasketProvider)->init();

// Caching
define('BASKET_VIEW_CACHE', 720);
add_action('wp_cache_cleared', function() {
    global $wpdb;

    // Delete all transients
    $result = $wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('%transient_%')");
});

// Remove comments from admin menu
add_action('admin_menu', function() {
    remove_menu_page( 'edit-comments.php' );
});



