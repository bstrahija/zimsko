<?php get_header(); ?>

<?php partial('partials/news/hero_single') ?>


<!-- Content
		================================================== -->
<div class="site-content">
    <div class="container">

        <div class="row">
            <!-- Content -->
            <div class="content col-lg-8">
                <?php while (have_posts()) : the_post(); ?>
                    <!-- Article -->
                    <article class="card card--lg post post--single">
                        <?php if (has_post_thumbnail(get_the_ID())) : ?>
                            <figure class="post__thumbnail">
                                <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large') ?>" alt="">
                            </figure>
                        <?php endif; ?>

                        <div class="card__content">
                            <!-- <div class="post__category">
                                <span class="label posts__cat-label">The Team</span>
                            </div> -->
                            <header class="post__header">
                                <h2 class="post__title"><?php the_title() ?></h2>
                                <ul class="post__meta meta">
                                    <li class="meta__item meta__item--date"><time datetime="2017-08-23"><?php echo the_date() ?></time></li>
                                </ul>
                            </header>

                            <div class="post__content">
                                <?php the_content() ?>
                            </div>

                            <!-- <footer class="post__footer">
                                <div class="post__tags">
                                    <a href="#" class="btn btn-primary btn-outline btn-xs">Playoffs</a>
                                    <a href="#" class="btn btn-primary btn-outline btn-xs">Alchemists</a>
                                    <a href="#" class="btn btn-primary btn-outline btn-xs">Injuries</a>
                                    <a href="#" class="btn btn-primary btn-outline btn-xs">Team</a>
                                    <a href="#" class="btn btn-primary btn-outline btn-xs">Uniforms</a>
                                    <a href="#" class="btn btn-primary btn-outline btn-xs">Champions</a>
                                </div>
                            </footer> -->
                        </div>
                    </article>
                    <!-- Article / End -->
                <?php endwhile; ?>

                <!-- Post Sharing Buttons -->
                <!-- <div class="post-sharing">
                    <a href="#" class="btn btn-default btn-facebook btn-icon btn-block"><i class="fa fa-facebook"></i> <span class="post-sharing__label hidden-xs">Share on Facebook</span></a>
                    <a href="#" class="btn btn-default btn-twitter btn-icon btn-block"><i class="fa fa-twitter"></i> <span class="post-sharing__label hidden-xs">Share on Twitter</span></a>
                </div> -->
                <!-- Post Sharing Buttons / End -->
            </div>

            <!-- Sidebar -->
            <div id="sidebar" class="sidebar col-lg-4">
                <?php  partial('partials/global/standings') ?>

                <?php  partial('partials/aside/social') ?>

                <?php  partial('partials/aside/tournament_leaders') ?>
            </div>
            <!-- Sidebar / End -->
        </div>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>

<!-- Content / End -->

<?php get_footer(); ?>
