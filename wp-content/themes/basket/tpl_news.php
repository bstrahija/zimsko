<?php
/** Template Name: News */

get_header(); ?>

<?php partial('partials/news/hero') ?>


<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-8">
                <!-- Posts List -->
                <div class="posts posts--cards posts--cards-thumb-left post-list">
                    <?php foreach (\Creolab\Basket\Plugin\Repositories\News::latest(['posts_per_page' => 100]) as $article) : ?>
                        <div class="post-list__item">
                            <div class="posts__item posts__item--card posts__item--category-1 card card--block">
                                <?php if (has_post_thumbnail($article->ID)) : ?>
                                    <figure class="posts__thumb">
                                        <a href="<?php the_permalink($article->id); ?>"><img src="<?php echo get_the_post_thumbnail_url($article->ID, 'news-list') ?>" alt=""></a>
                                        <a href="#" class="posts__cta"></a>
                                    </figure>
                                <?php endif; ?>

                                <div class="posts__inner">
                                    <div class="card__content">
                                        <?php if (get_the_category()) : ?>
                                            <div class="posts__cat">
                                                <?php foreach (get_the_category() as $category) : ?>
                                                    <span class="label posts__cat-label"><?php echo $category->cat_name ?></span>
                                                <?php endforeach; ?>
                                            </div>
                                        <?php endif; ?>
                                        <h6 class="posts__title"><a href="<?php the_permalink($article->ID); ?>"><?php echo get_the_title($article->ID) ?></a></h6>
                                        <time datetime="2016-08-17" class="posts__date"><?php echo get_the_date('', $article->ID) ?></time>
                                        <div class="posts__excerpt">
                                            <?php echo get_the_excerpt($article->ID) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <!-- Content / End -->

            <!-- Sidebar -->
            <div id="sidebar" class="sidebar col-lg-4">
                <?php  partial('partials/global/standings') ?>

                <?php  partial('partials/aside/social') ?>
            </div>
            <!-- Sidebar / End -->
        </div>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
