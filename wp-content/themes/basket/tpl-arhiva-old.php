<?php
/** Template Name: Archive Old */

get_header(); ?>

<?php partial('partials/archive/hero') ?>


<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-8">
                <?php
                $standings = \Creolab\Basket\Plugin\Repositories\Standings::get();
                $limit   = isset($limit) ? $limit : 10;
                $leaders = \Creolab\Basket\Plugin\Repositories\Tournaments::leaders(['limit' => $limit]);
                ?>
                <?php partial('partials/global/standings') ?>

                <?php partial('partials/global/tournament_leaders') ?>
            </div>
            <!-- Content / End -->

            <!-- Sidebar -->
            <div id="sidebar" class="sidebar col-lg-4">
                <?php  partial('partials/global/featured_news') ?>

                <?php  partial('partials/global/standings') ?>

                <?php  partial('partials/aside/social') ?>
            </div>
            <!-- Sidebar / End -->
        </div>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
