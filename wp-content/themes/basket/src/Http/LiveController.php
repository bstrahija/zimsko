<?php

namespace Creolab\Basket\Theme\Http;

use Creolab\Basket\Plugin\Data\SyncMatch;
use Creolab\Basket\Plugin\Db\Tables;
use Creolab\Basket\Plugin\Repositories\Matches;
use WP_REST_Request;
use WP_REST_Response;

class LiveController
{
    public function show(WP_REST_Request $request) :WP_REST_Response
    {
        global $wpdb;

        // Find the match ID
        $matchId = (int) $request->get_param('id');

        // Get the match post
        $matchPost = get_post($matchId);

        // Find the match in new table
        $match = $wpdb->get_row("SELECT * FROM ".Tables::get('matches')." WHERE wp_id = $matchPost->ID");

        // If there's no match, we run the sync
        if (! $match) {
            $match = (new SyncMatch)->sync($matchPost);
        }

        // And try to get the match again
        $match = $wpdb->get_row("SELECT * FROM ".Tables::get('matches')." WHERE wp_id = $matchPost->ID");
        $data  = ['message' => 'No match found. Please try again.'];

        if ($match) {
            // Get the match
            $match = Matches::find($match->wp_id);
            $data  = $match->liveDetails();
        }

        return new WP_REST_Response($data, 200);
    }

    public function update(WP_REST_Request $request) :WP_REST_Response
    {
        global $wpdb;

        // Get the match
        $match         = Matches::find($request->get_param('id'));
        $quarterScores = $request->get_param('quarterScores');
        $finished      = (bool) ($request->get_param('status') === 'finished');

        // First update the ACF fields
        update_field('match_finished', (int) $finished, $match->ID);
        update_field('match_results',    [
            'home_score' => (int) $request->get_param('homeScore'),
            'away_score' => (int) $request->get_param('awayScore'),
        ], $match->ID);
        update_field('match_results_q1', [
            'home_score_q1' => (int) $quarterScores['home_q1'],
            'away_score_q1' => (int) $quarterScores['away_q1'],
        ], $match->ID);
        update_field('match_results_q2', [
            'home_score_q2' => (int) $quarterScores['home_q2'],
            'away_score_q2' => (int) $quarterScores['away_q2'],
        ], $match->ID);
        update_field('match_results_q3', [
            'home_score_q3' => (int) $quarterScores['home_q3'],
            'away_score_q3' => (int) $quarterScores['away_q3'],
        ], $match->ID);
        update_field('match_results_q4', [
            'home_score_q4' => (int) $quarterScores['home_q4'],
            'away_score_q4' => (int) $quarterScores['away_q4'],
        ], $match->ID);
        update_field('match_results_p1', [
            'home_score_p1' => (int) $quarterScores['home_ot1'],
            'away_score_p1' => (int) $quarterScores['away_ot1'],
        ], $match->ID);
        update_field('match_results_p2', [
            'home_score_p2' => (int) $quarterScores['home_ot2'],
            'away_score_p2' => (int) $quarterScores['away_ot2'],
        ], $match->ID);

        // Now update new tables
        $data = [
            'home_team_score' => (int) $request->get_param('homeScore'),
            'away_team_score' => (int) $request->get_param('awayScore'),
            'home_team_score_q1' => (int) $quarterScores['home_q1'],
            'away_team_score_q1' => (int) $quarterScores['away_q1'],
            'home_team_score_q2' => (int) $quarterScores['home_q2'],
            'away_team_score_q2' => (int) $quarterScores['away_q2'],
            'home_team_score_q3' => (int) $quarterScores['home_q3'],
            'away_team_score_q3' => (int) $quarterScores['away_q3'],
            'home_team_score_q4' => (int) $quarterScores['home_q4'],
            'away_team_score_q4' => (int) $quarterScores['away_q4'],
            'home_team_score_ot1' => (int) $quarterScores['home_ot1'],
            'away_team_score_ot1' => (int) $quarterScores['away_ot1'],
            'status'              => $request->get_param('status'),
        ];
        $wpdb->update(Tables::get('matches'), $data, ['wp_id' => $match->ID]);

        return new WP_REST_Response(['results' => []], 200);
    }
}
