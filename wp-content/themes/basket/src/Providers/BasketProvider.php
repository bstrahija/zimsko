<?php

namespace Creolab\Basket\Theme\Providers;

use Creolab\Basket\Plugin\Db\Tables;

class BasketProvider
{
    public function init()
    {
        (new AssetsProvider)->init();

        // Add some image dimensions
        add_image_size('news-list', 450, 450, true );

         // Add actions
         add_filter('post_row_actions',    [$this, 'setMatchEditButton'], 10, 2);
         add_filter('display_post_states', [$this, 'addMatchState'], 10, 2);

         // Add submenu page to admin interface
         add_action('admin_menu', function() {
             add_submenu_page(
                'edit',
                'Live Score',
                'Live Score',
                'manage_options',
                'live-score',
                function() {
                include __DIR__.'/../../assets/views/live-score.php';
                },
                1
             );
         });

         add_action( 'rest_api_init', function () {
            register_rest_route('zmsk/v1', '/live-details/(?P<id>\d+)', ['methods' => 'GET', 'callback' => ['\Creolab\Basket\Theme\Http\LiveController', 'show'],    'permission_callback' => '__return_true']);
            register_rest_route('zmsk/v1', '/live-details/(?P<id>\d+)', [
                'methods' => 'POST',
                'callback' => ['\Creolab\Basket\Theme\Http\LiveController', 'update'],
                'permission_callback' => function($request) {
                    return current_user_can('administrator');
                }
            ]);
        });

        add_filter('show_admin_bar' , function() {
            return false;
        });
    }

    public function setMatchEditButton($actions, $post)
    {
        if ($post->post_type != 'match')
            return $actions;

        // Old way: simple href link to WP edit page with a hashtag
        $actions['edit_with_op'] = sprintf(
            '<a href="%s" target="_blank">%s</a>',
            admin_url('admin.php?page=live-score&match_id='.$post->ID),
            'Live Score'
        );

        return $actions;
    }

    public function addMatchState($postStates, $page)
    {
        global $wpdb;

        $match = $wpdb->get_row("SELECT * FROM ".Tables::get('matches')." WHERE wp_id = $page->ID");
        $status = ($match && isset($match->status)) ? $match->status : '-';
        $statusColor = ($status == 'finished') ? '#0a0' : 'orange';

        if ($page->post_type != 'match')
            return $postStates;

        if (empty($page))
            return $postStates;

        $postStates['live-results'] = '&#9830; <a href="'.admin_url('admin.php?page=live-score&match_id='.$page->ID).'" target="_blank">Live Score</a> <small style="color: '.$statusColor.'">('.$status.')</small>';

        return $postStates;
    }
}
