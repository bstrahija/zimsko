<?php

namespace Creolab\Basket\Theme\Providers;

class AssetsProvider
{
    public function init()
    {
        global $pagenow;

        // Also add some JS data
        $data = [
            'ajax_url'         => admin_url('admin-ajax.php'),
            'admin_url'        => admin_url(),
            'current_match_id' => isset($_GET['match_id']) ? $_GET['match_id'] : null,
            'rest_url'         => esc_url_raw(rest_url('zmsk/v1')),
            'nonce'            => wp_create_nonce('wp_rest'),
            'user_id'          => get_current_user_id(),
            'user'             => wp_get_current_user(),
        ];

        add_action('wp_enqueue_scripts', function() use ($data) {
            // Stylesheets
            wp_enqueue_style('zimsko_style', get_template_directory_uri() . '/public/css/app.css');

            // Scripts
            wp_enqueue_script('zimsko_app_script', get_template_directory_uri() . '/public/js/app.js', ['jquery'], 1.0, true);

            wp_localize_script('zimsko_app_script', 'Zimsko', $data);
        });

        add_action('admin_enqueue_scripts', function() use ($data, $pagenow) {
            if ($pagenow === 'admin.php' && isset($_GET['page']) && $_GET['page'] === 'live-score') {
                // Stylesheets
                wp_enqueue_style('zimsko_admin_style', get_template_directory_uri() . '/public/assets/css/score.css');

                // Scripts
                wp_enqueue_script('zimsko_admin_app_script', get_template_directory_uri() . '/public/assets/js/score.js', ['jquery'], 1.0, true);

                wp_localize_script('zimsko_admin_app_script', 'Zimsko', $data);
            }
        });
    }
}
