<?php
/** Template Name: Galleries */

get_header(); ?>

<?php partial('partials/page/hero') ?>


<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-12">
                <?php
                    $galleryId = isset($_GET['id']) ? (int) $_GET['id'] : null;
                ?>

                <?php
                    if ($galleryId) {
                        partial('partials/galleries/single', ['id' => $galleryId]);
                    } else {
                        partial('partials/galleries/collection');
                    }
                ?>
            </div>
            <!-- Content / End -->
        </div>
        <br><br><br>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
