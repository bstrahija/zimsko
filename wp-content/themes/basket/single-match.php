<?php get_header(); ?>

<?php partial('partials/matches/hero') ?>


<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-8">
                <?php partial('partials/matches/details') ?>

                <?php partial('partials/matches/match_leaders') ?>
            </div>
            <!-- Content / End -->

            <!-- Sidebar -->
            <div id="sidebar" class="sidebar col-lg-4">
                <?php  partial('partials/matches/referees') ?>

                <?php  partial('partials/global/standings') ?>

                <?php  partial('partials/aside/social') ?>

                <?php  partial('partials/aside/upcoming_matches', ['limit' => 3]) ?>

                <?php  partial('partials/aside/tournament_leaders', ['limit' => 10]) ?>
            </div>
            <!-- Sidebar / End -->
        </div>

        <?php partial('partials/matches/gallery') ?>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
