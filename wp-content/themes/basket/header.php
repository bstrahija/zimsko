<!DOCTYPE html>
<html lang="hr">
<head>
    <?php partial('partials/header/meta') ?>

    <script src="https://kit.fontawesome.com/85e256c469.js" data-search-pseudo-elements defer crossorigin="anonymous"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-20635588-7"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-20635588-7');
    </script>
</head>
<body data-template="template-basketball" <?php body_class('page-loader-disable'); ?>>
<div class="site-wrapper clearfix">
    <div class="site-overlay"></div>

    <?php if (get_template_name() !== 'tpl-game-edit.php' && get_template_name() !== 'tpl-game-score.php') : ?>
        <!-- Header
        ================================================== -->
        <?php partial('partials/header/mobile') ?>
        <?php partial('partials/header/desktop') ?>
        <?php // partial('partials/side_panel') ?>
    <?php endif; ?>
