import { createWebHashHistory, createRouter } from "vue-router"
import Games from '../pages/Games.vue'

const routes = [
    { name: 'home', path: '/', component: Games,      meta: { title: 'Zimsko Matches', 'mark': 'dashboard' } },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes:  routes,
})

// Run on route change, and find meta title
router.beforeEach((to, from, next) => {
    const route = to.matched.slice().reverse().find(r => r.meta && r.meta.title)
    if (route) {
        document.title = route.meta.title

        // Mark navigation as active
        let path   = '/' + route.path.replace(/\//g, '#')
        let $items = jQuery('#toplevel_page_op-flow .wp-submenu a')
        let mark   = route.meta.mark
        let marked = false

        // Loop through items
        $items.each(function(index, el) {
            let $el  = jQuery(el)
            let href = $el.attr('href')
            let page = href.replace('admin\.php?page=op-flow\/\#', '')

            if (page === mark && path !== '/#') {
                marked = true;
                $el.closest('li').addClass('current')
            } else {
                $el.closest('li').removeClass('current')
            }
        })

        // Mark dashboard
        if (! marked) {
            jQuery('#toplevel_page_op-flow .wp-submenu li.wp-first-item').addClass('current')
        }
    }

    next()
})

export default router
