import { createApp } from 'vue'
import axios from 'axios'
import helpers from './mixins/helpers'
import router from './router'
import { markRaw } from 'vue'
import { createPinia } from 'pinia'
import mitt from 'mitt'
import { vfmPlugin } from 'vue-final-modal'
import VueFlatPickr from 'vue-flatpickr-component'
import Toaster from '@meforma/vue-toaster'
import ZimskoScoreApp from './components/ZimskoScoreApp.vue'
import ZimskoLiveScoreApp from './components/ZimskoLiveScoreApp.vue'

class ZimskoScore {
    /**
     * Init OptimizeTemplates app
     */
    constructor() {
        this.$            = jQuery
        this.debug        = true
        this.app          = null
        this.appLive      = null
        this.router       = null
        this.selector     = '#zimsko-score-app'
        this.selectorLive = '#zimsko-live-score-app'

        this._initApp()
    }

    /**
     * ZimskoScore
     * Init Vue.js app
     *
     * @private
     */
    _initApp() {
        this.$(() => {
            // Create new VueJS app
            this.app     = createApp(ZimskoScoreApp)
            this.appLive = createApp(ZimskoLiveScoreApp)

            // Enable debugging
            this.app.config.devtools = this.debug

            // Assign OPP object for global access
            this.app.config.globalProperties.Zimsko = window.Zimsko

            // Assign window object for global access
            this.app.config.globalProperties.window = window

            // Add helpers
            this.app.mixin(helpers)

            // Add HTTP client
            this.app.config.globalProperties.$http = axios
            axios.interceptors.request.use(function (config) {
                document.body.classList.add('zimsko-is-loading')
                // console.log("Loading...")
                return config
            }, function (error) {
                return Promise.reject(error)
            })
            axios.interceptors.response.use(function (response) {
                document.body.classList.remove('zimsko-is-loading')
                // console.log("Done loading...")
                return response
            }, function (error) {
                document.body.classList.remove('zimsko-is-loading')
                return Promise.reject(error)
            })

            // Set base URL
            axios.defaults.baseURL                      = window.Zimsko.rest_url
            axios.defaults.headers.common['X-WP-Nonce'] = window.Zimsko.nonce

            // Enable routing
            this.app.use(router)

            // Popovers
            // this.app.component("Popper", Popper)

            // Add event bus
            this.app.config.globalProperties.$events = mitt()

            // Add modal support
            this.app.use(vfmPlugin({
                key: '$vfm',
                componentName: 'VueFinalModal',
                dynamicContainerName: 'ModalsContainer'
            }))

            // Add notification support
            this.app.use(Toaster)

            // Add FlatPickr (DatePicker)
            this.app.use(VueFlatPickr)

            // Register a global custom directive called `v-focus`
            this.app.directive('focus', {
                // When the bound element is mounted into the DOM...
                mounted(el) {
                    // Focus the element
                    el.focus()
                }
            })

            // Pinia store
            const pinia = createPinia();
            pinia.use(({ store }) => {
                store.$http = this.app.config.globalProperties.$http
                store.$toast = this.app.config.globalProperties.$toast
                store.$router = this.app.config.globalProperties.$router
            });
            this.app.use(pinia)

            // Mount app and components
            router.isReady().then(() => {
                 this.app.mount(this.selector)
            })

            // Mark HTML element
            document.querySelector("html").classList.add("zimsko-score-app-root")
        })
    }
}


export default ZimskoScore

window.Zimsko.ZimskoScore = new ZimskoScore
