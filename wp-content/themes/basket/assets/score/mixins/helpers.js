export default {
    methods: {

        /**
         * Translate strings
         *
         * @param str
         * @returns {*}
         */
        $e: str => {
            return str
        },

        /**
         * Hide/close all modals
         */
        closeAllModals() {
            this.$vfm.hideAll()
        },
    },
}
