window.Basket = {};

window.Basket.App = {
    init: function() {
        jQuery(".points-history-chart").each(function() {
            let $chart = jQuery(this);
            let data = window.teamPointsHistoryData.data;
            let labels = window.teamPointsHistoryData.labels;

            let chartData = {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        label: 'POINTS',
                        fill: false,
                        lineTension: 0,
                        backgroundColor: "#ffdc11",
                        borderWidth: 2,
                        borderColor: "#ffdc11",
                        borderCapStyle: 'butt',
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'bevel',
                        pointRadius: 0,
                        pointBorderWidth: 0,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#fff",
                        pointHoverBorderColor: "#ffcc00",
                        pointHoverBorderWidth: 5,
                        pointHitRadius: 10,
                        data: data,
                        spanGaps: false,
                    }]
                },
                options: {
                    legend: {
                        display: false,
                        labels: {
                            boxWidth: 8,
                            fontSize: 9,
                            fontColor: '#31404b',
                            fontFamily: 'Montserrat, sans-serif',
                            padding: 20,
                        }
                    },
                    tooltips: {
                        backgroundColor: "rgba(49,64,75,0.8)",
                        titleFontSize: 0,
                        titleSpacing: 0,
                        titleMarginBottom: 0,
                        bodyFontFamily: 'Montserrat, sans-serif',
                        bodyFontSize: 9,
                        bodySpacing: 0,
                        cornerRadius: 2,
                        xPadding: 10,
                        displayColors: false,
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                color: "#e4e7ed",
                            },
                            ticks: {
                                fontColor: '#9a9da2',
                                fontFamily: 'Montserrat, sans-serif',
                                fontSize: 10,
                            },
                        }],
                        yAxes: [{
                            gridLines: {
                                display:false,
                                color: "rgba(255,255,255,0)",
                            },
                            ticks: {
                                beginAtZero: true,
                                fontColor: '#9a9da2',
                                fontFamily: 'Montserrat, sans-serif',
                                fontSize: 10,
                                padding: 20
                            }
                        }]
                    }
                },
            };

            let pointsChart = new Chart($chart, chartData);
        });

        this.refreshLiveScores();
    },

    refreshLiveScores: function() {
        let $container = jQuery(".widget__live-score");

        if ($container.length > 0) {
            let that     = this;
            let id       = $container.attr('data-id');
            let endpoint = window.Zimsko.rest_url + '/live-details/' + id;
            let nonce    = window.Zimsko.none;

            jQuery.ajax({
                url: endpoint,
                type: "GET",
                data: { nonce: nonce },
                success: function(data) {
                    $container.addClass('loaded');

                    if (data) {
                        $container.find('.widget__live-score__team-home-name').text(data.homeTeam.title);
                        $container.find('.widget__live-score__team-away-name').text(data.awayTeam.title);
                        $container.find('.widget__live-score__team-home-logo img').attr("src", data.homeTeam.logo);
                        $container.find('.widget__live-score__team-away-logo img').attr("src", data.awayTeam.logo);
                        $container.find('.widget__live-score__total-home').text(data.homeScore);
                        $container.find('.widget__live-score__total-away').text(data.awayScore);
                        $container.find('.widget__live-score__quarter1-home').text(data.quarterScores.home_q1);
                        $container.find('.widget__live-score__quarter1-away').text(data.quarterScores.away_q1);
                        $container.find('.widget__live-score__quarter2-home').text(data.quarterScores.home_q2);
                        $container.find('.widget__live-score__quarter2-away').text(data.quarterScores.away_q2);
                        $container.find('.widget__live-score__quarter3-home').text(data.quarterScores.home_q3);
                        $container.find('.widget__live-score__quarter3-away').text(data.quarterScores.away_q3);
                        $container.find('.widget__live-score__quarter4-home').text(data.quarterScores.home_q4);
                        $container.find('.widget__live-score__quarter4-away').text(data.quarterScores.away_q4);

                        // Load next scores in 5 seconds
                        setTimeout(window.Basket.App.refreshLiveScores, 10000);
                    }
                }
            });
        }

        // Get the content container
        let $content = jQuery('.site-content .content');

        // Get width of screen
        let screenWidth = window.innerWidth;
        // console.log(screenWidth);

        // Check if we are on a mobile device by the screen width
        let isMobile = screenWidth < 900;
        // console.log(isMobile);

        // For mobile we need to move the live score widget to the top of the page
        if ($content.length > 0 && isMobile) {
            let $liveScore = jQuery('.widget__live-score');
            $liveScore.addClass('mobile');

            if ($liveScore.length > 0) {
                $content.prepend($liveScore);
            }
        }




    }
};

jQuery(function($) {
    window.Basket.App.init();
});
