<?php get_header(); ?>

<div class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <h1 class="page-heading__title">Greška <span class="highlight">404</span></h1>
                <ol class="page-heading__breadcrumb breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo home_url() ?>">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Greška</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="site-content">
    <div class="container">

        <!-- Error 404 -->
        <div class="error-404">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <figure class="error-404__figure">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon-ghost.svg" alt="">
                    </figure>
                    <header class="error__header">
                        <h2 class="error__title">UPS! Stranice nije pronađena</h2>
                        <h3 class="error__subtitle">Izgleda da imamo problem!</h3>
                    </header>
                    <!-- <div class="error__description">
                        The page you are looking for has been moved or doesn’t exist anymore, if you like you can return to our homepage. If the problem persists, please send us an email to <a href="mailto:info@alchemists.com">info@alchemists.com</a>
                    </div> -->
                    <footer class="error__cta">
                        <a href="<?php echo home_url() ?>" class="btn btn-primary">Povratak na početnu stranicu</a>
                    </footer>
                </div>
            </div>
        </div>
        <!-- Error 404 / End -->

    </div>
</div>


<?php get_footer(); ?>
