<?php get_header(); ?>

<?php partial('partials/team/hero') ?>


<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <!-- Team Roster: Grid -->
        <div class="team-list team-list--grid row">
            <?php foreach (\Creolab\Basket\Plugin\Repositories\Teams::get() as $team) : ?>
                <div class="team-list__item col-4 col-sm-3">
                    <a href="<?php echo get_permalink($team->ID) ?>" class="team-list__holder">
                        <figure class="team-list__img">
                            <img src="<?php the_field('team_logo', $team->ID) ?>" alt="">
                        </figure>
                        <div class="team-list__content">
                            <div class="team-list__content-inner">
                                <div class="team-list__member-info">
                                    <h2 class="team-list__member-name">
                                        <?php echo $team->post_title ?>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer() ?>
