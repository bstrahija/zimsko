<?php
/** Template Name: Archive */

get_header(); ?>

<?php partial('partials/archive/hero') ?>


<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-12">

            </div>
            <!-- Content / End -->
        </div>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
