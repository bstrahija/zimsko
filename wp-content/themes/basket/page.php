<?php get_header(); ?>

<?php partial('partials/page/hero') ?>


<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col">
                <?php while (have_posts()) : the_post(); ?>
                    <?php the_content() ?>
                <?php endwhile; ?>
            </div>
            <!-- Content / End -->

            <!-- Sidebar -->
            <div id="sidebar" class="sidebar col-lg-4">
                <?php  partial('partials/global/standings') ?>

                <?php partial('partials/aside/tournament_leaders', ['limit' => 10]) ?>

                <?php partial('partials/aside/3pt_leaders', ['limit' => 10]) ?>

                <?php  partial('partials/aside/social') ?>
            </div>
            <!-- Sidebar / End -->
        </div>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
