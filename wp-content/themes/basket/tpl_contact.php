<?php
/** Template Name: Contact */

get_header(); ?>

<?php partial('partials/news/hero') ?>


<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-8">
                <?php while (have_posts()) : the_post(); ?>
                    <!-- Contact Area -->
                    <div class="card">
                        <header class="card__header">
                            <h4><?php the_title() ?></h4>
                        </header>
                        <div class="card__content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <!-- Contact Area / End -->
                <?php endwhile; ?>
            </div>
            <!-- Content / End -->

            <!-- Sidebar -->
            <div id="sidebar" class="sidebar col-lg-4">
                <?php  partial('partials/global/standings') ?>

                <?php  partial('partials/aside/social') ?>
            </div>
            <!-- Sidebar / End -->
        </div>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
