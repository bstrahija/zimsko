<?php
/** Template Name: Live Score */

use Creolab\Basket\Plugin\Repositories\Matches;

get_header();

wp_enqueue_style('zimsko-score-app-css', get_template_directory_uri() . '/public/assets/css/score.css');
wp_enqueue_script('zimsko-score-app-js', get_template_directory_uri() . '/public/assets/js/score.js', ['jquery'], false, true);

// Find the game/match
$matchId = isset($_GET['match_id']) ? $_GET['match_id'] : null;

// If no match ID is set, we need to display the match picker
if (! $matchId) {
    partial('partials/live/select_live');
    get_footer();
    exit;
// Else we try yo find the match/game
} else {
    $match = Matches::find($matchId);

    if (! $match) {
        echo 'Utakmica nije nađena';
    } else {
        partial('partials/live/index', ['match' => $match]);
    }
    get_footer();
    exit;
}
