<?php get_header(); ?>

<?php partial('partials/team/hero_single') ?>

<!-- Content
================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="content col-lg-8">
                <?php // partial('partials/team/photo') ?>

                <?php partial('partials/team/logo_head') ?>

                <?php partial('partials/team/latest_match') ?>

                <?php partial('partials/team/roster') ?>

                <?php partial('partials/team/upcoming_matches', ['limit' => 9]) ?>

                <?php partial('partials/team/points') ?>

                <?php // partial('partials/team/aside_latest_matches') ?>
            </div>
            <!-- Content / End -->

            <!-- Sidebar -->
            <div id="sidebar" class="sidebar col-lg-4">
                <?php  partial('partials/team/awards') ?>

                <?php  partial('partials/team/coaches') ?>

                <?php  partial('partials/team/team_leaders') ?>

                <?php  partial('partials/global/standings') ?>

                <?php  partial('partials/aside/social') ?>
            </div>
            <!-- Sidebar / End -->
        </div>

        <?php partial('partials/global/sponsors') ?>
    </div>
</div>
<!-- Content / End -->

<?php get_footer(); ?>
