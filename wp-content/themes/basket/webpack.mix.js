const mix = require('laravel-mix');

mix.options({
    processCssUrls: false
});

mix.js('assets/js/app.js', 'public/js').sourceMaps();
mix.sass('assets/scss/app.scss', 'public/css').sourceMaps();

mix.sass('assets/score/scss/score.scss', 'public/assets/css').sourceMaps();
mix.js('assets/score/score.js', 'public/assets/js').vue().sourceMaps();
